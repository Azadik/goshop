<?php
/**
 * Created by PhpStorm.
 * User: Azad
 * Date: 9/29/2018
 * Time: 17:31
 */
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Tools {
    public function convertHourToSeconds($hour){
        return $hour * 3600;
    }

    public function convertMinutesToSeconds($minute){
        return $minute * 60;
    }

    public function convertDayToSeconds($day){
        return $day * 86400;
    }

    public function blacklist(){
        DB::table('blacklist')->insert([
            'user_id'    => Auth::user()->id,
            'date'       => date('d-m-Y H:i:s'),
            'from'       => $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
            'ip'         => $_SERVER['REMOTE_ADDR'],
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
        ]);
    }


    public function translit($var) {
        if (preg_match( '/[^A-Za-z0-9_\-]/', $var )) {
            $tr = array( "А" => "a", "Б" => "b", "В" => "v", "Г" => "g", "Д" => "d", "Е" => "e", "Ж" => "j", "З" => "z", "И" => "i", "Й" => "y", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n", "О" => "o", "П" => "p", "Р" => "r", "С" => "s", "Т" => "t", "У" => "u", "Ф" => "f", "Х" => "h", "Ц" => "ts", "Ч" => "ch", "Ш" => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "yi", "Ь" => "", "Э" => "e", "Ю" => "yu", "Я" => "ya", "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ж" => "j", "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l", "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r", "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h", "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "y", "ы" => "yi", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya", "ı" => "i", "I" => "i", "Ü" => "u", "ü" => "u", "Ə" => "E", "İ" => "i", "i" => "i", "Ö" => "o", "ö" => "o", "Ğ" => "g", "Ç" => "c", "ç" => "c", "Ş" => "s", "ş" => "s", "ğ" => "g", "&#601;" => "e", "&#214;" => "o", "&#231;" => "ch", "ə" => "e", " " => "-", "/" => "-", " - " => "-", "--" => "-", "&#305;" => "i", "&#199;" => "ch", "&#287;" => "g", "&#252;" => "u", "&#351;" => "sh", "&#246;" => "o", "." => "" );
            $var = strtr( trim( $var ), $tr );
            $var = strtolower( $var );
            $var = str_replace( '--', '-', $var );
        }
        $var = htmlspecialchars( trim( $var ) );
        return $var;
    }

    public function qorun($temiz) {
        $ar=array("!"=>"", "™"=>"","\""=>"","#"=>"","@"=>"","$"=>"s","%"=>"","^"=>"","*"=>"","&"=>" ","<"=>"",">"=>"",";"=>"",":"=>"","'"=>" ",","=>"","{"=>"","}"=>"","["=>"","]"=>"","-"=>" ","|"=>" ","?"=>"","`"=>"","("=>" ",")"=>" ","~"=>"","+"=>" ","lang=ru"=>"","lang=az"=>"","lang=en"=>"","lang=tr"=>"","="=>"","/"=>" ","http://"=>"","http://vk.com"=>"","♥"=>" ","★"=>" ","♫"=>" ","▄ █ ▄ █"=>"","é"=>"e","script"=>"","amp;"=>"");
        $temiz=strip_tags($temiz);
        $temiz=strtr($temiz,$ar);
        $temiz=trim($temiz);
        $temiz = str_replace( ' ', '-', $temiz );
        return $temiz;
    }

    public function totalUsdMonthQuota(){
        $user_id = Auth::user()->id;


//        $total_usd = DB::select('select SUM(t.all_usd) as total_usd from (SELECT p.user_id, p.all_usd
//                            FROM products p
//                            WHERE p.user_id = '.$user_id.'
//                            ORDER BY p.id DESC
//                            LIMIT 30) t');

        $total_usd = DB::select('select SUM(t.all_usd) as total_usd from (SELECT p.user_id, p.all_usd
                            FROM products p
                            WHERE p.user_id = '.$user_id.'
                            AND   p.order_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()
                            ) t');

        $total_usd = $total_usd[0]->total_usd;
        return $total_usd;
    }


    public function getSiteName($site){
        $site = parse_url($site);
        if (array_key_exists('host', $site)) {
            $site = $site['host'];
        }elseif (array_key_exists('path', $site)){
            $site = $site['path'];
        }else{
            $site = 'Not Define';
        }
        return $site;
    }

    public function siteParser($site){
        require_once(public_path().'/phpQuery/phpQuery.php');
        @$url = file_get_contents($site);

        if($url){ //esli file_get_contents ne vidaet oshibku
            $site_name = $this->getSiteName($site);

            $document = phpQuery::newDocument($url);
            $title = $this->qorun($document->find("title")->text());

            if( $site_name == 'www.trendyol.com' || $site_name == 'trendyol.com'){
                $meta_tag_name = 'twitter:image:src';
                $atribut = 'name';
            }elseif ($site_name == 'www.koton.com'
                or $site_name == 'tr.uspoloassn.com'){
                $meta_tag_name = 'og:image';
                $atribut = 'name';
            }elseif ($site_name == 'www.pierrecardin.com.tr'
                or $site_name == 'www.network.com.tr'
                or $site_name == 'www.cacharel.com.tr'
                or $site_name == 'www.morhipo.com'){
                $meta_tag_name = 'og:image';
                $atribut = 'property';
            }elseif ($site_name == 'www.lcwaikiki.com'){
                $meta_tag_name = 'ImageLink';
                $atribut = 'name';
            }
            else {
                $meta_tag_name = null;
                $atribut = null;
            }


            if($meta_tag_name == null and $atribut == null){ //esli nikakogo sayta net v spiske
                $title = null;
                $img = null;
            }else{
                $a = [];
                foreach(pq('meta') as $meta) {
                    $key = pq($meta)->attr($atribut);
                    if($key == $meta_tag_name){
                        $img = pq($meta)->attr('content');
                        break;
                    }else{
                        $img = null;
                    }
                    $a[$key] = pq($meta)->attr('content');
                }
            }

        }else{
            $title = null;
            $img = null;
        }

        $data = array('title' => $title, 'img' => $img);

        return $data;
    }

    public function getPriceSize($url){
        require_once(public_path().'/phpQuery/phpQuery.php');
        @$url = file_get_contents($url);

        if($url){
            $document = phpQuery::newDocument($url);

            $price = null;
            foreach(pq('meta') as $meta) {
                $key = pq($meta)->attr('name');
                if($key == 'twitter:data1'){
                    $price = pq($meta)->attr('content');

                    $price = str_replace(',', '.', $price);
                    break;
                }
            }

            if(!$price){ $price = 0; }

            $sizes = 0;
            $variants = $document->find('.pr-in-drp-u .vrn-item');

            $i = 0;
            foreach ($variants as $variant){
                if( $i == 0 ){ $sizes .= '<option>Seçin</option>'; } $i++;

                    if(pq($variant)->hasClass('so')){
                        $disabled = 'disabled';
                    }else{
                        $disabled = '';
                    }

                    $sizes.= '<option value="'.pq($variant)->text().'" '.$disabled.'>'.pq($variant)->text().'</option>';
            }


            if(empty($sizes)){
                $sizes = 0;
            }


            return array('price' => $price, 'sizes'=> $sizes );

        }else{
            return array('price' => 0, 'sizes'=> 0 );
        }

        die;
    }


    public function getImg($url){
        $data = explode('.', json_decode($url)->creative);
        $img = 'https://img-trendyol.mncdn.com'.$data[0].'_new.'.$data[1];
        return $img;
    }

    public function trendyol(){
        require_once(public_path().'/phpQuery/phpQuery.php');
        @$url = file_get_contents('https://www.trendyol.com/');

        if($url){
            $document = phpQuery::newDocument($url);
            $data = $document->find('.component-big-list .component-item');
            if($data->count()){
                foreach ($data as $d){
                    $trendyol = pq($d);
                    $img = $this->getImg($trendyol->find('a.campaign-big')->attr('data-enhanced'));
                    $title = $trendyol->find('.image-container img')->attr('alt');
                    $url = 'https://www.trendyol.com'.$trendyol->find('a.campaign-big')->attr('href');
                    $trendyol_data[] = array(
                        'img'   => $img,
                        'title' => $title,
                        'url'   => $url
                    );
                }
            }else{
                $trendyol_data = null;
            }
//            https://img-trendyol.mncdn.com/Assets/ProductImages/oa/CampaignVisual/OriginalBoutiqueImages/317688/5e0ea8e31e_1_new.jpg
//            /Assets/ProductImages/oa/CampaignVisual/OriginalBoutiqueImages/317688/5e0ea8e31e_1.jpg
//            dd($trendyol_data);
            return $trendyol_data;
        }else{
            return $trendyol_data = null;
        }
    }

    public function getLocationInfoByIp(){
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = @$_SERVER['REMOTE_ADDR'];
        $result  = array('country'=>'', 'city'=>'');
        if(filter_var($client, FILTER_VALIDATE_IP)){
            $ip = $client;
        }elseif(filter_var($forward, FILTER_VALIDATE_IP)){
            $ip = $forward;
        }else{
            $ip = $remote;
        }
        $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
        if($ip_data && $ip_data->geoplugin_countryName != null){
            $result['country'] = $ip_data->geoplugin_countryCode;
            $result['city'] = $ip_data->geoplugin_city;
        }
        return $result;
    }


    public function recycleBinBasket($id){
        DB::select('INSERT INTO recycle_bin_basket ( 
                                                                user_id,
                                                                 url,
                                                                  note,
                                                                   order_date,
                                                                    `count`,
                                                                     country,
                                                                      price,
                                                                       weight,
                                                                        shipping_price,
                                                                         img,
                                                                          status,
                                                                           boutique_id,
                                                                            order_time,
                                                                             boutique_time,
                                                                              price_az,
                                                                               price_usa,
                                                                                title,
                                                                                 order_id,
                                                                                  baglama_id,
                                                                                   all_usd,
                                                                                    own_price, 
                                                                                     img_status,
                                                                                      order_number,
                                                                                      cargo,
                                                                                      cargo_az,
                                                                                      manual,
                                                                                      lost,
                                                                                      product_number
                                                                                       )
                                                              
                                                          SELECT 
                                                           user_id,
                                                                 url,
                                                                  note,
                                                                   order_date,
                                                                    `count`,
                                                                     country,
                                                                      price,
                                                                       weight,
                                                                        shipping_price,
                                                                         img,
                                                                          status,
                                                                           boutique_id,
                                                                            order_time,
                                                                             boutique_time,
                                                                              price_az,
                                                                               price_usa,
                                                                                title,
                                                                                 order_id,
                                                                                  baglama_id,
                                                                                   all_usd,
                                                                                    own_price, 
                                                                                     img_status,
                                                                                      order_number,
                                                                                      cargo,
                                                                                      cargo_az,
                                                                                      manual,
                                                                                      lost,
                                                                                      id
                                                           FROM tm_products  WHERE id = ' . $id);
    }
}