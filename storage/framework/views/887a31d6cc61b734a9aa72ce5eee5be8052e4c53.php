<?php $__env->startSection('content'); ?>
    <script>
        let tl = parseFloat('<?=$course_valute->tl?>');
        let usd = parseFloat('<?=$course_valute->usa?>');
    </script>
        <div class="row">

            <div class="col-sm-12 mobile-slider">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <?php $k=0; ?>
                        <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li data-target="#myCarousel" data-slide-to="<?php echo e($k); ?>" <?php if($k == 0): ?> class="active" <?php endif; ?> ></li>
                        <?php $k++; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ol>


                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php $s=1; ?>
                        <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="item <?php if($s == 1): ?> active <?php endif; ?>">
                                <a href="<?php echo e($slide->url); ?>" target="_blank">
                                    <img src="<?php echo e(asset('slider')); ?>/<?php echo e($slide->img); ?>" alt="Image">
                                    <div class="carousel-caption">
                                        <h3><?php echo e($slide->title); ?></h3>
                                    </div>
                                </a>
                            </div>
                            <?php $s++; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <hr>

        <?php echo $__env->make('user.calculate', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div class="text-center">
            <h2 class="own-header"><?php echo trans('interface.how_to_works'); ?></h2>
            <br>
            <div class="row">

                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="thumbnail">
                        <img src="<?php echo e(asset('own/location-pin.svg')); ?>" alt="Lights" style="width:50%">
                        <div class="caption own-captain">
                            <h4><?php echo e(trans('interface.get_our_address_abroad')); ?></h4>
                            <p><?php echo trans('interface.get_our_address_abroad_text'); ?></p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="thumbnail">
                        <img src="<?php echo e(asset('own/position.svg')); ?>" alt="Lights" style="width:50%">
                        <div class="caption own-captain">
                            <h4><?php echo e(trans('interface.send_order_in_our_address')); ?></h4>
                            <p><?php echo e(trans('interface.send_order_in_our_address_text')); ?></p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="thumbnail">
                        <img src="<?php echo e(asset('own/home.svg')); ?>" alt="Lights" style="width:50%">
                        <div class="caption own-captain">
                            <h4><?php echo e(trans('interface.home_delivery')); ?></h4>
                            <p><?php echo e(trans('interface.home_delivery_text')); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div class="row">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3039.414401324129!2d49.848357815782755!3d40.37750727936967!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2s!4v1545132699250" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>


        <div class="text-center">
            <div class="own-carousel row">
                <br>
                <ul id="content-slider" class="content-slider">
                    <li>
                        <div class="own-box"><a href=""><img src="shops/damat.png"></a></div>
                    </li>
                    <li>
                        <div class="own-box"><a href=""><img src="shops/Desktop_NW_Logo.png"></a></div>
                    </li>
                    <li>
                        <div class="own-box"><a href=""><img src="shops/dsdamat.png"></a></div>
                    </li>
                    <li>
                        <div class="own-box"><a href=""><img src="shops/lcw.png"></a></div>
                    </li>
                    <li>
                        <div class="own-box"><a href=""><img src="shops/mango.png"></a></div>
                    </li>
                    <li>
                        <div class="own-box"><a href=""><img src="shops/trendyol-online-white.png"></a></div>
                    </li>
                    <li>
                        <div class="own-box"><a href=""><img src="shops/damat.png"></a></div>
                    </li>
                    <li>
                        <div class="own-box"><a href=""><img src="shops/Desktop_NW_Logo.png"></a></div>
                    </li>
                    <li>
                        <div class="own-box"><a href=""><img src="shops/dsdamat.png"></a></div>
                    </li>
                    <li>
                        <div class="own-box"><a href=""><img src="shops/lcw.png"></a></div>
                    </li>
                    <li>
                        <div class="own-box"><a href=""><img src="shops/mango.png"></a></div>
                    </li>
                    <li>
                        <div class="own-box"><a href=""><img src="shops/trendyol-online-white.png"></a></div>
                    </li>
                </ul>
            </div>

        </div>
    <br>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>