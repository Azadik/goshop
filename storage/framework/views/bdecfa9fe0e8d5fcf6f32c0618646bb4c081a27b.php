<?php $__env->startSection('content'); ?>
      <div class="row">
          <?php echo $__env->make('user.content_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <div style="clear: both"><br></div>
          <div class="col-md-12 col-sm-12">
              <div class="white-background">
                  <div class="row">
                      <div class="col-md-12 col-sm-12">
                          <?php if(Session::has('msg')): ?>
                              <?php echo Session::get('msg'); ?>

                          <?php endif; ?>
                      </div>

                      <div class="col-md-12 col-sm-12">
                          <div class="panel panel-default">
                              <div class="panel-heading sorgu_img">
                                  <?php if($question->img): ?>
                                      <img id="myImg" src="<?php echo e(asset('questions')); ?>/<?php echo e($question->img); ?>" alt="Snow" style="width:100%;max-width:100px">
                                      <div id="myModal" class="modal">
                                          <span class="close_task_img">&times;</span>
                                          <img class="modal-content" id="img01">
                                          <div id="caption"></div>
                                      </div>
                                  <?php endif; ?>
                                  <?php echo e($question->text); ?>

                              </div>

                              <div class="panel-body chat_message">
                                  <div class="text-right">
                                      <?php if($question->status == 1): ?>
                                          <?php echo e($question->finish_date); ?>

                                      <?php else: ?>
                                          <button data-id="<?php echo e($question->id); ?>" class="btn btn-success btn-sm finishQuestion" style="padding: 0px 4px;">
                                              <span class="glyphicon glyphicon-ok"></span> <?php echo e(trans('interface.to_end')); ?>

                                          </button>
                                      <?php endif; ?>
                                  </div>
                                  <br>

                                  <?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <div class="container_msg
                                            <?php if(Auth::user()->id != $comment->user_id): ?>
                                              <?php echo e('darker'); ?>

                                              <?php endif; ?>
                                                ">

                                          <?php if($comment->user_id): ?>
                                              <?php if($comment->user_img): ?>
                                                <img src="<?php echo e(asset('profile')); ?>/<?php echo e(Auth::user()->img); ?>" alt="<?php echo e(Auth::user()->name); ?> <?php echo e(Auth::user()->last_name); ?>"
                                                     <?php if(Auth::user()->id == $comment->user_id): ?>
                                                             <?php echo e('class=right'); ?>

                                                             <?php endif; ?>
                                                     style="width:100%;">
                                              <?php else: ?>
                                                  <?php
                                                  $f_name = substr(Auth::user()->name, 0,1);
                                                  $f_lastname = substr(Auth::user()->last_name, 0,1);
                                                  $profile_name = $f_name.'.'.$f_lastname
                                                  ?>
                                                  <div class="profile_name_right"><?php echo e($profile_name); ?></div>
                                              <?php endif; ?>
                                          <?php else: ?>
                                              <img src="<?php echo e(asset('profile')); ?>/admin.jpg" alt="Avatar"  style="width:100%;">
                                          <?php endif; ?>

                                          <p><?php echo e($comment->text); ?></p>
                                          <span style="font-size: 12px" class="
                                            <?php if(Auth::user()->id != $comment->user_id): ?>
                                              <?php echo e('time-right'); ?>

                                            <?php else: ?>
                                              <?php echo e('time-left'); ?>

                                            <?php endif; ?>
                                           "><?php echo e($comment->date); ?></span>
                                      </div>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                      <form action="<?php echo e(route('create.comment')); ?>" method="post">
                                          <?php echo e(csrf_field()); ?>

                                          <input type="hidden" name="id" value="<?php echo e($question->id); ?>">
                                          <div class="form-group">
                                              <textarea class="form-control" rows="2" id="comment" name="comment" required></textarea>
                                          </div>

                                          <div class="text-right">
                                              <button class="btn btn-success btn-sm text-right"><i class="glyphicon glyphicon-send"></i>&nbsp; <?php echo e(trans('interface.send')); ?></button>
                                          </div>
                                      </form>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <?php echo $__env->make('user.beyanname', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <div style="clear: both"><br></div>
      </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>