<?php $__env->startSection('content'); ?>
      <div class="row">
          <div class="col-md-12 col-sm-12">
              <div class="white-background">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Img</th>
                                    <th>Title</th>
                                    <th>Sifarişin qeydi</th>
                                    <th>Sifariş tarixi</th>
                                    <th class="text-center">Say</th>
                                    <th class="text-right">Qiymət</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <?php
                                        if($order['country'] == 1){
                                            $valyuta = 'TL';
                                            $price = $order['price'];
                                        } else {
                                            $valyuta = 'USD';
                                            $price = $order['price_usa'];
                                        }

                                        if($order['img']){
                                            echo '<img src="'.asset('products/').'/'.$order['img'].'" alt="'.$order['title'].'" width="50">';
                                        }else{
                                            echo '<img src="'.asset('products/no_img.png').'" width="50">';
                                        }
                                        ?>
                                    </td>
                                    <td style="max-width: 300px">
                                        <?php if($order['title']): ?>
                                            <a href="<?php echo e($order['url']); ?>" target="_blank"><?php echo e($order['title']); ?></a>
                                        <?php else: ?>
                                            <a href="<?php echo e($order['url']); ?>" target="_blank"><?php echo e($order['url']); ?></a>
                                        <?php endif; ?>
                                    </td>
                                    <td><?php echo e($order['note']); ?></td>
                                    <td><?php echo e($order['order_date']); ?> <sup><?php echo e($order['order_time']); ?></sup></td>
                                    <td class="text-center"><?php echo e($order['count']); ?></td>
                                    <td class="text-right"><?php echo e($price); ?> <?php echo e($valyuta); ?></td>
                                </tr>

                                <?php if($cargo_info): ?>
                                    <tr>
                                        <th colspan="6">
                                            Türkiyə kargosu
                                        </th>
                                    </tr>

                                    <?php $__currentLoopData = $cargo_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td colspan="5"><?php echo e($key); ?></td>
                                            <td class="text-right">
                                                <?php if($value['price'] == 0): ?>
                                                    Kargo bedava
                                                <?php else: ?>
                                                    <?php echo e($value['price']); ?> TL
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                                <?php if($haverushproduct): ?>
                                    <tr>
                                        <td colspan="5">Təcili sifariş</td>
                                        <td class="text-right">1 $</td>
                                    </tr>
                                <?php endif; ?>

                                <tr>
                                    <td colspan="5"></td>
                                    <td class="text-right"><b style="font-size: 16px">Total price: <?php echo e($total_prices->total_tl + $cargo_prices); ?> TL</b></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-12 text-center">
                            <form action="<?php echo e(route('paymes.form')); ?>" method="post">
                                <p>
                                    <label><input type="checkbox" required value="1"> <a  target="_blank" href="<?php echo e(route('guest.getpage', [8, 'xidmet-shertleri'])); ?>">Şərtlərlə</a> və <a target="_blank" href="<?php echo e(route('user.rules')); ?>">qaydalarla</a> razıyam</label>
                                </p>

                                <input type="hidden" name="products[]" value="<?php echo e($order['id']); ?>">
                                <input type="hidden" name="amount" value="<?php echo e($total_prices->total_tl + $cargo_prices); ?>" maxlength="12">
                                <input type="hidden" id="description" name="description" value="<?php echo e(Auth::user()->name); ?> <?php echo e(Auth::user()->last_name); ?> -- <?php echo e($total_prices->total_tl); ?> TL / <?php echo e($total_prices->total_az + $cargo_prices); ?> AZN">
                                <button class="btn btn-success btn-sm">Ödə</button>
                            </form>
                        </div>

                    </div>
              </div>
          </div>
      </div>
      <br>
      <div style="clear: both"></div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>