<?php $__env->startSection('content'); ?>
    <script>
        let pricedata =  <?=$cargo_prices_table?> ;
    </script>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="alert alert-info text-center">
                <?php echo trans('interface.about_rush_order'); ?>

            </div>

            <div class="alert-danger alert text-center">
                <?php echo trans('interface.about_link_info'); ?>

            </div>
        </div>


        <div class="col-md-12 col-sm-12">
            <div class="text-center">
                <ul class="nav nav-pills naw-tabs">
                    <li class="active tabs"><a data-toggle="pill" href="#home"><?php echo e(trans('interface.turkey')); ?></a></li>

                </ul>
            </div>

            <div class="tabs-content">
                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <div class="row">
                            <div class="col-md-3 col-sm-3"><button class="btn btn-block btn-info text-center own-btn-info">Toplam TL: <span id="total_tl">0</span></button></div>
                            <div class="col-md-3 col-sm-3"><button class="btn btn-block btn-info text-center own-btn-info" style="background-color: #d9534f;">Toplam AZN (CBAR): <span id="total_az">0</span></button></div>
                            <div class="col-md-3 col-sm-3"><button class="btn btn-block btn-info text-center own-btn-info" data-toggle="modal" data-target="#myModal"><?php echo e(trans('interface.what_is_rush_order')); ?></button></div>
                            <div class="col-md-3 col-sm-3"><button class="btn btn-block btn-info text-center own-btn-info" data-toggle="modal" data-target="#myModal2"> Türkiyə daxili kargo Nədir ?</button></div>
                        </div>

                        <hr style="margin-bottom: 5px">
                        <div class="row">
                            <div class="col-md-12 cargo_info">
                                <span>Türkiye daxili kargo:</span>
                            </div>
                        </div>
                        <hr style="margin-top: 5px">
                        <div class="row">
                            <?php if(Session::has('msg')): ?>
                                <div class="col-sm-12">
                                    <?php echo Session::get('msg'); ?>

                                </div>
                            <?php endif; ?>

                            <form class="form" method="post" action="<?php echo e(route('addToCart')); ?>">
                                <?php echo e(csrf_field()); ?>

                                <input class="country"  type="hidden" name="country" value="1">

                                <div class="own-order-box">
                                    <div class="col-md-10 price-parent">
                                        <input name="url[]" type="text" class="own-input-style own-url-style js_get_price_and_size" required placeholder="Bura linki yazın" data-uniq="1111">
                                        <input name="own_price[]" type="text"  class="change-price text-center own-prices js_set_price" placeholder="Qiymət" required>
                                        <input class="text-center own-prices" value="+ 5%" disabled style="background-color: #1796fb; color: #ffffff; border-color: #057cda">
                                        <select class="js_set_sizes form-control size_form_control">
                                        </select>

                                        <input name="price[]" type="text" class="update-price text-center own-prices" value="Toplam" readonly>
                                        <span class="btn btn-warning counter" data-c="plus"><span class="glyphicon glyphicon-plus"></span></span>
                                        <input name="count[]" style="width: 30px" class="text-center own-prices count" value="1" readonly>
                                        <span class="btn btn-warning counter" data-c="minus"><span class="glyphicon glyphicon-minus"></span></span>
                                    </div>

                                    <div class="col-md-2 text-right">
                                        <span class="btn btn-success new-link">Yeni link</span>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="own-date" style="display: none">
                                            <spam class="end-time">Dükanın və ya aksiaynın bitmə vaxtı</spam>
                                            <input type="number" name="hour[]" class="text-center" placeholder="saat" min="1" max="23"> <input type="number" min="1" max="59" name="minute[]" class="text-center" placeholder="degige" >&nbsp;
                                            <input type="number" min="1" max="7" name="day[]" class="text-center" placeholder="gun" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Limit 7 gündən artıx ola bilməz">
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <textarea name="note[]" class="form-control js_description" rows="2" required placeholder="Qeydi bura yazın"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="order-box"></div>

                                <div class="col-md-offset-4 col-md-4 col-sm-4 text-center">
                                    <label class="checkbox-inline btn btn-danger"><input name="rush" class="rush-order" type="checkbox" value="1">Təcili Sifariş olunsun</label>
                                </div>


                                <div class="col-md-4 col-sm-4">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                        <label class="control-label">Pul kalkulyatoru</label>
                                        <div class="text-center">
                                            <input id="link_price" type="text" class="form-control" style="font-size: 12px;" maxlength="10" placeholder="TL">
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                        <label class="control-label" style="margin-top: 40px"></label>
                                        <span data-type="azn" class="block" style="color:#e2001a;font-size: 18px;margin-top:10px;font-weight: 600;">
                                        <b id="link_price_result">0.00 AZN</b>
                                        </span>
                                    </div>
                                </div>



                                <div style="clear: both;"><br></div>

                                <div class="col-md-12 col-sm-12">
                                    <button class="btn btn-block btn-success text-uppercase"><?php echo e(trans('interface.add_to_basket')); ?></button>
                                </div>

                            </form>
                        </div>
                    </div>

                    <div id="menu1" class="tab-pane fade">
                        <div class="row">
                            <div class="col-md-3 col-sm-3"><button class="btn btn-block btn-info text-center own-btn-info">Toplam USD: <span id="total_usd">0</span></button></div>
                            <div class="col-md-3 col-sm-3"><button class="btn btn-block btn-info text-center own-btn-info" style="background-color: #d9534f;">Toplam AZN (CBAR): <span id="total_usd_az">0</span></button></div>
                            <div class="col-md-3 col-sm-3"><button class="btn btn-block btn-info text-center own-btn-info" data-toggle="modal" data-target="#myModal"><?php echo e(trans('interface.what_is_rush_order')); ?></button></div>
                            <div class="col-md-3 col-sm-3"><button class="btn btn-block btn-info text-center own-btn-info" data-toggle="modal" data-target="#myModal3">  Amerika daxili kargo Nədir ?</button></div>
                        </div>

                        <hr>
                        <div class="row">

                            <?php if(Session::has('msg')): ?>
                                <div class="col-sm-12">
                                    <?php echo Session::get('msg'); ?>

                                </div>
                            <?php endif; ?>

                            <form class="form" method="post" action="<?php echo e(route('addToCart')); ?>">
                                <?php echo e(csrf_field()); ?>

                                <input class="country" type="hidden" name="country" value="2">

                                <div class="own-order-box">
                                    <div class="col-md-10 price-parent">
                                        <input name="url[]" type="text" class="own-input-style own-url-style js_get_price_and_size" required placeholder="Bura linki yazın">
                                        <input name="own_price[]" type="text"  class="change-price text-center own-prices" placeholder="Qiymət" required>
                                        <input class="text-center own-prices" value="+ 5%" disabled style="background-color: #1796fb; color: #ffffff; border-color: #057cda">
                                        <input name="price[]" type="text" class="update-price text-center own-prices" count="1" value="Toplam" readonly>

                                        <span class="btn btn-warning counter" data-c="plus"><span class="glyphicon glyphicon-plus"></span></span>
                                        <input name="count[]" style="width: 30px" class="text-center own-prices count" value="1" readonly>
                                        <span class="btn btn-warning counter" data-c="minus"><span class="glyphicon glyphicon-minus"></span></span>
                                    </div>

                                    <div class="col-md-2 text-right">
                                        <span class="btn btn-success new-link">Yeni link</span>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="own-date" style="display: none">
                                            <spam class="end-time">Dükanın və ya aksiaynın bitmə vaxtı</spam>
                                            <input type="number" name="hour[]" class="text-center" placeholder="saat" min="1" max="23"> <input type="number" min="1" max="59" name="minute[]" class="text-center" placeholder="degige" >&nbsp;
                                            <input type="number" min="1" max="7" name="day[]" class="text-center" placeholder="gun" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Limit 7 gündən artıx ola bilməz">
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <textarea name="note[]" class="form-control" rows="2" required placeholder="Qeydi bura yazın"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="order-box"></div>

                                <div class="col-md-offset-4 col-sm-offset-4 col-md-4 col-sm-4 text-center">
                                    <label class="checkbox-inline btn btn-danger"><input name="rush" class="rush-order" type="checkbox" value="1">Təcili Sifariş olunsun</label>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                        <label class="control-label">Pul kalkulyatoru</label>
                                        <div class="text-center">
                                            <input id="link_price_usd" type="text" class="form-control" style="font-size: 12px;" maxlength="10" placeholder="USD">
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                        <label class="control-label" style="margin-top: 40px"></label>
                                        <span data-type="azn" class="block" style="color:#e2001a;font-size: 18px;margin-top:10px;font-weight: 600;">
                                        <b id="link_price_result_usd">0.00 AZN</b>
                                        </span>
                                    </div>
                                </div>

                                <div style="clear: both;"><br></div>

                                <div class="col-md-12 col-sm-12">
                                    <button class="btn btn-block btn-success text-uppercase"><?php echo e(trans('interface.add_to_basket')); ?></button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>


    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php echo e(trans('interface.what_is_rush_order')); ?></h4>
                </div>
                <div class="modal-body"><?php echo trans('interface.what_is_rush_order_text'); ?></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal2" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Daxili kargo nədir ?</h4>
                </div>
                <div class="modal-body">
                    <?php echo e(trans('interface.cargo_text')); ?>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Mağaza</th>
                            <th>Limit</th>
                            <th>Kargo</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Trendyol</td>
                            <td>Məbləğdən asılı olmayaraq</td>
                            <td>8.99</td>
                        </tr>
                        <tr>
                            <td>FLO</td>
                            <td>75 TL dən aşağı</td>
                            <td>4.99</td>
                        </tr>
                        <tr>
                            <td>PierreCardin</td>
                            <td>200 TL dən aşağı</td>
                            <td>6.90</td>
                        </tr>
                        <tr>
                            <td>UsPolo</td>
                            <td>150 TL dən aşağı</td>
                            <td>6.90</td>
                        </tr>
                        <tr>
                            <td>LcWaikiki</td>
                            <td>75 TL dən aşağı</td>
                            <td>4.95</td>
                        </tr>
                        <tr>
                            <td>DsDamat</td>
                            <td>150 TL dən aşağı</td>
                            <td>9.00</td>
                        </tr>
                        <tr>
                            <td>Koton</td>
                            <td>100 TL dən aşağı</td>
                            <td>4.90</td>
                        </tr>
                        <tr>
                            <td>Colins</td>
                            <td>100 TL dən aşağı</td>
                            <td>5.90</td>
                        </tr>
                        <tr>
                            <td>Patirti</td>
                            <td>50 TL dən aşağı</td>
                            <td>4.99</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal3" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">  Amerika daxili kargo Nədir ?</h4>
                </div>
                <div class="modal-body">
                    Əksər firmalarda Amerika daxili çatdırılma ödənişli olur (Amerika daxili kargo). Bəzi saytlarda müəyyən limitdən sonra Amerika daxili kargo pulsuz olur.
                    <br><strong>Nəzərinizə çatdıraq ki aşağıdaki cədvəldə qeyd olunan məlumatlar satıcı firmaların saytlarından götürülüb və həmin firmaların iş prinsiplərinə əsasən kargo qiymətləri dəyişiklik göstərə bilər. Biz bu məlumatları mümkün olduğu qədər aktual saxlamağa çalışırıq və daim yeniləyirik.</strong>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Mağaza</th>
                            <th>Limit</th>
                            <th>Kargo</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>