<?php $__env->startSection('content'); ?>
      <div class="row">
          <div class="col-md-12 col-sm-12">
              <div class="white-background">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-center alert alert-danger" style="padding-top: 100px; padding-bottom: 100px;">
                                <?php if(Session::has('msg')): ?>
                                    <?php echo Session::get('msg'); ?>

                                <?php endif; ?>
                            </h2>
                        </div>
                    </div>
              </div>
          </div>
      </div>
      <br>
      <div style="clear: both"></div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>