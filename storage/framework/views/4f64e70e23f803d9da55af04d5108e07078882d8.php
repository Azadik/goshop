<div id="beyanname" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">BƏYANNAMƏ BLANKI TAM VƏ DƏQİQ DOLDURULMALIDIR</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger balert-danger" style="display:none"></div>
                <div class="alert alert-success balert-success text-center text-uppercase" style="display:none"></div>

                <div class="well b_well">
                    <form enctype="multipart/form-data">

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="hidden" name="country" id="country" value="1">

                                <div class="form-group <?php echo e($errors->has('magazin_name') ? ' has-error' : ''); ?>">
                                    <label for="magazin_name">Mağaza adı <span class="own_danger_text">*</span></label>
                                    <input type="text" class="form-control" id="magazin_name" name="magazin_name" value="<?php echo e(old('magazin_name')); ?>">
                                    <?php if($errors->has('magazin_name')): ?>
                                        <span class="help-block">
                                    <?php echo e($errors->first('magazin_name')); ?>

                                </span>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group">
                                    <label for="tracking_number">Sifariş izləmə kodu</label>
                                    <input type="text" class="form-control" id="tracking_number" name="tracking_number" placeholder="Sifarişiniz Türkiyə ofisinə təslim edildikdən sonra əlavə edilməlidir">
                                </div>

                                <div class="form-group">
                                    <label for="date">Sifariş tarixi</label>
                                    <input type="text" class="form-control" id="date" name="date">
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="mehsul_type">Məhsul növü <span class="own_danger_text">*</span></label>
                                    <input type="text" class="form-control" name="mehsul_type" id="mehsul_type" required>
                                </div>

                                <div class="form-group <?php echo e($errors->has('price') ? ' has-error' : ''); ?>">
                                    <label for="price">Məhsulun qiyməti <span class="own_danger_text">*</span></label>
                                    <div class="input-group" style="width: 100%;">
                                        <input class="form-control" name="price" id="price" type="text" value="<?php echo e(old('price')); ?>">
                                        <span class="input-group-btn" style="width: 80px;">
                                    <select class="form-control" name="price_type" id="price_type">
                                        <option value="1">USD</option>
                                        <option value="2">EUR</option>
                                        <option value="3" selected="selected">TRY</option>
                                    </select>
                                </span>
                                    </div>
                                    <?php if($errors->has('price')): ?>
                                        <span class="help-block">
                                    <?php echo e($errors->first('price')); ?>

                                </span>
                                    <?php endif; ?>
                                </div>

                                <label>Invois (jpeg,jpg,png) <span class="own_danger_text">*</span></label>
                                <div class="form-group <?php echo e($errors->has('img') ? ' has-error' : ''); ?>">
                                    <input type="file" name="img" id="img" class="form-control">
                                    <?php if($errors->has('img')): ?>
                                        <span class="help-block">
                                    <?php echo e($errors->first('img')); ?>

                                </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="text-danger">Təslimat nömrəsi, məhsul barədə məlumatı və bağlamada neçə malınız varsa bura yazın</label>
                                    <textarea class="form-control" rows="6" id="description" name="description" placeholder="Bağlamanıza aid qeydlərinizidə bura yazın"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 text-left"><button class="btn btn-success" id="submit"><?php echo e(trans('interface.send')); ?></button></div>
                    </form>
                        <div class="col-md-6 col-sm-6 col-xs-6 text-right"><button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo e(trans('interface.close')); ?></button></div>

                    <div style="clear: both"></div>
                </div>
            </div>
        </div>

    </div>
</div>