<?php $__env->startSection('content'); ?>
            <h2 class="own-header text-center"><?php echo $page->$title; ?></h2>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <?php echo $page->$text; ?>

                </div>
            </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>