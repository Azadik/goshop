<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12 col-sm-12 text-center"><h1><?php echo e(trans('interface.my_orders')); ?></h1></div>
        <?php echo $__env->make('user.content_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div style="clear: both"><br></div>


        <div class="col-md-12 col-sm-12">
            <div class="order_show_page">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <?php if(Session::has('msg')): ?>
                            <?php echo Session::get('msg'); ?>

                        <?php endif; ?>
                        <div id="d_o"></div>
                    </div>

                    <div class="col-md-12 col-sm-12">
                        <div class="row">
                            <?php $__currentLoopData = $tm_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tm_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-md-3 col-sm-3 col-xs-12" style="margin-bottom: 5px;">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <?php if($tm_product->img): ?>
                                                <a href="<?php echo e($tm_product->url); ?>" target="_blank" title="<?php if($tm_product->title): ?> <?php echo e($tm_product->url); ?>  <?php endif; ?>">
                                                <img src="<?php echo e(asset('products')); ?>/<?php echo e($tm_product->img); ?>" class="img-rounded" alt="<?php echo e($tm_product->img); ?>" width="100%">
                                                </a>
                                            <?php else: ?>
                                                <a href="<?php echo e($tm_product->url); ?>" target="_blank" title="<?php if($tm_product->title): ?> <?php echo e($tm_product->url); ?>  <?php endif; ?>">
                                                <img src="<?php echo e(asset('products/no_img.png')); ?>" class="img-rounded" width="100%">
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="text-center">
                                            <?php if($tm_product->insurance): ?>
                                                <img src="<?php echo e(asset('own/crown.png')); ?>" title="Bu məhsul sığortalanıb"><br>
                                            <?php endif; ?>
                                            <?php if(!$tm_product->insurance): ?>

                                                <input class="check_insurance" type="checkbox" name="type" value="<?php echo e($tm_product->id); ?>" title="Sığortalamaq üçün klik edin">
                                            <?php endif; ?>
                                            </div>
                                            
                                            <div class="progress">
                                                <?php $random_progressbar = rand(25, 90); ?>
                                                <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo e($random_progressbar); ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo e($random_progressbar); ?>%">
                                                    <?php echo e($random_progressbar); ?>% Process
                                                </div>
                                            </div>
                                            <kbd><?php echo e(trans('interface.code')); ?>: <?php echo e($tm_product->id); ?></kbd>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <br>

                        <ul class="nav nav-tabs own_tabs_menu">
                            <li class="active">
                                <a data-toggle="tab" href="#home">
                                    <span class="mobile_orders_icons">
                                        <img src="<?php echo e(asset('own/ordered.png')); ?>">
                                        <span class="label label-primary"><?php echo e($count_sifarish); ?></span>
                                    </span>
                                    <span class="desktop_orders_text"><?php echo e(trans('interface.ordered')); ?>

                                        <span class="label label-primary"><?php echo e($count_sifarish); ?></span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#menu1">
                                   <span class="mobile_orders_icons">
                                        <img src="<?php echo e(asset('own/warehouse.png')); ?>">
                                        <span class="label label-primary"><?php echo e($count_in_baku); ?></span>
                                   </span>

                                    <span class="desktop_orders_text"><?php echo e(trans('interface.warehouse')); ?>

                                        <span class="label label-primary"><?php echo e($count_in_baku); ?></span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#menu2">
                                    <span class="mobile_orders_icons">
                                        <img src="<?php echo e(asset('own/over_handed.png')); ?>">
                                        <span class="label label-primary"><?php echo e($count_tehvil); ?></span>
                                    </span>
                                    <span class="desktop_orders_text"><?php echo e(trans('interface.was_handed_over')); ?>

                                        <span class="label label-primary"><?php echo e($count_tehvil); ?></span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <br>

                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="row order-box-parent">
                                        <div class="col-md-12 col-sm-12 order-box">
                                            <div class="col-md-12 col-sm-12 order-box-header">
                                                <div class="col-md-6 col-sm-6">
                                                    <strong><?php echo e(trans('interface.shop')); ?>:
                                                        <?php if($boutique_name[$key]): ?>
                                                            <span class="package_name_color"><?php echo e($boutique_name[$key]->name); ?></span> | <?php echo e(trans('interface.package_code')); ?>: <?php echo e($baglamalar[$key]->id); ?>

                                                        <?php endif; ?>
                                                    </strong>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <strong><?php echo e(trans('interface.country')); ?>:
                                                        <?php
                                                        foreach ($product as $p){
                                                            if($p->country == 1){
                                                                echo trans('interface.turkey');
                                                            }else{
                                                                echo trans('interface.usa');
                                                            }
                                                            break;
                                                        }
                                                        ?>
                                                    </strong>
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-sm-2 order-box-img">
                                                <img src="<?php echo e(asset('own/order.png')); ?>" height="79" class="img-rounded" alt="Cinque Terre">
                                                <div class="order-count"><?php echo e($counter[$key]); ?></div>
                                            </div>
                                            <div class="col-md-10 col-sm-10 order-box-content">
                                                <table class="table table-bordered order-box-body-table">
                                                    <thead>
                                                    <tr>
                                                        <th><?php echo e(trans('interface.date')); ?></th>
                                                        <th><?php echo e(trans('interface.price')); ?></th>
                                                        <th><?php echo e(trans('interface.weight')); ?></th>
                                                        <th><?php echo e(trans('interface.delivery_fee')); ?></th>
                                                        <th><?php echo e(trans('interface.dimensions')); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td><?php if($baglamalar[$key]): ?> <?php echo e($baglamalar[$key]->date); ?> <?php endif; ?></td>
                                                        <td>
                                                            <?php
                                                            $total_price = 0.00;
                                                            foreach ($product as $p){
                                                                    $p->cargo_az ? $cargopr = $p->cargo_az : $cargopr = 0;
                                                                    $total_price = ($p->price_az * $p->count ) + $total_price  + $cargopr;
                                                            }
                                                            echo   sprintf("%.2f" . "", $total_price). ' AZN';
                                                            ?>
                                                        </td>

                                                        <?php if($baglamalar[$key]): ?>
                                                            <td><?php echo e($baglamalar[$key]->weight); ?></td>
                                                            <td><?php if($baglamalar[$key]->shipping_price): ?> <?php echo e($baglamalar[$key]->shipping_price); ?> <?php else: ?> 0 <?php endif; ?> $ </td>
                                                            <td><?php echo e($baglamalar[$key]->size); ?></td>
                                                        <?php else: ?>
                                                            <td colspan="3" class="text-center"><?php echo e(trans('interface.will_show_warehouse')); ?></td>
                                                        <?php endif; ?>

                                                    </tr>
                                                    </tbody>
                                                </table>

                                                <table class="table table-bordered order-box-footer-table">
                                                    <tbody>
                                                    <tr>
                                                        <td class="show-product text-center"><?php echo e(trans('interface.show_products_inside_package')); ?></td>
                                                        <td class="text-center a baglama_id" baglama-id="<?php echo e($baglamalar[$key]->id); ?>"> <?php echo e(trans('interface.package_moving')); ?></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 product-list" style="display: none">
                                            <table class="table table-bordered products-in-order">
                                                <thead>
                                                <tr>
                                                    <th class="text-center"><?php echo e(trans('interface.number')); ?></th>
                                                    <th><?php echo e(trans('interface.photo')); ?></th>
                                                    <th><?php echo e(trans('interface.url')); ?></th>
                                                    <th><span class="content-hide-in-mobile"><?php echo e(trans('interface.client_note')); ?></span><span class="content-hide-in-desktop"><?php echo e(trans('interface.note')); ?></span></th>
                                                    <th class="text-center"><?php echo e(trans('interface.count')); ?></th>
                                                    <th><?php echo e(trans('interface.price')); ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_show): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($product_show->lost): ?><tr class="alert-danger alert"><td colspan="6" class="text-center">Bu mehsul anbara chatmayib ararshdirilir</td></tr><?php endif; ?>
                                                    <tr <?php if($product_show->lost): ?> class="alert-danger alert" <?php endif; ?>>
                                                        <td class="text-center">
                                                            <?php if($product_show->insurance): ?>
                                                                <img src="<?php echo e(asset('own/crown.png')); ?>" title="Bu məhsul sığortalanıb"><br>
                                                            <?php endif; ?>
                                                            <?php echo e($product_show->id); ?>

                                                            <br>
                                                            <?php if(!$product_show->insurance): ?>
                                                                <input class="check_insurance" type="checkbox" name="type" value="<?php echo e($product_show->id); ?>">
                                                            <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <?php if($product_show->img): ?>
                                                                <img src="<?php echo e(asset('products')); ?>/<?php echo e($product_show->img); ?>" class="img-rounded" width="80">
                                                            <?php else: ?>
                                                                <img src="<?php echo e(asset('products/no_img.png')); ?>" class="img-rounded" width="80">
                                                            <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <a href="<?php echo e($product_show->url); ?>" target="_blank">
                                                                  <span class="content-hide-in-mobile">
                                                                      <?php if($product_show->title): ?>
                                                                          <?php echo e($product_show->title); ?>

                                                                      <?php else: ?>
                                                                          <?php echo substr($product_show->url, 0, 35) ?> ...
                                                                      <?php endif; ?>
                                                                  </span>
                                                                <span class="content-hide-in-desktop">link</span>
                                                            </a>
                                                        </td>
                                                        <td><?php echo e($product_show->note); ?></td>
                                                        <td class="text-center"><?php echo e($product_show->count); ?></td>
                                                        <td>
                                                            <?php
                                                                $price = $product_show->price_az * $product_show->count;
                                                                echo   sprintf("%.2f" . "", $price). ' AZN';
                                                            ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <hr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>

                            <div id="menu1" class="tab-pane fade">
                                <?php $__currentLoopData = $products_b; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="row order-box-parent">
                                        <div class="col-md-12 col-sm-12 order-box">
                                            <div class="col-md-12 col-sm-12 order-box-header">
                                                <div class="col-md-6 col-sm-6">
                                                    <strong><?php echo e(trans('interface.shop')); ?>:
                                                        <?php if($boutique_name_b[$key]): ?>
                                                            <span class="package_name_color"><?php echo e($boutique_name_b[$key]->name); ?></span> | <?php echo e(trans('interface.package_code')); ?>: <?php echo e($baglamalar_b[$key]->id); ?>

                                                        <?php endif; ?>
                                                    </strong>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <strong><?php echo e(trans('interface.country')); ?>:
                                                        <?php
                                                        foreach ($product as $p){
                                                            if($p->country == 1){
                                                                echo trans('interface.turkey');
                                                            }else{
                                                                echo trans('interface.usa');
                                                            }
                                                            break;
                                                        }
                                                        ?>
                                                    </strong>
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-sm-2 order-box-img">
                                                <img src="<?php echo e(asset('own/order.png')); ?>" height="79" class="img-rounded" alt="Cinque Terre">
                                                <div class="order-count"><?php echo e($counter_b[$key]); ?></div>
                                            </div>
                                            <div class="col-md-10 col-sm-10 order-box-content">
                                                <table class="table table-bordered order-box-body-table">
                                                    <thead>
                                                    <tr>
                                                        <th><?php echo e(trans('interface.date')); ?></th>
                                                        <th><?php echo e(trans('interface.price')); ?></th>
                                                        <th><?php echo e(trans('interface.weight')); ?></th>
                                                        <th><?php echo e(trans('interface.delivery_fee')); ?></th>
                                                        <th><?php echo e(trans('interface.dimensions')); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td><?php if($baglamalar_b[$key]): ?> <?php echo e($baglamalar_b[$key]->date); ?> <?php endif; ?></td>
                                                        <td>
                                                            <?php
                                                            $total_price = 0.00;
                                                            foreach ($product as $p){
                                                                $p->cargo_az ? $cargopr = $p->cargo_az : $cargopr = 0;
                                                                $total_price = ($p->price_az * $p->count ) + $total_price  + $cargopr;
                                                            }
                                                            echo   sprintf("%.2f" . "", $total_price). ' AZN';
                                                            ?>
                                                        </td>

                                                        <?php if($baglamalar_b[$key]): ?>
                                                            <td><?php echo e($baglamalar_b[$key]->weight); ?></td>
                                                            <td>
                                                                <?php if($baglamalar_b[$key]->shipping_price): ?> <?php echo e($baglamalar_b[$key]->shipping_price); ?> <?php else: ?> 0 <?php endif; ?> $</td>
                                                            <td><?php echo e($baglamalar_b[$key]->size); ?></td>
                                                        <?php else: ?>
                                                            <td colspan="3" class="text-center"><?php echo e(trans('interface.will_show_warehouse')); ?></td>
                                                        <?php endif; ?>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                                <table class="table table-bordered order-box-footer-table">
                                                    <tbody>
                                                    <tr>
                                                        <td class="show-product text-center"><?php echo e(trans('interface.show_products_inside_package')); ?></td>
                                                        <td class="text-center a baglama_id" baglama-id="<?php echo e($baglamalar_b[$key]->id); ?>"> <?php echo e(trans('interface.package_moving')); ?></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 product-list" style="display: none">
                                            <table class="table table-bordered products-in-order">
                                                <thead>
                                                <tr>
                                                    <th class="text-center"><?php echo e(trans('interface.number')); ?></th>
                                                    <th><?php echo e(trans('interface.photo')); ?></th>
                                                    <th><?php echo e(trans('interface.url')); ?></th>
                                                    <th><span class="content-hide-in-mobile"><?php echo e(trans('interface.client_note')); ?></span><span class="content-hide-in-desktop"><?php echo e(trans('interface.note')); ?></span></th>
                                                    <th class="text-center"><?php echo e(trans('interface.count')); ?></th>
                                                    <th><?php echo e(trans('interface.price')); ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_show): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td class="text-center">
                                                            <?php if($product_show->insurance): ?>
                                                                <img src="<?php echo e(asset('own/crown.png')); ?>" title="Bu məhsul sığortalanıb"><br>
                                                            <?php endif; ?>
                                                            <?php echo e($product_show->id); ?>

                                                            <br>
                                                            <?php if(!$product_show->insurance): ?>
                                                                <input class="check_insurance" type="checkbox" name="type" value="<?php echo e($product_show->id); ?>">
                                                            <?php endif; ?>
                                                        </td>
                                                        <td><img src="<?php echo e(asset('products')); ?>/<?php echo e($product_show->img); ?>" class="img-rounded" width="80"></td>
                                                        <td>
                                                            <a href="<?php echo e($product_show->url); ?>" target="_blank">
                                                                  <span class="content-hide-in-mobile">
                                                                      <?php if($product_show->title): ?>
                                                                          <?php echo e($product_show->title); ?>

                                                                      <?php else: ?>
                                                                          <?php echo substr($product_show->url, 0, 35) ?> ...
                                                                      <?php endif; ?>
                                                                  </span>
                                                                <span class="content-hide-in-desktop"><?php echo e(trans('interface.url')); ?></span>
                                                            </a>
                                                        </td>
                                                        <td><?php echo e($product_show->note); ?></td>
                                                        <td class="text-center"><?php echo e($product_show->count); ?></td>
                                                        <td>
                                                            <?php
                                                            $price = $product_show->price_az * $product_show->count;
                                                            echo   sprintf("%.2f" . "", $price). ' AZN';
                                                            ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <hr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>

                            <div id="menu2" class="tab-pane fade">
                                <?php $__currentLoopData = $products_t; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="row order-box-parent">
                                            <div class="col-md-12 col-sm-12 order-box">
                                                <div class="col-md-12 col-sm-12 order-box-header">
                                                    <div class="col-md-6 col-sm-6">
                                                        <strong><?php echo e(trans('interface.shop')); ?>:
                                                            <?php if($boutique_name_t[$key]): ?>
                                                                <span class="package_name_color"><?php echo e($boutique_name_t[$key]->name); ?></span> | <?php echo e(trans('interface.package_code')); ?>: <?php echo e($baglamalar_t[$key]->id); ?>

                                                            <?php endif; ?>
                                                        </strong>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <strong><?php echo e(trans('interface.country')); ?>:
                                                            <?php
                                                            foreach ($product as $p){
                                                                if($p->country == 1){
                                                                    echo trans('interface.turkey');
                                                                }else{
                                                                    echo trans('interface.usa');
                                                                }
                                                                break;
                                                            }
                                                            ?>
                                                        </strong>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 col-sm-2 order-box-img">
                                                    <img src="<?php echo e(asset('own/order.png')); ?>" height="79" class="img-rounded" alt="Cinque Terre">
                                                    <div class="order-count"><?php echo e($counter_t[$key]); ?></div>
                                                </div>
                                                <div class="col-md-10 col-sm-10 order-box-content">
                                                    <table class="table table-bordered order-box-body-table">
                                                        <thead>
                                                        <tr>
                                                            <th><?php echo e(trans('interface.date')); ?></th>
                                                            <th><?php echo e(trans('interface.price')); ?></th>
                                                            <th><?php echo e(trans('interface.weight')); ?></th>
                                                            <th><?php echo e(trans('interface.delivery_fee')); ?></th>
                                                            <th><?php echo e(trans('interface.dimensions')); ?></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><?php if($baglamalar_t[$key]): ?> <?php echo e($baglamalar_t[$key]->date); ?> <?php endif; ?></td>
                                                            <td>
                                                                <?php
                                                                $total_price = 0.00;
                                                                foreach ($product as $p){
                                                                    $p->cargo_az ? $cargopr = $p->cargo_az : $cargopr = 0;
                                                                    $total_price = ($p->price_az * $p->count ) + $total_price  + $cargopr;
                                                                }
                                                                echo   sprintf("%.2f" . "", $total_price). ' AZN';
                                                                ?>
                                                            </td>

                                                            <?php if($baglamalar_t[$key]): ?>
                                                                <td><?php echo e($baglamalar_t[$key]->weight); ?></td>
                                                                <td>
                                                                    <?php if($baglamalar_t[$key]->shipping_price): ?> <?php echo e($baglamalar_t[$key]->shipping_price); ?> <?php else: ?> 0 <?php endif; ?> $</td>
                                                                <td><?php echo e($baglamalar_t[$key]->size); ?></td>
                                                            <?php else: ?>
                                                                <td colspan="3" class="text-center"><?php echo e(trans('interface.will_show_warehouse')); ?></td>
                                                            <?php endif; ?>

                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                    <table class="table table-bordered order-box-footer-table">
                                                        <tbody>
                                                        <tr>
                                                            <td class="show-product text-center"><?php echo e(trans('interface.show_products_inside_package')); ?></td>
                                                            <td class="text-center a baglama_id" baglama-id="<?php echo e($baglamalar_t[$key]->id); ?>"> <?php echo e(trans('interface.package_moving')); ?></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12 product-list" style="display: none">
                                                <table class="table table-bordered products-in-order">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center"><?php echo e(trans('interface.number')); ?></th>
                                                        <th><?php echo e(trans('interface.photo')); ?></th>
                                                        <th><?php echo e(trans('interface.url')); ?></th>
                                                        <th><span class="content-hide-in-mobile"><?php echo e(trans('interface.client_note')); ?></span><span class="content-hide-in-desktop"><?php echo e(trans('interface.note')); ?></span></th>
                                                        <th class="text-center"><?php echo e(trans('interface.count')); ?></th>
                                                        <th><?php echo e(trans('interface.price')); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_show): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr>
                                                            <td class="text-center"><?php echo e($product_show->id); ?></td>
                                                            <td><img src="<?php echo e(asset('products')); ?>/<?php echo e($product_show->img); ?>" class="img-rounded" width="80"></td>
                                                            <td>
                                                                <a href="<?php echo e($product_show->url); ?>" target="_blank">
                                                                  <span class="content-hide-in-mobile">
                                                                      <?php if($product_show->title): ?>
                                                                          <?php echo e($product_show->title); ?>

                                                                      <?php else: ?>
                                                                          <?php echo substr($product_show->url, 0, 35) ?> ...
                                                                      <?php endif; ?>
                                                                  </span>
                                                                    <span class="content-hide-in-desktop"><?php echo e(trans('interface.url')); ?></span>
                                                                </a>
                                                            </td>
                                                            <td><?php echo e($product_show->note); ?></td>
                                                            <td class="text-center"><?php echo e($product_show->count); ?></td>
                                                            <td>
                                                                <?php
                                                                $price = $product_show->price_az * $product_show->count;
                                                                echo   sprintf("%.2f" . "", $price). ' AZN';
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                    </div>
                                    <hr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Order edit -->
        <div class="modal fade" id="myOrderEdit" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button class="btn btn-danger btn-sm own-close" data-dismiss="modal">&times;</button>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>

        <!-- Package tracking -->
        <div class="modal fade" id="showTracking" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center"><?php echo e(trans('interface.package_moving')); ?></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo e(trans('interface.close')); ?></button>
                    </div>
                </div>
            </div>
        </div>


        <!-- Sigorta -->
        <div class="modal fade" id="insurance_modal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content insurance_data">
                </div>
            </div>
        </div>

        <?php echo $__env->make('user.beyanname', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div style="clear: both"><br></div>
    </div>

    <button class="btn btn-danger insurance_button">Sığortala</button>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>