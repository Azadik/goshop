<div class="text-center">
    <h2 class="own-header"><?php echo e(trans('interface.country_rates')); ?></h2>
    <br>
    <div class="row">
        <div class="col-sm-6">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel">
                <div class="media">
                    <div class="media-left">
                        <img src="<?php echo e(asset('own/usa.png')); ?>" class="media-object" width="100">
                    </div>
                    <div class="media-body" style="padding-top: 15px;">
                        <div class="col-lg-9 col-md-9 col-sm-12 text-left own-font-size"><strong>0,1 kq dan - 0,5 kq dək</strong></div>
                        <div class="col-lg-3 col-md-3 col-sm-12 own-font-size"><strong class="price">5.10 $</strong></div>
                        <div class="col-lg-9 col-md-9 col-sm-12 text-left own-font-size"><strong>0,5 kq dan - 1 kq dək</strong></div>
                        <div class="col-lg-3 col-md-3 col-sm-12 own-font-size"><strong class="price">8.80 $</strong></div>
                    </div>
                </div>
                <p class="text-left own-font-size">
                    <strong><?php echo e(trans('interface.package_info')); ?></strong>
                </p>
            </div>

        </div>
        <div class="col-sm-6">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel">
                <div class="media">
                    <div class="media-left">
                        <img src="<?php echo e(asset('own/tr.png')); ?>" class="media-object" width="100">
                    </div>
                    <div class="media-body" style="padding-top: 15px;">
                        
                        
                        
                        

                        <div class="col-lg-9 col-md-9 col-sm-12 text-left own-font-size"><strong>0 kq dan - 0,25 kq dək</strong></div>
                        <div class="col-lg-3 col-md-3 col-sm-12 own-font-size"><strong class="price">2.50 $</strong></div>
                        <div class="col-lg-9 col-md-9 col-sm-12 text-left own-font-size"><strong>0,25 kq dan - 0,5 kq dək</strong></div>
                        <div class="col-lg-3 col-md-3 col-sm-12 own-font-size"><strong class="price">3.70 $</strong></div>
                        <div class="col-lg-9 col-md-9 col-sm-12 text-left own-font-size"><strong>0,5 kq dan - 1 kq dək</strong></div>
                        <div class="col-lg-3 col-md-3 col-sm-12 own-font-size"><strong class="price">5.90 $</strong></div>
                    </div>
                </div>
                <p class="text-left own-font-size">
                    <strong><?php echo e(trans('interface.package_info')); ?></strong>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="text-center">
    <div class="row">
        <div class="col-sm-6">
            <h2 class="own-header"><?php echo e(trans('interface.calculator')); ?></h2>
            <br>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center panel padding-top-15 padding-bottom-15">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 margin-bottom-10">
                    <label class="control-label own-label" for="country"><?php echo trans('interface.country'); ?></label>
                    <div class="text-center">
                        <select class="form-control" id="country">
                            <option value="1"><?php echo trans('interface.turkey'); ?></option>
                            <option value="2"><?php echo trans('interface.america'); ?></option>
                        </select>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 margin-bottom-10">
                    <label class="control-label own-label" for="package"><?php echo trans('interface.package_count'); ?></label>
                    <input type="text" class="form-control" id="package" required="" onkeypress="validate(event)">
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 margin-bottom-10">
                    <label class="control-label own-label" for="length"><?php echo trans('interface.length'); ?></label>
                    <div class="input-group" style="width: 100%;">
                        <input type="text" class="form-control" placeholder="" id="length" required="">
                        <span class="input-group-btn" style="width: 100px;">
                                <select class="form-control" id="length_etalon">
                                    <option value="cm"><?php echo trans('interface.sm'); ?></option>
                                    <option value="inch"><?php echo trans('interface.inch'); ?></option>
                                </select>
                            </span>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 margin-bottom-10">
                    <label class="control-label own-label" for="width"><?php echo trans('interface.width'); ?></label>
                    <div class="input-group" style="width: 100%;">
                        <input type="text" class="form-control" placeholder="" id="width" required="">
                        <span class="input-group-btn" style="width: 100px;">
                                <select class="form-control" id="width_etalon">
                                    <option value="cm"><?php echo trans('interface.sm'); ?></option>
                                    <option value="inch"><?php echo trans('interface.inch'); ?></option>
                                </select>
                            </span>
                    </div>
                </div>


                <div data-hide="1" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 margin-bottom-10">
                    <label class="control-label own-label" for="height"><?php echo trans('interface.height'); ?></label>
                    <div class="input-group" style="width: 100%;">
                        <input type="text" class="form-control" id="height" placeholder="" required="">
                        <span class="input-group-btn" style="width: 100px;">
                                <select class="form-control" id="height_etalon">
                                    <option value="cm"><?php echo trans('interface.sm'); ?></option>
                                    <option value="inch"><?php echo trans('interface.inch'); ?></option>
                                </select>
                            </span>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 margin-bottom-10">
                    <label class="control-label own-label" for="wght"><?php echo trans('interface.weight'); ?></label>
                    <div class="input-group" style="width: 100%;">
                        <input type="text" class="form-control" id="weight" placeholder="" required="">
                        <span class="input-group-btn" style="width: 100px;">
                                <select class="form-control" id="weight_etalon">
                                    <option value="kg"><?php echo trans('interface.kg'); ?></option>
                                </select>
                            </span>
                    </div>
                </div>



                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center margin-top-25 padding-bottom-15">
                        <span style="color:#737373;font-size: 18px;">
                            <b><?php echo trans('interface.total'); ?></b>
                        </span>
                    <span style="color:#e2001a;font-size: 18px;">
                            <b id="t_price">$0,00</b>
                        </span>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <button id="calculate" class="text-uppercase btn" style="width: 200px;"><?php echo trans('interface.calculate'); ?></button>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <h2 class="own-header"><?php echo e(trans('interface.link_calculate')); ?></h2>
            <br>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center panel padding-top-15">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                    <label class="control-label"><?php echo trans('interface.amount'); ?> <?php echo trans('interface.tl'); ?></label>
                    <div class="text-center">
                        <input id="link_price" type="text" class="form-control" style="font-size: 12px;" placeholder="<?php echo trans('interface.write_number'); ?>" maxlength="10">
                        <span class="block" style="color: #e2001a;font-size:15px;display: none;line-height: 18px;font-weight: 600;">Türkiyə daxili  pulu hesablana bilər (7.99 TL - 9.99 TL)</span>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 " style="padding-top: 28px;">
                    <label class="control-label"><?php echo trans('interface.amount_to_pay'); ?></label>
                    <span data-type="azn" class="block" style="color:#e2001a;font-size: 18px;margin-top:10px;font-weight: 600;">
                                <b id="link_price_result">0.00 AZN</b>
                            </span>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3 style="font-size: 18px;"><?php echo e(trans('interface.info')); ?></h3>
                    <p style="color:#737373;line-height: 18px;"><?php echo e(trans('interface.info_text')); ?></p>

                    <div style="display: inline-block;width: 100%;border-bottom: 2px dotted #e0e0dd;margin:15px 0;"></div>

                    <h3 style="font-size: 18px;"><?php echo e(trans('interface.how_calculate')); ?></h3>
                    <p style="color:#737373;line-height: 18px;"><?php echo e(trans('interface.how_calculate_text')); ?></p>
                    <p style="color:#737373;line-height: 18px;" class="">
                        <label class="label label-danger text-uppercase" style="background: #e2001a;padding-bottom: 2px;"><?php echo e(trans('interface.note')); ?></label><?php echo e(trans('interface.note_text')); ?></p>
                </div>

            </div>
        </div>
    </div>
    <hr>
</div>