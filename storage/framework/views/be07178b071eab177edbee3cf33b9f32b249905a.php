<?php $__env->startSection('content'); ?>
      <div class="row">
          <div class="col-md-12 col-sm-12 text-center"><h1><?php echo e(trans('interface.questions')); ?></h1></div>
          <div class="col-md-12 col-sm-12">
              <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-6">
                      <button class="btn btn-success" data-toggle="modal" data-target="#myModal">
                          <i class="glyphicon glyphicon-plus"></i> Sorğu yarat
                      </button>
                      <button class="btn btn-success" data-toggle="modal" data-target="#beyanname">Bəyannamə Əlavə Et</button>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                      <button class="btn btn-info mobile_hide_customer_code"><?php echo e(trans('interface.customer_code')); ?>: <?php echo e(Auth::user()->reference_code); ?></button>
                      <a href="<?php echo e(route('addOrder')); ?>" class="btn btn-danger">
                          <span class="glyphicon glyphicon-plus"></span> <?php echo e(trans('interface.order_it')); ?>

                      </a>
                  </div>
              </div>
          </div>

          <?php if($errors->any()): ?>
          <div class="col-md-12 col-sm-12">
              <div class="row">
                  <div class="col-md-12">
                          <div class="alert alert-danger">
                              <ul>
                                  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <li><?php echo e($error); ?></li>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              </ul>
                          </div>
                  </div>
              </div>
          </div>
          <?php endif; ?>

          <div style="clear: both"><br></div>

          <div class="col-md-12 col-sm-12">
              <div class="white-background">
                  <div class="row">
                      <div class="col-md-12 col-sm-12">
                          <?php if(Session::has('msg')): ?>
                              <?php echo Session::get('msg'); ?>

                          <?php endif; ?>
                      </div>

                      <div class="col-md-12 col-sm-12">
                          <table class="table">
                              <thead>
                              <tr>
                                  <th>№</th>
                                  <th><?php echo e(trans('interface.title')); ?></th>
                                  <th class="desktop_questions_text"><?php echo e(trans('interface.date')); ?></th>
                                  <th class="text-center desktop_questions_text"><?php echo e(trans('interface.date_finish')); ?></th>
                                  <th class="text-center"><?php echo e(trans('interface.look')); ?></th>
                              </tr>
                              </thead>
                              <tbody>

                              <?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $question): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <tr>
                                  <td><?php echo e($question->id); ?></td>
                                  <td><?php echo e($question->title); ?></td>
                                  <td class="desktop_questions_text"><?php echo e($question->start_date); ?></td>
                                  <td class="text-center desktop_questions_text">
                                      <?php if($question->status == 1): ?>
                                          <?php echo e($question->finish_date); ?>

                                       <?php else: ?>
                                          <button data-id="<?php echo e($question->id); ?>" class="btn btn-success btn-sm finishQuestion" style="padding: 0px 4px;">
                                              <span class="glyphicon glyphicon-ok"></span> Bitir
                                          </button>
                                      <?php endif; ?>
                                  </td>
                                  <td  class="text-center">
                                      <a href="<?php echo e(route('get.question', $question->id)); ?>" class="btn btn-info btn-sm" style="padding: 0px 4px;">
                                          <span class="glyphicon glyphicon-eye-open"></span>
                                      </a>
                                  </td>
                              </tr>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              </tbody>
                          </table>

                          <div class="text-center">
                              <?php echo e($questions->links()); ?>

                          </div>
                      </div>

                  </div>
              </div>
          </div>
          <div style="clear: both"><br></div>
      </div>


      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title"><?php echo e(trans('interface.create_new_question')); ?></h4>
                  </div>
                  <div class="modal-body">
                      <form action="<?php echo e(route('create.question')); ?>" method="post" enctype="multipart/form-data">
                          <?php echo e(csrf_field()); ?>


                          <div class="row">
                              <div class="col-md-6 col-sm-6 col-xs-12">

                                  <div class="form-group <?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                                      <label for="title"><?php echo e(trans('interface.title')); ?> <span class="own_danger_text">*</span></label>
                                      <input type="text" class="form-control" id="title" name="title" required value="<?php echo e(old('title')); ?>">
                                      <?php if($errors->has('title')): ?>
                                          <span class="help-block">
                                            <strong><?php echo e($errors->first('title')); ?></strong>
                                          </span>
                                      <?php endif; ?>
                                  </div>

                                  <div class="form-group">
                                      <label for="prioritet_id"><?php echo e(trans('interface.prioritet')); ?> <span class="own_danger_text">*</span></label>
                                      <select class="form-control" id="prioritet_id" name="prioritet_id">
                                          <?php $__currentLoopData = $prioritets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prioritet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                              <option value="<?php echo e($prioritet->id); ?>"><?php echo e($prioritet->$title); ?></option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      </select>
                                  </div>


                                  <div class="form-group">
                                      <label for="link"><?php echo e(trans('interface.url')); ?></label>
                                      <input type="text" class="form-control" id="link" name="link" value="<?php echo e(old('link')); ?>">
                                  </div>

                                  <div class="form-group <?php echo e($errors->has('img') ? ' has-error' : ''); ?>">
                                      <label><?php echo e(trans('interface.file_upload')); ?></label>
                                      <div class="input-group">
                                            <span class="input-group-btn">
                                                <span class="btn btn-default btn-file">
                                                    <?php echo e(trans('interface.browse')); ?>… <input type="file" name="img" id="imgInp">
                                                </span>
                                            </span>
                                          <input type="text" class="form-control" readonly>
                                      </div>
                                      <img id='img-upload'/>

                                      <?php if($errors->has('img')): ?>
                                          <span class="help-block">
                                            <strong><?php echo e($errors->first('img')); ?></strong>
                                          </span>
                                      <?php endif; ?>
                                  </div>

                              </div>

                              <div class="col-md-6 col-sm-6 col-xs-12">
                                  <div class="form-group">
                                      <label for="movzu_id"><?php echo e(trans('interface.theme')); ?> <span class="own_danger_text">*</span></label>
                                      <select class="form-control" id="movzu_id" name="movzu_id">
                                          <?php $__currentLoopData = $movzu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $movz): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                              <option value="<?php echo e($movz->id); ?>"><?php echo e($movz->$title); ?></option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      </select>
                                  </div>

                                  <div class="form-group <?php echo e($errors->has('text') ? ' has-error' : ''); ?>">
                                      <label for="comment"><?php echo e(trans('interface.your_question')); ?> <span class="own_danger_text">*</span></label>
                                      <textarea class="form-control" rows="5" id="comment" name="text" required><?php echo e(old('text')); ?></textarea>
                                      <?php if($errors->has('text')): ?>
                                          <span class="help-block">
                                            <strong><?php echo e($errors->first('text')); ?></strong>
                                          </span>
                                      <?php endif; ?>
                                  </div>
                              </div>

                              <div class="col-md-12 text-right">
                                  <input class="btn btn-success btn-sm btn-block" type="submit" value="<?php echo e(trans('interface.send')); ?>">
                              </div>
                          </div>
                      </form>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo e(trans('interface.close')); ?></button>
                  </div>
              </div>
          </div>
      </div>
      <?php echo $__env->make('user.beyanname', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>