<!DOCTYPE html>
<html lang="<?php echo e(config('app.locale')); ?>">
<head>
    <title>Goshop.az</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <link rel="shortcut icon" href="<?php echo e(asset('/own/favicon.ico')); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo e(asset('favicon/apple-touch-icon.png')); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo e(asset('favicon/favicon-32x32.png')); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset('favicon/favicon-16x16.png')); ?>">
    <link rel="mask-icon" href="<?php echo e(asset('favicon/safari-pinned-tab.svg')); ?>" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="<?php echo e(asset('/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/lightslider.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/jquery-ui.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/mystyle.css')); ?>">
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>;
    </script>

    <?php if($type == 1): ?>
        <script>
            let tl = parseFloat('<?=$course_valute->tl?>');
            let usd = parseFloat('<?=$course_valute->usa?>');
        </script>
    <?php endif; ?>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129786961-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-129786961-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '406102379954183');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=406102379954183&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>

<div id="sidebar" class="sidenav">
    <div id="dismiss">
        <i class="glyphicon glyphicon-arrow-left"></i>
    </div>
    <ul>
        <li>
            <div class="user-view">
                <div class="background">
                    <img src="<?php echo e(asset('own/cover.jpg')); ?>">
                </div>

                <div class="own-mobile-language">
                    <div class="dropdown">
                           <span class="dropdown-toggle" type="button" data-toggle="dropdown" style="cursor: pointer; color: #ffffff;"><?php echo e(LaravelLocalization::getCurrentLocale()); ?>

                               <span class="caret"></span></span>
                        <ul class="dropdown-menu own-dropdown-menu mobile-own-dropdown-menu">
                            <?php $__currentLoopData = LaravelLocalization::getSupportedLocales(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $localeCode => $properties): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if(LaravelLocalization::getCurrentLocale() != $properties['native']): ?>
                                    <li>
                                        <a rel="alternate" hreflang="<?php echo e($localeCode); ?>" href="<?php echo e(LaravelLocalization::getLocalizedURL($localeCode, null, [], true)); ?>">
                                            <?php echo e($properties['native']); ?>

                                        </a>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>



                <?php if(Auth::check()): ?>
                    <span class="glyphicon glyphicon-shopping-cart user-cart"></span>
                    <span class="count-in-basket"><?php echo e($addToCartProducts); ?></span>
                    <div class="user-photo">
                        <a
                            <?php if($addToCartProducts): ?>
                            href="<?php echo e(route('user.cart')); ?>"
                            <?php else: ?>
                            href="#"
                            <?php endif; ?>
                        >
                            <img class="img-circle own-img-circle" src="
                        <?php if(Auth::user()->img): ?>
                            <?php echo e(asset('profile')); ?>/<?php echo e(Auth::user()->img); ?>

                            <?php else: ?>
                            <?php echo e(asset('own/yuna.jpg')); ?>

                            <?php endif; ?>
                                    ">
                        </a>
                    </div>


                    <div class="user-name">
                        <a href="#name">
                            <span><?php echo e(Auth::user()->name); ?> <?php echo e(Auth::user()->last_name); ?></span>
                        </a>
                    </div>

                    <div class="user-email">
                        <a href="#email">
                            <span><?php echo e(Auth::user()->email); ?></span>
                        </a>
                    </div>

                <?php else: ?>
                    <div class="user-name">
                        <a href="<?php echo e(route('login')); ?>"><span class="glyphicon glyphicon-user"></span> <?php echo trans('interface.login'); ?></a>
                    </div>
                    <div class="user-name">
                        <a href="<?php echo e(route('register')); ?>"><span class="glyphicon "></span> <?php echo trans('interface.register'); ?></a>
                    </div>
                <?php endif; ?>

            </div>
        </li>
        <?php if(Auth::check()): ?>
            <li>
                <div class="profil-items">
                    <nav>
                        <ul>
                            <li <?php if($type == 9): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(route('user.cart')); ?>">
                                    <i class="glyphicon glyphicon-shopping-cart"></i>
                                    <?php echo e(trans('interface.basket')); ?>

                                    <span class="badge badge-danger pull-right"><?php echo e($addToCartProducts); ?></span>
                                </a>
                            </li>
                            <li <?php if($type == 2): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(route('myOrders')); ?>">
                                    <i class="glyphicon glyphicon-file"></i>
                                    <?php echo e(trans('interface.my_orders')); ?>

                                </a>
                            </li>
                            <li <?php if($type == 3): ?> class="active" <?php endif; ?>>
                                <a  href="<?php echo e(route('abroadAddress')); ?>">
                                    <i class="glyphicon glyphicon-copy"></i>
                                    <?php echo e(trans('interface.address_abroad')); ?>

                                </a>
                            </li>
                            <li <?php if($type == 8): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(route('user.delivery', 1)); ?>">
                                    <i class="glyphicon glyphicon-th-list"></i>
                                    Beyannameler</a>
                            </li>
                            <li <?php if($type == 7): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(route('balance')); ?>">
                                    <i class="glyphicon glyphicon-credit-card"></i>
                                    <?php echo e(trans('interface.balance')); ?> <?php echo e($current_balance); ?> AZN
                                </a>
                            </li>
                            <li <?php if($type == 4): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(route('profile')); ?>">
                                    <i class="glyphicon glyphicon-user"></i>
                                    <?php echo e(trans('interface.profile')); ?>

                                </a>
                            </li>
                            <li <?php if($type == 5 or $type == 6): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(route('questions')); ?>">
                                    <i class="glyphicon glyphicon-question-sign"></i>
                                    <?php echo e(trans('interface.questions')); ?>

                                    <span class="badge badge-danger pull-right"><?php echo e($questions_count); ?></span>
                                </a>
                            </li>
                            
                            
                        </ul>
                    </nav>
                </div>
            </li>
        <?php endif; ?>
        <li>
            <div class="own-sidenav">
                <?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <p><a href="<?php echo e($menu->url); ?>"><?php echo $menu->$title; ?></a></p>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php if(Auth::check()): ?>
                    <p><a href="<?php echo e(route('user.logout')); ?>"
                          onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="glyphicon glyphicon-log-out"></i> <?php echo e(trans('interface.logout')); ?></a></p>
                <?php endif; ?>
            </div>
        </li>
    </ul>
</div>


<div class="sidebar">

    <a class="text-center" href="/"><img class="logo" src="<?php echo e(asset('own/logo.png')); ?>"></a>
    <br>

    <?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <a href="<?php echo e($menu->url); ?>"><?php echo $menu->$title; ?></a>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>



<div id="content" class="main">
    <div class="container-fluid own-head">
        <div class="container">
            <div class="row desktop-header">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <div class="own-language">
                            <div class="dropdown">
                                    <span class="dropdown-toggle text-uppercase" type="button" data-toggle="dropdown" style="cursor: pointer; color: #1796fb;"><?php echo e(LaravelLocalization::getCurrentLocale()); ?>

                                        <span class="caret"></span>
                                    </span>
                                <ul class="dropdown-menu own-dropdown-menu">
                                    <?php $__currentLoopData = LaravelLocalization::getSupportedLocales(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $localeCode => $properties): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if(LaravelLocalization::getCurrentLocale() != $properties['native']): ?>
                                            <li>
                                                <a class="text-uppercase" rel="alternate" hreflang="<?php echo e($localeCode); ?>" href="<?php echo e(LaravelLocalization::getLocalizedURL($localeCode, null, [], true)); ?>">
                                                    <?php echo e($properties['native']); ?>

                                                </a>
                                            </li>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 text-right">
                        <?php if(Auth::guest()): ?>
                            <div class="login_and_sinup">
                                <a href="<?php echo e(route('login')); ?>"><span class="glyphicon glyphicon-user"></span> <?php echo trans('interface.login'); ?></a>
                                <a href="<?php echo e(route('register')); ?>"><span class="glyphicon "></span> <?php echo trans('interface.register'); ?></a>
                            </div>
                        <?php else: ?>
                            <?php
                            $f_name = substr(Auth::user()->name, 0,1);
                            $f_lastname = substr(Auth::user()->last_name, 0,1);
                            $profile_name = $f_name.'.'.$f_lastname
                            ?>
                            <a href="<?php echo e(route('addOrder')); ?>" class="go_to_buy  btn btn-danger btn" data-toggle="tooltip" data-placement="bottom" title="<?php echo e(trans('interface.order_it')); ?>"><span class="glyphicon glyphicon-plus"></span></a>

                            <span class="basketbox">
                                <span class="basket_product_count text-center"><?php echo e($addToCartProducts); ?></span>
                                <a
                                        <?php if($addToCartProducts): ?>
                                            href="<?php echo e(route('user.cart')); ?>"
                                        <?php else: ?>
                                            href="#"
                                        <?php endif; ?>
                                   data-toggle="tooltip" data-placement="bottom" title="<?php echo e(trans('interface.products_in_basket')); ?>" style="display: inline-block;">
                                    <img src="<?php echo e(asset('own/basket.png')); ?>" width="52">
                                </a>
                            </span>

                            <ul class="nav navbar-nav navbar-right own-naw after-login">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle text-uppercase profile-box" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <?php echo e($profile_name); ?>

                                    </a>

                                    <ul class="dropdown-menu profile-dropdown-menu" role="menu">
                                        
                                        <li>
                                            <a <?php if($type == 9): ?> class="active" <?php endif; ?> href="<?php echo e(route('user.cart')); ?>">
                                                <i class="glyphicon glyphicon-shopping-cart"></i>
                                                <?php echo e(trans('interface.basket')); ?>

                                                <span class="badge badge-danger pull-right">
                                                    <?php echo e($addToCartProducts); ?>

                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a <?php if($type == 2): ?> class="active" <?php endif; ?> href="<?php echo e(route('myOrders')); ?>">
                                                <i class="glyphicon glyphicon-file"></i>
                                                <?php echo e(trans('interface.my_orders')); ?>

                                            </a>
                                        </li>
                                        <li>
                                            <a <?php if($type == 3): ?> class="active" <?php endif; ?>  href="<?php echo e(route('abroadAddress')); ?>">
                                                <i class="glyphicon glyphicon-copy"></i>
                                                <?php echo e(trans('interface.address_abroad')); ?>

                                            </a>
                                        </li>
                                        <li>
                                            <a <?php if($type == 8): ?> class="active" <?php endif; ?> href="<?php echo e(route('user.delivery', 1)); ?>">
                                                <i class="glyphicon glyphicon-th-list"></i>
                                                Beyannameler</a>
                                        </li>
                                        <li>
                                            <a <?php if($type == 7): ?> class="active" <?php endif; ?> href="<?php echo e(route('balance')); ?>">
                                                <i class="glyphicon glyphicon-credit-card"></i>
                                                <?php echo e(trans('interface.balance')); ?></a>
                                        </li>
                                        <li>
                                            <a <?php if($type == 4): ?> class="active" <?php endif; ?> href="<?php echo e(route('profile')); ?>">
                                                <i class="glyphicon glyphicon-user"></i>
                                                <?php echo e(trans('interface.profile')); ?></a>
                                        </li>
                                        <li>
                                            <a <?php if($type == 5 or $type == 6): ?> class="active" <?php endif; ?> href="<?php echo e(route('questions')); ?>">
                                                <i class="glyphicon glyphicon-question-sign"></i>
                                                <?php echo e(trans('interface.questions')); ?>

                                                <span class="badge badge-danger pull-right">
                                                    <?php echo e($questions_count); ?>

                                                </span>
                                            </a>
                                        </li>
                                        <li>

                                            <a href="<?php echo e(route('user.logout')); ?>"
                                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                <i class="glyphicon glyphicon-log-out"></i>
                                                <?php echo e(trans('interface.logout')); ?>

                                            </a>

                                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                                <?php echo e(csrf_field()); ?>

                                            </form>
                                        </li>
                                    </ul>

                                </li>
                            </ul>
                        <?php endif; ?>
                    </div>

                </div>
            </div>

            <div class="row mobile-header">

                <div class="col-xs-4 box-for-glyphicon">
                    <span class="glyphicon glyphicon-menu-hamburger" style="cursor:pointer; font-size:30px; color: #ffffff;" id="sidebarCollapse"></span>
                </div>

                <div class="col-xs-4 text-center"><a href="/"><img class="logo" src="<?php echo e(asset('own/logo.png')); ?>" width="100%"></a></div>


                <div class="col-xs-4 text-right" style="padding-top: 10px">
                    <div class="mobile_user_name">
                        <?php if(Auth::guest()): ?>
                            <div>
                                <a href="<?php echo e(route('login')); ?>"><span class="glyphicon glyphicon-user"></span> <?php echo trans('interface.login'); ?></a>
                            </div>
                            <br>
                            <div>
                                <a href="<?php echo e(route('register')); ?>"><span class="glyphicon "></span> <?php echo trans('interface.register'); ?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <br>

    <div class="container content padding-top-15">
        <?php echo $__env->yieldContent('content'); ?>
    </div>

    <footer class="container-fluid">
        <div class="container own-footer-main">
            <div class="own-footer-div">
                <div class="own-footer-div-child"></div>
            </div>
        </div>

        <div class="container">
            <div class="col-md-5 col-sm-5 col-xs-12">
                <p style="font-size: 12px">
                    "Goshop" MMC sürətli poçtdaşıma və kuryer şirkətidir. Fiziki və hüquqi şəxslərin sifariş etdikləri yükləri xaricdəki anbarlarına qəbul edir, onların Azərbaycana daşınıb, müştəriyə təhvil verilməsini təmin edir.
                </p>
            </div>
        </div>
        <div class="container">
            <div class="col-sm-5 col-xs-12"><?php echo trans('interface.copyright'); ?> <?=date('Y')?> <?php echo trans('interface.copyright_text'); ?></div>
            <div class="col-sm-3 col-xs-12 text-center">
                <a href="https://wa.me/994555715"><img src="<?php echo e(asset('own/whatsapp.png')); ?>"></a>
                <a href="https://www.instagram.com/goshop.az"><img width="26" src="<?php echo e(asset('own/instagram.png')); ?>"></a>&nbsp;
                <a href="https://m.me/goshop.az"><img width="26" src="<?php echo e(asset('own/messenger.png')); ?>"></a>
            </div>
            <div class="col-sm-4 col-xs-12 text-center"><img class="img-responsive" src="<?php echo e(asset('own/visa.png')); ?>"></div>
        </div>
    </footer>
</div>

<script src="<?php echo e(asset('/js/jquery-3.3.1.min.js')); ?>"></script>
<script src="<?php echo e(asset('/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('/js/lightslider.js')); ?>"></script>
<script src="<?php echo e(asset('/js/animateNumber.js')); ?>"></script>
<script src="<?php echo e(asset('/js/my_js.js')); ?>"></script>
<script src="<?php echo e(asset('/js/jquery.mCustomScrollbar.concat.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery-ui.min.js')); ?>"></script>
<script>
    $( function() {
        $( "#date" ).datepicker();
    } );
</script>


<script type="text/javascript">
    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
        });
    });
</script>

<?php if($type == 0): ?>
    <script>
        $(document).ready(function() {
            var autoplaySlider = $('#content-slider').lightSlider({
                auto:true,
                loop:true,
                item: 4,
                pager: false,
                pauseOnHover: true,
                onBeforeSlide: function (el) {
                    $('#current').text(el.getCurrentSlideCount());
                }
            });
            $('#total').text(autoplaySlider.getTotalSlideCount());
        });
    </script>
<?php endif; ?>
<?php if($type == 5 or $type == 4): ?>
    <script src="<?php echo e(asset('/js/sorgular.js')); ?>"></script>
    <script>
        $('.finishQuestion').click(function (event) {
            let ask = confirm("Silmək istədiyinizə əminsiniz?");
            if(ask){
                let q_id = $(event.target).attr('data-id');
                $.post("<?php echo e(route('questions.finish')); ?>",
                    { id: q_id,
                        "_token": "<?php echo e(csrf_token()); ?>",
                    },
                    function( data ) {
                        if(data.status == 1){
                            location.reload();
                        }
                    }, "json");
            }
        });
    </script>
<?php endif; ?>

<?php if($type == 1): ?>
    
    <script>
        $(function () {

            function getRandomNumber(min, max) {
                return Math.floor(Math.random() * (max - min)) + min;
            }
            function getSeconds(){
                return new Date().getTime();
            }

            let new_link = '<div class="own-order-box">' +
                '<div style="clear: both; width: 95%; margin: 0 auto"><hr></div><div class="col-md-10 price-parent">\n' +
                '                                        <input name="url[]" type="text" class="own-input-style own-url-style js_get_price_and_size" required placeholder="Bura linki yazın" data-uniq="" >\n' +
                '                                        <input name="own_price[]" type="text" class="change-price text-center own-prices js_set_price" placeholder="Qiymət" required>\n' +
                '                                        <input class="text-center own-prices" value="+ 5%" disabled style="background-color: #1796fb; color: #ffffff; border-color: #057cda">' +
                '<select class="js_set_sizes form-control size_form_control">\n' +
                '                                        </select>' +
                '                                        <input name="price[]" type="text" class="update-price text-center own-prices" value="Toplam" readonly>' +
                '<span class="btn btn-warning counter" data-c="plus"><span class="glyphicon glyphicon-plus"></span></span>\n' +
                '                                        <input name="count[]" style="width: 40px" class="text-center own-prices count" value="1" readonly>\n' +
                '                                        <span class="btn btn-warning counter" data-c="minus"><span class="glyphicon glyphicon-minus"></span></span>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2 text-right">\n' +
                '                                        <span class="btn btn-danger delete-order-box">Sil</span>\n' +
                '                                    </div>\n' +
                '\n' +
                '\n' +
                '                                    <div class="col-md-12">\n' +
                '                                        <div class="own-date" style="display: none">\n' +
                '                                            <spam class="end-time">Dükanın və ya aksiaynın bitmə vaxtı</spam>\n' +
                '                                            <input type="number" name="hour[]" class="text-center" placeholder="saat" min="1" max="23"> <input type="number" min="1" max="59" name="minute[]" class="text-center" placeholder="degige" >&nbsp;\n' +
                '                                            <input type="number" min="1" max="7" name="day[]" class="text-center" placeholder="gun" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Limit 7 gündən artıx ola bilməz">\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-12 col-sm-12">\n' +
                '                                        <div class="form-group">\n' +
                '                                            <textarea name="note[]" class="form-control js_description" rows="2" required placeholder="Qeydi bura yazın"></textarea>\n' +
                '                                        </div>\n' +
                '                                    </div></div>';

            let new_link_with_cheked = '<div class="own-order-box"><div style="clear: both; width: 95%; margin: 0 auto"><hr></div><div class="col-md-9 price-parent">\n' +
                '                                        <input name="url[]" type="text" class="own-input-style own-url-style js_get_price_and_size" required  placeholder="Bura linki yazın">\n' +
                '                                        <input type="text" class="change-price text-center own-prices js_set_price" placeholder="Qiymət" required>\n' +
                '                                        <input class="text-center own-prices" value="+ 5%" disabled style="background-color: #1796fb; color: #ffffff; border-color: #057cda"><select class="js_set_sizes form-control size_form_control">\n' +
                '                                        </select>' +
                '                                        <input name="price[]" type="text" class="update-price text-center own-prices" value="Toplam" readonly>' +
                ' <span class="btn btn-warning counter" data-c="plus"><span class="glyphicon glyphicon-plus"></span></span>\n' +
                '                                        <input name="count[]" style="width: 40px" class="text-center own-prices count" value="1" readonly>\n' +
                '                                        <span class="btn btn-warning counter" data-c="minus"><span class="glyphicon glyphicon-minus"></span></span>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-3 text-right">\n' +
                '                                        <span class="btn btn-danger delete-order-box">Sil</span>\n' +
                '                                    </div>\n' +
                '\n' +
                '\n' +
                '                                    <div class="col-md-12">\n' +
                '                                        <div class="own-date" style="display: block">\n' +
                '                                            <spam class="end-time">Dükanın və ya aksiaynın bitmə vaxtı</spam>\n' +
                '                                            <input type="number" name="hour[]" class="text-center" placeholder="saat" min="1" max="23"> <input type="number" min="1" max="59" name="minute[]" class="text-center" placeholder="degige" >&nbsp;\n' +
                '                                            <input type="number" min="1" max="7" name="day[]" class="text-center" placeholder="gun" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Limit 7 gündən artıx ola bilməz">\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-12 col-sm-12">\n' +
                '                                        <div class="form-group">\n' +
                '                                            <textarea name="note[]" class="form-control js_description" rows="2" required  placeholder="Qeydi bura yazın"></textarea>\n' +
                '                                        </div>\n' +
                '                                    </div></div>';

            // get domain name from url
            function extractHostname(url) {
                var hostname;
                if (url.indexOf("//") > -1) {
                    hostname = url.split('/')[2];
                }
                else {
                    hostname = url.split('/')[0];
                }
                hostname = hostname.split(':')[0];
                hostname = hostname.split('?')[0];
                return hostname;
            }

            function extractRootDomain(url) {
                var domain = extractHostname(url),
                    splitArr = domain.split('.'),
                    arrLen = splitArr.length;
                if (arrLen > 2) {
                    domain = splitArr[arrLen - 2] + '.' + splitArr[arrLen - 1];
                    if (splitArr[arrLen - 2].length == 2 && splitArr[arrLen - 1].length == 2) {
                        domain = splitArr[arrLen - 3] + '.' + domain;
                    }
                }
                return domain;
            }
            // end

            function checkCountry(element){
                return parseInt($(element).closest(".form").find(".country").val());
            }

            $(".rush-order").click(function (event) {
                if (this.checked) {
                    $(event.target).closest('.form').find(".own-date").show();
                } else {
                    $(event.target).closest('.form').find(".own-date").hide();
                }
            })

// add new link
            $('.new-link').click(function (event) {
                let uniq_id = getSeconds()+'_'+getRandomNumber(1, 10000);
                let temp = new_link.replace(/data-uniq=\"\"/g, 'data-uniq="'+uniq_id+'"');

                if($(event.target).closest('.form').find(".rush-order").is(":checked")) {
                    $(event.target).closest('.form').find('.order-box').append(new_link_with_cheked);
                }else{
                    $(event.target).closest('.form').find('.order-box').append(temp);
                }
            });


            $(document).on("click", ".delete-order-box", function (event) {
                let ordercount = parseInt($(event.target).closest('.own-order-box').find('.count').val());
                let getDeletedPrice = parseFloat($(event.target).closest(".own-order-box").find(".update-price").val()) ;


                if(!isNaN(getDeletedPrice)){
                    if(checkCountry(event.target) == 1){
                        let getTotalTlPrice = parseFloat ($('#total_tl').text());
                        let updateTotalTl = getTotalTlPrice - getDeletedPrice * ordercount;
                        let updateTotalAz = updateTotalTl * tl;
                        $('#total_tl').text(updateTotalTl.toFixed(2));
                        $('#total_az').text(updateTotalAz.toFixed(2));
                    } else {
                        let getTotalUSDPrice = parseFloat ($('#total_usd').text());
                        let updateTotalUSD = getTotalUSDPrice - getDeletedPrice * ordercount;
                        let updateTotalAZ = updateTotalUSD * usd;
                        $('#total_usd').text(updateTotalUSD.toFixed(2));
                        $('#total_usd_az').text(updateTotalAZ.toFixed(2));
                    }
                }




                if(!isNaN(getDeletedPrice)) {
                    let url = $(event.target).closest('.own-order-box').find('.js_get_price_and_size').val();
                    if (url) {
                        let site_name = extractHostname(url);

                        let all_prices = 0;
                        $(event.target).closest('.form').find(".own-order-box").each(function () {
                            let urls = $(this).find('.js_get_price_and_size').val();

                            if (urls.indexOf(site_name) != -1) {
                                let get_price_product = parseFloat($(this).find('.change-price').val());
                                let product_count = parseInt($(this).find('.count').val());

                                if (!isNaN(get_price_product)) {
                                    all_prices = all_prices + get_price_product * product_count;
                                }
                            }
                        });


                        let get_price_product2 = parseFloat($(event.target).closest('.own-order-box').find('.change-price').val());
                        let product_count2 = parseInt($(event.target).closest('.own-order-box').find('.count').val());
                        let current_price = get_price_product2 * product_count2;


                        let all_prices2 = all_prices - (get_price_product2 * product_count2);


                        if (pricedata[site_name]) {
                            if (all_prices2 < pricedata[site_name].limit) {
                                let temp_class = site_name.replace(/[\.\/]/g, '_');
                                temp_class = '.' + temp_class;
                                $(temp_class).text(site_name + ' - ' + pricedata[site_name].price + ' TL');
                            }
                        }


                        let i = parseInt(temp_cargo_data[site_name].i);
                        temp_cargo_data[site_name].i = i - 1;

                        if (temp_cargo_data[site_name].i == 0) {
                            delete temp_cargo_data[site_name];
                            let temp_class = site_name.replace(/[\.\/]/g, '_');
                            temp_class = '.' + temp_class
                            $(temp_class).remove();

                            let total_tl = parseFloat($('#total_tl').text());
                            let get_cargo_price = 0;
                            if (site_name in pricedata) {
                                // get_cargo_price = parseFloat(pricedata[site_name].price);
                                // get_cargo_price = get_cargo_price * 0.05 + get_cargo_price;
                                // console.log(get_cargo_price);
                            } else {
                                get_cargo_price = 7.99 * 0.05 + 7.99;
                            }

                            let last_price = total_tl - get_cargo_price;
                            last_price = Math.round(last_price * 100) / 100;
                            $('#total_tl').text(last_price);
                        } else {

                            let atp = 0;
                            $(event.target).closest('.form').find(".own-order-box").each(function () {
                                let geturl = $(this).find('.js_get_price_and_size').val();

                                if (geturl.indexOf(site_name) != -1) {
                                    let get_p_p = parseFloat($(this).find('.change-price').val());
                                    let p_count = parseInt($(this).find('.count').val());

                                    if (!isNaN(get_p_p)) {
                                        atp = atp + get_p_p * p_count;
                                    }
                                }
                            });

                            atp = atp - current_price;

                            if (atp >= pricedata[site_name].limit) {
                                let total_tl = parseFloat($('#total_tl').text());
                                let last_price = total_tl;
                                last_price = Math.round(last_price * 100) / 100;
                                let updateTotalAz = last_price * tl;
                                updateTotalAz = Math.round(updateTotalAz * 100) / 100;
                                $('#total_tl').text(last_price);
                                $('#total_az').text(updateTotalAz);
                            } else {
                                let total_tl = parseFloat($('#total_tl').text());
                                let get_cargo_price = parseFloat(pricedata[site_name].price);
                                get_cargo_price = get_cargo_price * 0.05 + get_cargo_price;
                                let last_price = total_tl + get_cargo_price;
                                last_price = Math.round(last_price * 100) / 100;
                                let updateTotalAz = last_price * tl;
                                updateTotalAz = Math.round(updateTotalAz * 100) / 100;
                                $('#total_tl').text(last_price);
                                $('#total_az').text(updateTotalAz);
                            }
                        }

                    }
                }

                $(event.target).closest(".own-order-box").remove();

            });



            $(document).on("keyup", ".change-price", function (event) {
                event.target.value = event.target.value.replace(/[^\d,.]*/g, '')
                    .replace(/([,.])[,.]+/g, '$1')
                    .replace(/^[^\d]*(\d+([.]\d{0,5})?).*$/g, '$1');

                let getPrice = parseFloat(event.target.value);
                let total = getPrice + getPrice * 0.05;

                if(isNaN(total)){
                    $(event.target).closest('.price-parent').find('.update-price').val('0.00');
                }else{
                    $(event.target).closest('.price-parent').find('.update-price').val(total.toFixed(2));
                }

                let all_prices = 0;
                $(event.target).closest('.form').find(".update-price").each(function() {
                    if(!isNaN(parseFloat($(this).val()))){
                        let ordercount = parseInt($(this).closest('.price-parent').find('.count').val());
                        all_prices = all_prices + parseFloat($(this).val() * ordercount);
                    }
                });


                let cargo_price = 0;
                $.each(temp_cargo_data, function (index, value) {
                    if(pricedata[index]){

                        let atp = 0;
                        $(event.target).closest('.form').find(".own-order-box").each(function() {
                            let geturl = $(this).find('.js_get_price_and_size').val();

                            if( geturl.indexOf(index) != -1){
                                let get_p_p = parseFloat($(this).find('.change-price').val());
                                let p_count = parseInt($(this).find('.count').val());

                                if(!isNaN(get_p_p)){
                                    atp = atp + get_p_p * p_count;
                                }
                            }
                        });

                        if(atp >= pricedata[index].limit){
                            cargo_price = cargo_price + 0;
                        }else{
                            cargo_price = cargo_price + parseFloat(pricedata[index].price);
                        }

                    }else{
                        cargo_price = cargo_price + 7.99;
                    }
                });

                cargo_price = cargo_price * 0.05 + cargo_price;

                all_prices = all_prices + cargo_price;
                all_prices = Math.round(all_prices * 100) / 100;



                let url = $(event.target).closest('.own-order-box').find('.js_get_price_and_size').val();
                let domain_name = extractHostname(url);
                if(domain_name in pricedata){
                    let all_price_url = 0;
                    $(event.target).closest('.form').find(".own-order-box").each(function() {
                        let urls = $(this).find('.js_get_price_and_size').val();

                        if( urls.indexOf(domain_name) != -1){
                            let get_price_product = parseFloat($(this).find('.change-price').val());
                            let product_count = parseInt($(this).find('.count').val());

                            if(!isNaN(get_price_product)){
                                all_price_url = all_price_url + get_price_product * product_count;
                            }
                        }
                    });


                    if(pricedata[domain_name].limit <= all_price_url){
                        let temp_class = domain_name.replace(/[\.\/]/g,'_');
                        temp_class = '.'+temp_class
                        $(temp_class).text(domain_name + ' - Kargo bedava');

                        // all_prices = all_prices - (parseFloat(pricedata[domain_name].price) * 0.05 + parseFloat(pricedata[domain_name].price));
                        // all_prices = Math.round(all_prices * 100) / 100;
                    }else{
                        let temp_class = domain_name.replace(/[\.\/]/g,'_');
                        temp_class = '.'+temp_class
                        $(temp_class).text(domain_name + ' - '+ pricedata[domain_name].price+ ' TL');
                    }
                }


                if( checkCountry(event.target) == 1){
                    let total_az = all_prices * tl;
                    $('#total_tl').text(all_prices);
                    $('#total_az').text(Math.round(total_az * 100) / 100);
                } else {
                    let total_az = all_prices * usd;
                    $('#total_usd').text(all_prices);
                    $('#total_usd_az').text(Math.round(total_az * 100) / 100);
                }

            });

            $(document).on('click', '.counter', function (event) {
                let counter = $(event.currentTarget).attr('data-c');
                let count = parseInt( $(event.target).closest('.price-parent').find('.count').val());
                let price = parseFloat($(event.target).closest('.price-parent').find('.update-price').val());

                if(counter == 'plus'){
                    count = count + 1;
                    $(event.target).closest('.price-parent').find('.count').val(count);
                    if( checkCountry(event.target) == 1){
                        let total_tl =  parseFloat($('#total_tl').text()) + price;
                        let total_az = total_tl * tl;
                        $('#total_tl').text( Math.round(total_tl * 100) / 100 );
                        $('#total_az').text( Math.round(total_az * 100) / 100 );
                    }else{
                        let total_usd =  parseFloat($('#total_usd').text()) + price;
                        let total_az = total_usd * usd;
                        $('#total_usd').text( Math.round(total_usd * 100) / 100 );
                        $('#total_usd_az').text( Math.round(total_az * 100) / 100 );
                    }


                    // cargo
                    let url = $(event.target).closest('.own-order-box').find('.js_get_price_and_size').val();
                    let domain_name = extractHostname(url);
                    if(domain_name in pricedata){
                        let all_price_url = 0;
                        $(event.target).closest('.form').find(".own-order-box").each(function() {
                            let urls = $(this).find('.js_get_price_and_size').val();

                            if( urls.indexOf(domain_name) != -1){
                                let get_price_product = parseFloat($(this).find('.change-price').val());
                                let product_count = parseInt($(this).find('.count').val());

                                if(!isNaN(get_price_product)){
                                    all_price_url = all_price_url + get_price_product * product_count;
                                }
                            }
                        });

                        if(pricedata[domain_name].limit <= all_price_url){
                            let temp_class = domain_name.replace(/[\.\/]/g,'_');
                            temp_class = '.'+temp_class
                            $(temp_class).text(domain_name + ' - Kargo bedava');
                        }else{
                            let temp_class = domain_name.replace(/[\.\/]/g,'_');
                            temp_class = '.'+temp_class
                            $(temp_class).text(domain_name + ' - '+ pricedata[domain_name].price+' TL');
                        }

                    }
                    // end
                }else{
                    if(count != 1 ){
                        count = count - 1;
                        $(event.target).closest('.price-parent').find('.count').val(count);
                        if( checkCountry(event.target) == 1){
                            let total_tl =  parseFloat($('#total_tl').text()) - price;
                            let total_az = total_tl * tl;
                            $('#total_tl').text( Math.round(total_tl * 100) / 100 );
                            $('#total_az').text( Math.round(total_az * 100) / 100 );
                        }else{
                            let total_usd =  parseFloat($('#total_usd').text()) - price;
                            let total_az = total_usd * usd;
                            $('#total_usd').text( Math.round(total_usd * 100) / 100 );
                            $('#total_usd_az').text( Math.round(total_az * 100) / 100 );
                        }

                        // cargo
                        let url = $(event.target).closest('.own-order-box').find('.js_get_price_and_size').val();
                        let domain_name = extractHostname(url);
                        if(domain_name in pricedata){
                            let all_price_url = 0;
                            $(event.target).closest('.form').find(".own-order-box").each(function() {
                                let urls = $(this).find('.js_get_price_and_size').val();

                                if( urls.indexOf(domain_name) != -1){
                                    let get_price_product = parseFloat($(this).find('.change-price').val());
                                    let product_count = parseInt($(this).find('.count').val());

                                    if(!isNaN(get_price_product)){
                                        all_price_url = all_price_url + get_price_product * product_count;
                                    }
                                }
                            });

                            if(pricedata[domain_name].limit <= all_price_url){
                                let temp_class = domain_name.replace(/[\.\/]/g,'_');
                                temp_class = '.'+temp_class
                                $(temp_class).text(domain_name + ' - Kargo bedava');
                            }else{
                                let temp_class = domain_name.replace(/[\.\/]/g,'_');
                                temp_class = '.'+temp_class
                                $(temp_class).text(domain_name + ' - '+ pricedata[domain_name].price);
                            }

                        }
                        // end
                    }
                }
            });


            let temp_cargo_data = {};

            $(document).on('change', '.js_get_price_and_size', function (event) {
                let data_uniq = $(event.target).attr('data-uniq');

                let url = $(event.target).val();

                let domain_name = extractHostname(url);

                if(url.indexOf('trendyol.com') != -1){
                    $.post("<?php echo e(route('get.datafromurl')); ?>",
                        { site: url,
                            "_token": "<?php echo e(csrf_token()); ?>",
                        },
                        function( data ) {
                            if(data.price != 0){

                                let price = parseFloat(data.price);
                                $(event.target).closest('.price-parent').find('.js_set_price').val(price);

                                let with_percentage = price + price * 0.05;
                                if(isNaN(with_percentage)){
                                    $(event.target).closest('.price-parent').find('.update-price').val('0.00');
                                }else{
                                    $(event.target).closest('.price-parent').find('.update-price').val(with_percentage.toFixed(2));
                                }


                                let all_prices = 0;
                                $(event.target).closest('.form').find(".update-price").each(function() {
                                    if(!isNaN(parseFloat($(this).val()))){
                                        let ordercount = parseInt($(this).closest('.price-parent').find('.count').val());
                                        all_prices = all_prices + parseFloat($(this).val() * ordercount);
                                    }
                                });

                                if( checkCountry(event.target) == 1){
                                    let total_az = all_prices * tl;
                                    $('#total_tl').text( Math.round(all_prices * 100) / 100 );
                                    $('#total_az').text( Math.round(total_az * 100) / 100 );
                                }else{
                                    let total_az = all_prices * usd;
                                    $('#total_usd').text( Math.round(all_prices * 100) / 100 );
                                    $('#total_usd_az').text( Math.round(total_az * 100) / 100 );
                                }

                                // kargo
                                if(domain_name in temp_cargo_data){
                                    let i = parseInt(temp_cargo_data[domain_name]['i']);
                                    temp_cargo_data[domain_name]['i'] = i + 1;

                                    let cargo_price = parseFloat(pricedata[domain_name].price);
                                    let get_tottal_tl = parseFloat($('#total_tl').text()) + cargo_price * 0.05 + cargo_price;
                                    get_tottal_tl = Math.round(get_tottal_tl * 100) / 100;
                                    let tl_convert_azn = get_tottal_tl  * tl;

                                    $('#total_tl').text(get_tottal_tl);
                                    $('#total_az').text(Math.round(tl_convert_azn * 100) / 100);
                                }else{
                                    temp_cargo_data[domain_name] = {i: 1}

                                    if(domain_name in pricedata){
                                            // let info = '&nbsp<span class="btn btn-danger">'+domain_name+' - '+pricedata[domain_name].price+' TL</span>';
                                            // $('.cargo_info').append(info);
                                            //
                                            let cargo_price = parseFloat(pricedata[domain_name].price);
                                            let get_tottal_tl = parseFloat($('#total_tl').text()) + cargo_price * 0.05 + cargo_price;
                                            get_tottal_tl = Math.round(get_tottal_tl * 100) / 100;
                                            let tl_convert_azn = get_tottal_tl  * tl;

                                        $('#total_tl').text(get_tottal_tl);
                                        $('#total_az').text(Math.round(tl_convert_azn * 100) / 100);
                                    }else{
                                        let info = '&nbsp<span class="btn btn-warning">Məlumat yoxdur - 7.99 TL</span>';
                                        $('.cargo_info').append(info);
                                    }
                                }
                                // end

                            }else{
                                $(event.target).closest('.price-parent').find('.js_set_price').val('');
                            }

                            if(data.sizes != 0){
                                $(event.target).closest('.price-parent').find('.js_set_sizes').show();
                                $(event.target).closest('.price-parent').find('.js_set_sizes').html(data.sizes);
                            }else{
                                $(event.target).closest('.price-parent').find('.js_set_sizes').hide();
                            }

                        }
                        ,"json"
                    );
                } else {
                    $(event.target).closest('.price-parent').find('.js_set_price').val('');
                    $(event.target).closest('.price-parent').find('.js_set_sizes').hide();


                    // kargo
                    if(domain_name in temp_cargo_data){
                        let i = parseInt(temp_cargo_data[domain_name]['i']);
                        temp_cargo_data[domain_name]['i'] = i + 1;

                        if(domain_name in pricedata){
                            let all_prices = 0;
                            $(event.target).closest('.form').find(".own-order-box").each(function() {
                                let urls = $(this).find('.js_get_price_and_size').val();

                                if( urls.indexOf(domain_name) != -1){
                                    let get_price_product = parseFloat($(this).find('.change-price').val());
                                    let product_count = parseInt($(this).find('.count').val());

                                    if(!isNaN(get_price_product)){
                                        all_prices = all_prices + get_price_product * product_count;
                                    }
                                }
                            });

                            // if(pricedata[domain_name].limit <= all_prices){
                            //     let temp_class = domain_name.replace(/[\.\/]/g,'_');
                            //     temp_class = '.'+temp_class
                            //     $(temp_class).text(domain_name + ' - Kargo bedava');
                            // }


                        }

                    }else{
                        temp_cargo_data[domain_name] = {i: 1}

                        if(domain_name in pricedata){
                                // let temp_class = domain_name.replace(/[\.\/]/g,'_');
                                // let info = '&nbsp<span class="btn btn-danger ' + temp_class + ' ' + data_uniq + '">'+domain_name+' - '+pricedata[domain_name].price+' TL</span>';
                                // $('.cargo_info').append(info);

                                let cargo_price = parseFloat(pricedata[domain_name].price);
                                let get_tottal_tl = parseFloat($('#total_tl').text()) + cargo_price;
                                get_tottal_tl = Math.round(get_tottal_tl * 100) / 100;
                                $('#total_tl').text(get_tottal_tl);
                        }else{
                            // let temp_class = domain_name.replace(/[\.\/]/g,'_');
                            // let info = '&nbsp<span class="btn btn-warning ' + temp_class + ' ' + data_uniq + '">Məlumat yoxdur - 7.99 TL</span>';
                            // $('.cargo_info').append(info);

                            let get_tottal_tl = parseFloat($('#total_tl').text()) + 7.99;
                            get_tottal_tl = Math.round(get_tottal_tl * 100) / 100;
                            $('#total_tl').text(get_tottal_tl);
                        }
                    }
                    // end
                }


                $('.cargo_info').html('<span>Türkiye daxili kargo:</span>');
                $(event.target).closest('.form').find(".own-order-box").each(function() {
                    let url = $(this).find('.js_get_price_and_size').val();
                    let saytin_adi = extractHostname(url);

                    if(saytin_adi in pricedata){
                        let temp_class = saytin_adi.replace(/[\.\/]/g,'_');

                        if(!$('.cargo_info').find('.'+temp_class).length){
                            let set_info = '&nbsp<span class="btn btn-danger '+temp_class+'">'+saytin_adi+' - '+pricedata[saytin_adi].price+' TL</span>';
                            $('.cargo_info').append(set_info);
                        }
                    }else{
                        let temp_class = saytin_adi.replace(/[\.\/]/g,'_');

                        if(!$('.cargo_info').find('.'+temp_class).length){
                            let set_info = '&nbsp<span class="btn btn-warning '+temp_class+'">Məlumat yoxdur - 7.99 TL</span>';
                            $('.cargo_info').append(set_info);
                        }
                    }

                });


                
            });

            $(document).on('change', '.js_set_sizes', function(event) {
                $(event.target).closest('.own-order-box').find('.js_description').val(this.value );
            });

        });
    </script>
<?php elseif($type == 2 or $type == 8 or $type == 9): ?>
    
    <script>
        $(function () {
            $('a.edit-my-order').click(function(){
                var order=$(this);
                // $('#myOrderEdit').modal('show');

                $.post("<?php echo e(route('editOrder')); ?>",
                    {order_id: order.attr('order-id'),
                        "_token": "<?php echo e(csrf_token()); ?>",
                    },
                    function( data ) {
                        $('#myOrderEdit').find(".modal-body").html(data);
                        $('#myOrderEdit').modal('show');
                        $( "#date_beyanname" ).datepicker();
                    });
            });


            // beyanname
            $('.edit-my-beyanname').click(function(){
                var order=$(this);
                // $('#edit_beyanname').modal('show');

                $.post("<?php echo e(route('beyanname.edit')); ?>",
                    {order_id: order.attr('package-id'),
                        "_token": "<?php echo e(csrf_token()); ?>",
                    },
                    function( data ) {
                        $('#edit_beyanname').find(".modal-body").html(data);
                        $('#edit_beyanname').modal('show');
                    });
            });

            $('.d').click(function () {
                var order=$(this).attr('order-id');
                var ask = confirm("Silmək istədiyinizə əminsiniz?");

                if(ask){
                    console.log(order);
                    $.post("<?php echo e(route('deleteOrder')); ?>",
                        {order_id: order,
                            "_token": "<?php echo e(csrf_token()); ?>"
                        },
                        function( data ) {
                            if(data.status == 1)
                                location.reload();
                            else
                                $('#d_o').html(
                                    '<div class="alert alert-warning alert-dismissible fade in text-center">' +
                                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo e(trans('interface.access_denied')); ?></div>'
                                );
                        }, "json");
                }else{
                    return false;
                }
            });


            function validateNum(event){
                event.target.value = event.target.value.replace(/[^\d,.]*/g, '')
                    .replace(/([,.])[,.]+/g, '$1')
                    .replace(/^[^\d]*(\d+([.]\d{0,5})?).*$/g, '$1');
            }

            $(document).on("keyup", "#count", function (event) {
                let price = parseFloat($(event.target).closest('.basket-modal').find('#price').val());
                let totalPrice = parseInt(event.target.value) * price;
                if(isNaN(totalPrice)){
                    $('#total_price').val(price);
                }else{
                    $('#total_price').val(totalPrice);
                }
            });

            $(document).on("keyup", "#price", function (event) {
                let price = parseFloat(event.target.value);
                let count = parseInt($(event.target).closest('.basket-modal').find('#count').val());
                let totalPrice = price * count;

                if(isNaN(totalPrice)){
                    $('#total_price').val(0);
                }else{
                    $('#total_price').val(totalPrice);
                }
            });


            // show product
            $('.show-product').click(function (event) {
                $(event.target).closest('.order-box-parent').find('.product-list').toggle();
            })


            // bagalama herekati
            $('.baglama_id').click(function(){
                var baglama_id=$(this);
                $.post("<?php echo e(route('showtracking')); ?>",
                    {order_id: baglama_id.attr('baglama-id'),
                        "_token": "<?php echo e(csrf_token()); ?>",
                    },
                    function( data ) {
                        $('#showTracking').find(".modal-body").html(data);
                        let active = $('.complete' ).last();
                        active.removeClass('complete');
                        active.addClass('active');
                        $('#showTracking').modal('show');
                    });
            });

            // beyanname herekati
            $('.b_id').click(function(){
                var b_id=$(this);
                $.post("<?php echo e(route('beyanname.showtracking')); ?>",
                    {order_id: b_id.attr('b-id'),
                        "_token": "<?php echo e(csrf_token()); ?>",
                    },
                    function( data ) {
                        $('#showTracking').find(".modal-body").html(data);
                        let active = $('.complete' ).last();
                        active.removeClass('complete');
                        active.addClass('active');
                        $('#showTracking').modal('show');
                    });
            });


            // Beyamname file input
        //     function bs_input_file() {
        //         $(".input-file").before(
        //             function() {
        //                 if ( ! $(this).prev().hasClass('input-ghost') ) {
        //                     var element = $("<input type='file' class='input-ghost' id='invois' style='visibility:hidden; height:0'>");
        //                     element.attr("name",$(this).attr("name"));
        //                     element.change(function(){
        //                         element.next(element).find('input').val((element.val()).split('\\').pop());
        //                     });
        //                     $(this).find("button.btn-choose").click(function(){
        //                         element.click();
        //                     });
        //                     $(this).find('input').css("cursor","pointer");
        //                     $(this).find('input').mousedown(function() {
        //                         $(this).parents('.input-file').prev().click();
        //                         return false;
        //                     });
        //                     return element;
        //                 }
        //             }
        //         );
        //     }
        //     $(function() {
        //         bs_input_file();
        //     });
        });


        jQuery(document).ready(function(){
            jQuery('#submit').click(function(e){
                e.preventDefault();

                let data1 = new FormData();
                data1.append('file', jQuery('#img')[0].files[0]);
                data1.append('magazin_name',  jQuery('#magazin_name').val());
                data1.append('price',  jQuery('#price').val());

                data1.append('country',             jQuery('#country').val());
                data1.append('package_count',       jQuery('#package_count').val());
                // data1.append('sifarish_number',     jQuery('#sifarish_number').val());
                data1.append('price_type',          jQuery('#price_type').val());
                data1.append('date',                jQuery('#date').val());
                data1.append('mehsul_type',         jQuery('#mehsul_type').val());
                data1.append('tracking_number',     jQuery('#tracking_number').val());
                data1.append('description',         jQuery('#description').val());

                jQuery.ajax({
                    url: "<?php echo e(route('user.beyyanname')); ?>",
                    method: 'post',
                    data: data1,

                    cache: false,
                    contentType: false,
                    processData: false,

                    success: function(data){
                        if(data.errors){
                            jQuery('.balert-danger p').remove();
                            jQuery.each(data.errors, function(key, value){
                                jQuery('.balert-danger').show();
                                jQuery('.balert-danger').append('<p>'+value+'</p>');
                            });
                        }

                        if(data.success){
                            jQuery('.balert-danger').hide();
                            jQuery('.b_well').hide();
                            jQuery('.balert-success').show();
                            jQuery('.balert-success').append(data.success);

                            setTimeout(function(){
                                window.location.reload();
                            }, 2000);
                        }
                    }

                });
            });
        });


            $(document).on('click', '#beyanname_edit',  function(e){
                e.preventDefault();
                let data1 = new FormData();
                data1.append('file',                jQuery('#b_img')[0].files[0]);
                data1.append('magazin_name',        jQuery('#b_magazin_name').val());
                data1.append('price',               jQuery('#b_price').val());
                data1.append('package_count',       jQuery('#b_package_count').val());
                // data1.append('sifarish_number',     jQuery('#b_sifarish_number').val());
                data1.append('price_type',          jQuery('#b_price_type').val());
                data1.append('date',                jQuery('#b_date').val());
                data1.append('f_a_date',            jQuery('#f_a_date').val());
                data1.append('mehsul_type',         jQuery('#b_mehsul_type').val());
                data1.append('tracking_number',     jQuery('#b_tracking_number').val());
                data1.append('description',         jQuery('#b_description').val());
                data1.append('id',                  jQuery('#b_id').val());
                
                jQuery.ajax({
                    url: "<?php echo e(route('update.beyyanname')); ?>",
                    method: 'post',
                    data: data1,

                    cache: false,
                    contentType: false,
                    processData: false,

                    success: function(data){
                        console.log(data.a);

                        if(data.errors){
                            jQuery('.b-alert-danger p').remove();
                            jQuery.each(data.errors, function(key, value){
                                jQuery('.b-alert-danger').show();
                                jQuery('.b-alert-danger').append('<p>'+value+'</p>');
                            });
                        }

                        if(data.success){
                            jQuery('.b-alert-danger').hide();
                            jQuery('.b_e_well').hide();
                            jQuery('.b-alert-success').show();
                            jQuery('.b-alert-success').append(data.success);

                            setTimeout(function(){
                                window.location.reload();
                            }, 2000);
                        }
                    }

                });
            });
    </script>
<?php endif; ?>

<?php if($type == 2): ?>
<script>
    $(".check_insurance").click(function (event) {
        let a = 0;

        $(".check_insurance").each(function () {
            if(this.checked){
                a = 1;
            }
        })

        if(a == 1){
            $('.insurance_button').show();
        }else{
            $('.insurance_button').hide();
        }
    });

    $(document).ready(function() {
        $('.insurance_button').click(function () {
            let yourArray = new Array();
            $("input:checkbox[name=type]:checked").each(function(){
                yourArray.push($(this).val());
            });

            $.post("<?php echo e(route('show.insurance')); ?>",
                {data: yourArray },
                function( data ) {
                    $('#insurance_modal').find(".insurance_data").html(data);

                    $('#insurance_modal').modal('show');
                });
        })
    });
</script>
<?php endif; ?>
<!--Start of Tawk.to Script-->
<script type="text/javascript" async>
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5c4e13a7ab5284048d0f087b/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->

<div class="overlay"></div>
</body>
</html>