<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12 col-sm-12 text-center"><h1>Beyannameler</h1></div>
        <?php echo $__env->make('user.content_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div style="clear: both"><br></div>


        <div class="col-md-12 col-sm-12">
            <div class="order_show_page">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <?php if(Session::has('msg')): ?>
                            <?php echo Session::get('msg'); ?>

                        <?php endif; ?>
                        <div id="d_o"></div>
                    </div>

                    <div class="col-md-12 col-sm-12">
                        <ul class="nav nav-tabs own_tabs_menu">
                            <li <?php if($status == 1): ?> class="active" <?php endif; ?>><a href="<?php echo e(route('user.delivery', 1)); ?>">
                                    <span class="mobile_orders_icons">
                                        <img src="<?php echo e(asset('own/ordered.png')); ?>">
                                        <span class="label label-primary"><?php echo e($packages_count); ?></span>
                                    </span>
                                    <span class="desktop_orders_text"><?php echo e(trans('interface.ordered')); ?>

                                        <span class="label label-primary"><?php echo e($packages_count); ?></span>
                                    </span>
                                </a>
                            </li>
                            <li <?php if($status == 2): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(route('user.delivery', 2)); ?>">
                                    <span class="mobile_orders_icons">
                                        <img src="<?php echo e(asset('own/warehouse.png')); ?>">
                                        <span class="label label-primary"><?php echo e($packages_count_inbaku); ?></span>
                                   </span>

                                    <span class="desktop_orders_text"><?php echo e(trans('interface.warehouse')); ?>

                                        <span class="label label-primary"><?php echo e($packages_count_inbaku); ?></span>
                                    </span>
                                </a>
                            </li>
                            <li <?php if($status == 3): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(route('user.delivery', 3)); ?>">
                                    <span class="mobile_orders_icons">
                                        <img src="<?php echo e(asset('own/over_handed.png')); ?>">
                                        <span class="label label-primary"><?php echo e($packages_count_handet); ?></span>
                                    </span>
                                    <span class="desktop_orders_text"><?php echo e(trans('interface.was_handed_over')); ?>

                                        <span class="label label-primary"><?php echo e($packages_count_handet); ?></span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <br>

                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                    <?php $__currentLoopData = $packages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $package): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="row order-box-parent">
                                        <div class="col-md-12 col-sm-12 order-box">
                                            <div class="col-md-12 col-sm-12 order-box-header">
                                                <div class="row">
                                                <div class="col-md-10 col-sm-10">
                                                    <strong><?php echo e(trans('interface.shop')); ?>:
                                                            <span class="package_name_color"><?php echo e($package->magazin_name); ?></span> | <?php echo e(trans('interface.package_code')); ?>: <?php echo e($package->id); ?> | Dükandan alınan tarix: <?php echo e($package->date); ?>

                                                    </strong>
                                                </div>
                                                <div class="col-md-2 col-sm-2">
                                                    <strong><?php echo e(trans('interface.country')); ?>:
                                                        <?php echo e(trans('interface.turkey')); ?>

                                                    </strong>
                                                </div>
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-sm-2 order-box-img">
                                                <img src="<?php echo e(asset('own/order.png')); ?>" height="79" class="img-rounded" alt="Cinque Terre">
                                                <div class="order-count"><?php echo e($package->package_count); ?></div>
                                            </div>
                                            <div class="col-md-10 col-sm-10 order-box-content">
                                                <table class="table table-bordered order-box-body-table">
                                                    <thead>
                                                    <tr>
                                                        <th>Sifariş nömrəsi</th>
                                                        <th>Kargo izləmə kodu</th>
                                                        <th><?php echo e(trans('interface.price')); ?></th>
                                                        <th><?php echo e(trans('interface.weight')); ?></th>
                                                        <th><?php echo e(trans('interface.delivery_fee')); ?></th>
                                                        <th><?php echo e(trans('interface.dimensions')); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td><?php echo e($package->sifarish_number); ?></td>
                                                        <td><?php echo e($package->tracking_number); ?></td>
                                                        <td>
                                                            <?php echo e($package->price); ?>

                                                        </td>


                                                        <?php if($package->shipping_price): ?>
                                                            <td>
                                                                <?php if($package->weight): ?>
                                                                    <?php echo e($package->weight); ?> kg
                                                                <?php endif; ?>
                                                            </td>
                                                            <td>
                                                                <?php if($package->shipping_price): ?>
                                                                    <?php echo e($package->shipping_price); ?> $
                                                                <?php endif; ?>
                                                            </td>
                                                            <td>
                                                                <?php if($package->size): ?>
                                                                    <?php echo e($package->size); ?>

                                                                <?php endif; ?>
                                                            </td>
                                                        <?php else: ?>
                                                            <td colspan="3" class="alert alert-success"><?php echo e(trans('interface.will_show_warehouse')); ?></td>
                                                        <?php endif; ?>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                                <table class="table table-bordered order-box-footer-table">
                                                    <tbody>
                                                    <tr>
                                                        <td class="show-product text-center"><?php echo e(trans('interface.description')); ?></td>
                                                        <td class="text-center a b_id blinking" b-id="<?php echo e($package->id); ?>"> <?php echo e(trans('interface.package_moving')); ?></td>
                                                        <td class="text-center"><button  class="btn btn-block btn-success edit-my-beyanname" package-id="<?php echo e($package->id); ?>" style="padding: 0px 4px;"><i class="glyphicon glyphicon-edit"></i> <?php echo e(trans('interface.edit')); ?></button>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 product-list" style="display: none;">
                                            <table class="table table-bordered products-in-order">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <?php if($package->invois): ?>
                                                                <img src="<?php echo e(asset('beyanname')); ?>/<?php echo e($package->invois); ?>" class="img-responsive" alt="<?php echo e($package->magazin_name); ?>">
                                                            <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo e($package->description); ?>

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <hr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>

                        <div class="text-center">
                            <?php echo e($packages->links()); ?>

                        </div>

                    </div>
                </div>
            </div>
        </div>



        <!-- Package tracking -->
        <div class="modal fade" id="showTracking" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center"><?php echo e(trans('interface.package_moving')); ?></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo e(trans('interface.close')); ?></button>
                    </div>
                </div>
            </div>
        </div>

        
        <div id="edit_beyanname" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">BƏYANNAMƏ BLANKI TAM VƏ DƏQİQ DOLDURULMALIDIR</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                </div>
            </div>
        </div>
        <?php echo $__env->make('user.beyanname', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div style="clear: both"><br></div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>