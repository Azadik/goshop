<?php $__env->startSection('content'); ?>
        <div class="row">
            <div class="col-lg-6 col-lg-push-3">
                <div class="panel padding-25 margin-top-25" style="margin-bottom: 50px; box-shadow: 0 6px 22px 1px rgba(0, 0, 0, 0.15);">
                    <div class="text-center margin-bottom-25">
                        <h3 class="heading text-uppercase"><?php echo trans('interface.register'); ?></h3>
                    </div>
                    <form role="form" method="POST" action="<?php echo e(route('register')); ?>">
                        <?php echo e(csrf_field()); ?>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                    <input class="form-control" size="60" placeholder="<?php echo trans('interface.name'); ?>" maxlength="128" value="<?php echo e(old('name')); ?>" name="name" id="Users_name" type="text" required>
                                    <div class="errorMessage" id="Users_name_msg" style="display:none"></div>
                                    <?php if($errors->has('name')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('name')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group<?php echo e($errors->has('last_name') ? ' has-error' : ''); ?>">
                                    <input class="form-control" size="60" maxlength="128" placeholder="<?php echo trans('interface.last_name'); ?>" value="<?php echo e(old('last_name')); ?>" name="last_name" id="Users_lastname" type="text" required>
                                    <div class="errorMessage" id="Users_lastname_msg" style="display:none"></div>
                                    <?php if($errors->has('name')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('last_name')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-12 text-center">
                                <div class="form-group">
                                    <select class="form-control" name="gender" required>
                                        <option value="" disabled="" selected="" hidden=""><?php echo trans('interface.gender'); ?></option>
                                        <option value="0"><?php echo trans('interface.female'); ?></option>
                                        <option value="1"><?php echo trans('interface.male'); ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-12 text-center">
                                <div class="form-group text-center inline-block">
                                    <div class="input-group">
                                        <select class="form-control" name="day" required>
                                            <option value=""><?php echo trans('interface.day'); ?></option>
                                            <option value="01">1</option>
                                            <option value="02">2</option>
                                            <option value="03">3</option>
                                            <option value="04">4</option>
                                            <option value="05">5</option>
                                            <option value="06">6</option>
                                            <option value="07">7</option>
                                            <option value="08">8</option>
                                            <option value="09">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                        </select>
                                        <span class="input-group-btn" style="width: 120px;">
                                <select class="form-control" name="month" required>
									<option value=""><?php echo trans('interface.month'); ?></option>
									  <option value="01"><?php echo trans('interface.january'); ?></option>
                                      <option value="02"><?php echo trans('interface.february'); ?></option>
                                      <option value="03"><?php echo trans('interface.march'); ?></option>
                                      <option value="04"><?php echo trans('interface.april'); ?></option>
                                      <option value="05"><?php echo trans('interface.may'); ?></option>
                                      <option value="06"><?php echo trans('interface.june'); ?></option>
                                      <option value="07"><?php echo trans('interface.july'); ?></option>
                                      <option value="08"><?php echo trans('interface.august'); ?></option>
                                      <option value="09"><?php echo trans('interface.september'); ?></option>
                                      <option value="10"><?php echo trans('interface.october'); ?></option>
                                      <option value="11"><?php echo trans('interface.november'); ?></option>
                                      <option value="12"><?php echo trans('interface.december'); ?></option>
								</select>

                                </span>
                                        <span class="input-group-btn" style="width: 100px;">
                                <select class="form-control" name="year" required>
									<option value=""><?php echo trans('interface.year'); ?></option>
                                    <?php
                                        for($i=1900; $i<=2018; $i++){
                                         echo '<option value="'.$i.'">'.$i.'</option>';
                                        }
                                    ?>
                                </select>
                                </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                                    <input class="form-control" placeholder="<?php echo trans('interface.email'); ?>" size="60" maxlength="128" value="<?php echo e(old('email')); ?>" name="email" id="Users_email" type="text" required>
                                    <div class="errorMessage" id="Users_email_msg" style="display:none"></div>
                                    <?php if($errors->has('email')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group<?php echo e($errors->has('contact') ? ' has-error' : ''); ?>">
                                    <div class="input-group">
                                        <span class="input-group-btn" style="width: 80px;">
                                            <select class="form-control" name="operator" id="Users_operator">
                                                <option value="055">055</option>
                                                <option value="050">050</option>
                                                <option value="051">051</option>
                                                <option value="070">070</option>
                                                <option value="077">077</option>
                                            </select>
                                        </span>
                                        <input class="form-control" size="60" maxlength="7" placeholder="xxx xx xx" name="contact" id="Users_mobile" type="text" value="<?php echo e(old('contact')); ?>" required>
                                        <div class="errorMessage" id="Users_mobile_msg" style="display:none"></div>
                                    </div>
                                    <?php if($errors->has('contact')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('contact')); ?></strong>
                                            </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                                            <input class="form-control" size="60" placeholder="<?php echo trans('interface.password'); ?>" maxlength="256" name="password" id="password" type="password"  required>
                                            <div class="errorMessage" id="Users_password_msg" style="display:none"></div>
                                            <?php if($errors->has('password')): ?>
                                                <span class="help-block">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                        </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input id="password-confirm" class="form-control" placeholder="<?php echo trans('interface.confirm_password'); ?>" size="60" maxlength="256" name="password_confirmation" type="password" required>
                                            <div class="errorMessage" id="Users_repeatPassword_msg" style="display:none"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input class="form-control" placeholder="<?php echo trans('interface.address'); ?>" value="<?php echo e(old('address')); ?>" size="60" maxlength="200" name="address" id="Users_address" type="text">
                                    <div class="errorMessage" id="Users_address_em_" style="display:none"></div>
                                </div>
                            </div>


                            <div class="col-lg-12">
                                <div class="form-group<?php echo e($errors->has('passport') ? ' has-error' : ''); ?>">
                                    <input class="form-control" size="8" maxlength="8" placeholder="<?php echo trans('interface.passport'); ?>" value="<?php echo e(old('passport')); ?>" name="passport" id="Users_passport" type="text" required>
                                    <div class="errorMessage" id="Users_passport_msg" style="display:none"></div>
                                    <?php if($errors->has('passport')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('passport')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>



                            <div class="col-lg-12 text-right">
                                <span style="cursor: pointer;" data-toggle="modal" data-target="#myModal"><img src="<?php echo e(asset('own/info.png')); ?>"></span>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input class="form-control"  placeholder="<?php echo e(trans('interface.passport_fin_code')); ?>" value="<?php echo e(old('fin_code')); ?>" name="fin_code"  type="text" required>
                                </div>
                            </div>



                            <div class="col-lg-12 text-center">
                                <div class="form-group">
                                    <select class="form-control" name="social">
                                        <option value="" disabled="" selected="" hidden=""><?php echo trans('interface.how_find_us'); ?></option>
                                        <option value="1"><?php echo trans('interface.fb'); ?></option>
                                        <option value="2"><?php echo trans('interface.insta'); ?></option>
                                        <option value="3"><?php echo trans('interface.google'); ?></option>
                                        <option value="4"><?php echo trans('interface.radio'); ?></option>
                                        <option value="5"><?php echo trans('interface.tv'); ?></option>
                                        <option value="6"><?php echo trans('interface.from_friend'); ?></option>
                                        <option value="7"><?php echo trans('interface.other'); ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-12 text-center diger" style="display: none;">
                                <div class="form-group">
                                    <textarea class="form-control" name="other"></textarea>
                                </div>
                            </div>

                        </div>

                        <div class="row margin-top-10">
                            <div class="col-lg-12 text-center">
                                <button type="submit" class="text-uppercase btn own-btn-info"><?php echo trans('interface.confirm'); ?></button>
                            </div>
                        </div>

                        <hr>

                        <div class="row margin-top-10">
                            <div class="col-lg-12 text-center">
                                <span class="innerText"><?php echo trans('interface.rules1'); ?> <label class="">
                                        <a target="_blank" href="<?php echo e(route('guest.agreement')); ?>" class="own-label-color"><?php echo trans('interface.rules2'); ?></a>
                                    </label> <?php echo trans('interface.rules3'); ?> </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body text-center">
                        <img class="img-rounded" style="width: 100%" src="<?php echo e(asset('own/vesiqe.png')); ?>">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo e(trans('interface.close')); ?></button>
                    </div>
                </div>
            </div>
        </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>