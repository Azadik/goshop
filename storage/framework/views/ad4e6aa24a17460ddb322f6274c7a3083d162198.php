<?php $__env->startSection('content'); ?>
      <div class="row">
          <div class="col-md-12 col-sm-12 text-center"><h1><?php echo e(trans('interface.address_abroad')); ?></h1></div>

          <?php echo $__env->make('user.content_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          <div style="clear: both"><br></div>


          <div class="col-md-12 col-sm-12">
              <div class="white-background">
                  <div class="row">
                      <div class="col-md-12 col-sm-12">
                          <?php if(Session::has('msg')): ?>
                              <?php echo Session::get('msg'); ?>

                          <?php endif; ?>
                      </div>

                      <div class="col-md-12 col-sm-12">
                          <div class="alert alert-danger alert-dismissible fade in">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <strong>Diqqət! Satıcı firmanın saytında sifariş verərkən ünvan hissəsini aşağıda qeyd olunan formada doldurmağınızı xahiş edirik.</strong>
                          </div>

                           <ul class="nav nav-pills naw-tabs">
                              <li class="tabs active"><a data-toggle="pill" href="#home"><?php echo e(trans('interface.turkey')); ?></a></li>
                              
                           </ul>


                          <div class="tab-content">
                              <div id="home" class="tab-pane fade in active">
                                 <div class="row">
                                     <div class="col-md-6 col-sm-6">
                                         <br>
                                         <label class="address_style">Ad Soyad</label>
                                         <p class="address_p"><strong><?php echo e(Auth::user()->name); ?> <?php echo e(Auth::user()->last_name); ?></strong></p>

                                         <br>
                                         <label class="address_style">Adres satır 1</label>
                                         <p class="address_p"><strong>Cebeci Mahhallesi 2478 sokak No 2 Daire  4.</strong></p>

                                         <br>
                                         <label class="address_style">İl (Şehir)</label>
                                         <p class="address_p"><strong>İstanbul</strong></p>

                                         <br>
                                         <label class="address_style">İlçe</label>
                                         <p class="address_p"><strong>Sultangazi</strong></p>

                                         <br>
                                         <label class="address_style">Adres başlığı</label>
                                         <p class="address_p"><strong>Goshop AZE</strong></p>
                                     </div>

                                     <div class="col-md-6 col-sm-6">
                                         <br>
                                         <label class="address_style">ZIP/Post kodu</label>
                                         <p class="address_p"><strong>34270</strong></p>

                                         <br>
                                         <label class="address_style">Ülke</label>
                                         <p class="address_p"><strong>Turkiye</strong></p>

                                         <br>
                                         <label class="address_style">Cep telefonu</label>
                                         <p class="address_p"><strong>530 2442909</strong></p>
                                     </div>
                                 </div>
                              </div>

                              <div id="menu1" class="tab-pane fade">
                                  <h3>Menu 1</h3>
                                  <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                              </div>
                          </div>
                      </div>

                  </div>
              </div>
          </div>

          <?php echo $__env->make('user.beyanname', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          <div style="clear: both"><br></div>
      </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>