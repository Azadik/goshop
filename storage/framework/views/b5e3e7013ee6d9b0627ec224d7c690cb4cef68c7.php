<?php $__env->startSection('content'); ?>
            <h2 class="own-header text-center"><?php echo e(trans('interface.example_sites')); ?></h2>
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <div class="text-center">
                        <ul class="nav nav-pills" style="display: inline-block">
                            <li class="active"><a data-toggle="pill" href="#home"><?php echo e(trans('interface.turkey')); ?></a></li>
                            <li><a data-toggle="pill" href="#menu1"><?php echo e(trans('interface.usa')); ?></a></li>
                        </ul>
                    </div>

                    <div style="clear: both"></div>
                    <br>

                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.trendyol.com/" target="_blank" title="www.trendyol.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/trendyol.png')); ?>" alt="www.trendyol.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.trendyol.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.amazon.com.tr" target="_blank" title="www.amazon.com.tr">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/amazon.tr.jpg')); ?>" alt="www.trendyol.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.amazon.com.tr</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.trendyol.com/tum--urunler?q=koton&st=koton&qt=koton&qs=search" target="_blank" title="www.koton.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/koton.png')); ?>" alt="www.koton.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.koton.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.trendyol.com/pierre-cardin?st=pier&qt=Pierre%20Cardin&qs=search" target="_blank" title="www.pierrecardin.com.tr">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/pierre.png')); ?>" alt="www.pierrecardin.com.tr" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.pierrecardin.com.tr</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.trendyol.com/us-polo-assn?st=us&qt=US%20Polo%20Assn&qs=search" target="_blank" title="tr.uspoloassn.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/polo.png')); ?>" alt="tr.uspoloassn.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">tr.uspoloassn.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.network.com.tr" target="_blank" title="www.network.com.tr">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/network.png')); ?>" alt="www.network.com.tr" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.network.com.tr</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.trendyol.com/cacharel?st=cachar&qt=Cacharel&qs=search" target="_blank" title="www.cacharel.com.tr">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/cacharel.png')); ?>" alt="www.cacharel.com.tr" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.cacharel.com.tr</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.trendyol.com/tum--urunler?q=flo&st=flo&qt=flo&qs=search" target="_blank" title="www.flo.com.tr">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/flo.png')); ?>" alt="www.flo.com.tr" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.flo.com.tr</div>
                                    </a>
                                </div>
                            </div>


                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.morhipo.com" target="_blank" title="www.morhipo.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/morhipo.png')); ?>" alt="www.morhipo.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.morhipo.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="http://1v1y.com" target="_blank" title="1v1y.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/1v1y.png')); ?>" alt="1v1y.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">1v1y.com</div>
                                    </a>
                                </div>
                            </div>


                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.patirti.com" target="_blank" title="www.patirti.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/patirti.png')); ?>" alt="www.patirti.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.patirti.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.trendyol.com/tum--urunler?q=kigili&st=kigili&qt=kigili&qs=search" target="_blank" title="www.kigili.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/kigili.png')); ?>" alt="www.kigili.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.kigili.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.trendyol.com/tum--urunler?q=colins&st=colins&qt=colins&qs=search" target="_blank" title="www.colins.com.tr">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/colins.png')); ?>" alt="www.colins.com.tr" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.colins.com.tr</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.trendyol.com/tum--urunler?q=defacto&st=defacto&qt=defacto&qs=search" target="_blank" title="www.defacto.com.tr">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/defacto.png')); ?>" alt="www.defacto.com.tr" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.defacto.com.tr</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.hepsiburada.com" target="_blank" title="www.hepsiburada.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/hepsiburada.png')); ?>" alt="www.hepsiburada.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.hepsiburada.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.n11.com" target="_blank" title="www.n11.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/n11.png')); ?>" alt="www.n11.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.n11.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.gittigidiyor.com" target="_blank" title="www.gittigidiyor.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/gitti.png')); ?>" alt="www.gittigidiyor.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.gittigidiyor.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.trendyol.com/tum--urunler?q=dsdamat&st=dsdamat&qt=dsdamat&qs=search" target="_blank" title="www.dsdamat.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/dsdamat.png')); ?>" alt="www.dsdamat.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.dsdamat.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.damattween.com" target="_blank" title="www.damattween.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/damat.png')); ?>" alt="www.damattween.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.damattween.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.trendyol.com/lc-waikiki?st=lcwaikiki&qt=LC%20Waikiki&qs=search" target="_blank" title="www.lcwaikiki.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/lcw.png')); ?>" alt="www.lcwaikiki.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.lcwaikiki.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.trendyol.com/tum--urunler?q=mango&st=mango&qt=mango&qs=search" target="_blank" title="shop.mango.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/mango.png')); ?>" alt="shop.mango.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">shop.mango.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.ikea.com.tr/" target="_blank" title="www.ikea.com.tr">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/logo-ikea.png')); ?>" alt="www.ikea.com.tr" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.ikea.com.tr</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://shop.ltbjeans.com" target="_blank" title="shop.ltbjeans.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/ltb-logo.png')); ?>" alt="shop.ltbjeans.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">shop.ltbjeans.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.deichmann.com/TR/tr/shop/welcome.html" target="_blank" title="www.deichmann.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/Deichmann-logo.png')); ?>" alt="www.deichmann.com/TR" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.deichmann.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www2.hm.com/tr_tr/index.html" target="_blank" title="https://www2.hm.com/tr_tr/index.html">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/hm.png')); ?>" alt="https://www2.hm.com/tr_tr/index.html" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www2.hm.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.cartersoshkosh.com.tr/" target="_blank" title="https://www.cartersoshkosh.com.tr">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/store-logo-cartersoshkosh.png')); ?>" alt="https://www.cartersoshkosh.com.tr/" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.cartersoshkosh.com.tr</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.orijinall.net/" target="_blank" title="https://www.orijinall.net/">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/orijinall_net.png')); ?>" alt="https://www.orijinall.net/" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.orijinall.net</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.modaselvim.com/" target="_blank" title="https://www.modaselvim.com/">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/modaselvim_logo.png')); ?>" alt="https://www.modaselvim.com/" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.modaselvim.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.abiye.com/" target="_blank" title="https://www.abiye.com/">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/abiye_logo.png')); ?>" alt="https://www.abiye.com/" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.abiye.com</div>
                                    </a>
                                </div>
                            </div>


                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.moddamor.com/" target="_blank" title="https://www.moddamor.com/">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/moddamor_logoyeni.png')); ?>" alt="https://www.moddamor.com/" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.moddamor.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="http://modahicab.com.tr/" target="_blank" title="http://modahicab.com.tr/">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/Moda-Hicab_logo.png')); ?>" alt="http://modahicab.com.tr/" style="width:100%;">
                                        </div>
                                        <div class="site_tile">modahicab.com.tr</div>
                                    </a>
                                </div>
                            </div>


                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.setrms.com.tr/" target="_blank" title="https://www.setrms.com.tr/">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/main-logo.png')); ?>" alt="https://www.setrms.com.tr/" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.setrms.com.tr</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.kitapyurdu.com/" target="_blank" title="https://www.kitapyurdu.com/">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/kitapyurdu.png')); ?>" alt="https://www.kitapyurdu.com/" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.kitapyurdu.com</div>
                                    </a>
                                </div>
                            </div>


                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.modavitrini.com/" target="_blank" title="https://www.modavitrini.com/">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/modavitrini_logo.jpg')); ?>" alt="https://www.modavitrini.com/" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.modavitrini.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.allday.com.tr/" target="_blank" title="https://www.allday.com.tr/">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/allday-siyah.png')); ?>" alt="https://www.allday.com.tr/" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.allday.com.tr</div>
                                    </a>
                                </div>
                            </div>






                        </div>

                        <div id="menu1" class="tab-pane fade">
                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.ninewest.com" target="_blank" title="www.ninewest.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/ninewest.png')); ?>" alt="www.ninewest.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.ninewest.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.ebay.com" target="_blank" title="www.ebay.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/ebay.png')); ?>" alt="www.ebay.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.ebay.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.sephora.com" target="_blank" title="www.sephora.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/serhopa.jpg')); ?>" alt="www.sephora.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.sephora.com</div>
                                    </a>
                                </div>
                            </div>


                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.amazon.com" target="_blank" title="www.amazon.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/amazon.png')); ?>" alt="www.amazon.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.amazon.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.walmart.com" target="_blank" title="www.walmart.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/walmart.png')); ?>" alt="www.walmart.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.walmart.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.carters.com" target="_blank" title="www.carters.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/carters.png')); ?>" alt="www.carters.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.carters.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.bestbuy.com" target="_blank" title="www.bestbuy.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/bestbuy.png')); ?>" alt="www.bestbuy.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.bestbuy.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.guess.eu" target="_blank" title="www.carters.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/guess.png')); ?>" alt="www.guess.eu" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.guess.eu</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.ralphlauren.com" target="_blank" title="www.ralphlauren.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/ralphlauren.png')); ?>" alt="www.ralphlauren.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.ralphlauren.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.nike.com" target="_blank" title="www.nike.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/nike.png')); ?>" alt="www.nike.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.nike.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.shoes.com" target="_blank" title="www.shoes.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/shoes.png')); ?>" alt="www.shoes.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.shoes.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="http://www.victoriassecret.com" target="_blank" title="www.victoriassecret.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/viktoria.jpg')); ?>" alt="www.victoriassecret.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.victoriassecret.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.zappos.com" target="_blank" title="www.zappos.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/zappos.png')); ?>" alt="www.zappos.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.zappos.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.maccosmetics.com" target="_blank" title="www.maccosmetics.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/maccos.jpg')); ?>" alt="www.maccosmetics.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.maccosmetics.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="http://www.buybuybaby.com" target="_blank" title="www.buybuybaby.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/buybuybaby.png')); ?>" alt="www.buybuybaby.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.buybuybaby.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="http://www.bebe.com" target="_blank" title="www.bebe.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/bebe.jpg')); ?>" alt="www.bebe.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.bebe.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.motelrocks.com" target="_blank" title="www.motelrocks.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/motel.png')); ?>" alt="www.motelrocks.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.motelrocks.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.sony.com" target="_blank" title="www.sony.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/sony.jpg')); ?>" alt="www.sony.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.sony.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.dell.com" target="_blank" title="www.dell.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/dell.png')); ?>" alt="www.dell.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.dell.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="http://www.shop.nordstrom.com" target="_blank" title="www.shop.nordstrom.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/nordstrom.jpg')); ?>" alt="www.shop.nordstrom.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.shop.nordstrom.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.tiffany.com" target="_blank" title="www.tiffany.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/tiffany.png')); ?>" alt="www.tiffany.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.tiffany.com</div>
                                    </a>
                                </div>
                            </div>


                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.gap.com" target="_blank" title="www.gap.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/gap.jpg')); ?>" alt="www.gap.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.gap.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://wwww.hm.com" target="_blank" title="wwww.hm.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/hm.jpg')); ?>" alt="wwww.hm.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">wwww.hm.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.milanicosmetics.com" target="_blank" title="www.milanicosmetics.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/milani.jpg')); ?>" alt="www.milanicosmetics.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.milanicosmetics.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.barneys.com" target="_blank" title="www.barneys.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/barneys.png')); ?>" alt="www.barneys.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.barneys.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.lightinthebox.com" target="_blank" title="www.lightinthebox.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/lightin.jpg')); ?>" alt="www.lightinthebox.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.lightinthebox.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.eastbay.com" target="_blank" title="www.eastbay.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/eastbay.png')); ?>" alt="www.eastbay.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.eastbay.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.macys.com" target="_blank" title="www.macys.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/macys.png')); ?>" alt="www.macys.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.macys.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.shop.diesel.com" target="_blank" title="www.shop.diesel.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/diesel.png')); ?>" alt="www.shop.diesel.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.shop.diesel.com</div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                <div class="site_box">
                                    <a class="text-center" href="https://www.oldnavy.com" target="_blank" title="www.oldnavy.com">
                                        <div class="img_box">
                                            <img src="<?php echo e(asset('sites/usa/oldnavy.png')); ?>" alt="www.oldnavy.com" style="width:100%;">
                                        </div>
                                        <div class="site_tile">www.oldnavy.com</div>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>