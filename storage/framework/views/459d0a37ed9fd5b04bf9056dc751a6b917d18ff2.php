<?php $__env->startSection('content'); ?>
      <div class="row">
          
          <?php echo $__env->make('user.content_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <div style="clear: both"><br></div>
          <div class="col-md-12 col-sm-12">
              <div class="col-md-12 col-sm-12 white-background">
                  <div class="row">

                      <div class="col-md-4 col-sm-4 col-xs-12">
                          <div class="balance_cart">
                              <img src="<?php echo e(asset('own/mastercard_b.png')); ?>" width="100%">
                              <span class="balance_amout"><span><?php echo round($current_balance, 2); ?></span> AZN</span>
                          </div>
                            <br>
                          <div>
                              <form action="<?php echo e(route('post.terminal')); ?>" method="post">
                                  <?php echo e(csrf_field()); ?>

                                  <div class="form-group">
                                      <input class="form-control" type="text" name="amount" onkeyup="this.value=this.value.replace(/[^\d,.]*/g, '').replace(/([,.])[,.]+/g, '$1').replace(/^[^\d]*(\d+([.]\d{0,5})?).*$/g, '$1');" maxlength="12" required>
                                      <input type="hidden" id="description" name="description" value="<?php echo e(Auth::user()->name); ?> <?php echo e(Auth::user()->last_name); ?> -- Post terminal">
                                  </div>
                                  <button class="btn btn-info text-center own-btn-info text-uppercase btn-block"><?php echo e(trans('interface.top_up_balance')); ?></button>
                              </form>
                          </div>
                      </div>

                      
                          
                      
                      <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-12 mobile-margin-top-10">
                          <div class="panel panel-default text-center">
                              <div class="panel-heading">
                                  <?php echo e(trans('interface.last_30_expenses')); ?>

                              </div>
                              <div class="panel-body">
                                  <h3><?php echo e($month_quota); ?> $</h3>

                                  <div>
                                      <br>
                                      <button class="btn btn-default" data-toggle="modal" data-target="#monthQuota"><?php echo e(trans('interface.what_is_this')); ?></button>
                                  </div>
                              </div>
                          </div>

                          <!-- Modal -->
                          <div class="modal fade" id="monthQuota" role="dialog">
                              <div class="modal-dialog modal-lg">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title"></h4>
                                      </div>
                                      <div class="modal-body">
                                          <p class="alert-info alert"><?php echo e(trans('interface.tax_info')); ?></p>
                                      </div>
                                      <div class="modal-footer">
                                          <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo e(trans('interface.close')); ?></button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <!-- Modal -->
                      </div>

                      <div style="clear: both"><br></div>

                      <div class="col-md-12 col-sm-12">
                          <table class="table table-striped">
                              <thead>
                              <tr>
                                  <th><?php echo e(trans('interface.date')); ?></th>
                                  <th><?php echo e(trans('interface.operation')); ?></th>
                              </tr>
                              </thead>
                              <tbody>
                              <?php $__currentLoopData = $balance; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <tr>
                                      <td><?php echo e($bal->date); ?> <?php echo e($bal->time); ?></td>
                                      <td><?php echo e($bal->balance); ?> AZN - Balans: <?php echo e($bal->current_balance); ?> AZN</td>
                                  </tr>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              </tbody>
                          </table>
                      </div>

                      <div class="col-md-12 col-sm-12 text-center">
                          <?php echo e($balance->links()); ?>

                      </div>
                  </div>
              </div>
          </div>

          <?php echo $__env->make('user.beyanname', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <div style="clear: both"><br></div>
      </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('user.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>