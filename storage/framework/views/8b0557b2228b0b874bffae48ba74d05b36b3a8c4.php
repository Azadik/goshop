<?php $__env->startSection('content'); ?>
      <div class="row">
          <?php echo $__env->make('user.content_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          <div style="clear: both"><br></div>


          <div class="col-md-12 col-sm-12">
              <div class="white-background">
                  <div class="row">
                      <div class="col-md-12 col-sm-12">
                          <?php if(Session::has('msg')): ?>
                              <?php echo Session::get('msg'); ?>

                          <?php endif; ?>
                      </div>

                      <div class="col-md-3">
                              <div>
                                  <img
                                          <?php if(Auth::user()->img): ?>
                                            src="<?php echo e(asset('profile')); ?>/<?php echo e(Auth::user()->img); ?>"
                                          <?php else: ?>
                                              <?php if(Auth::user()->gender == 1): ?>
                                              src="<?php echo e(asset('own/man.png')); ?>"
                                              <?php else: ?>
                                              src="<?php echo e(asset('own/girl.png')); ?>"
                                              <?php endif; ?>
                                          <?php endif; ?>
                                       class="img-thumbnail" alt="<?php echo e(Auth::user()->name); ?> <?php echo e(Auth::user()->last_name); ?>" width="100%">
                              </div>
                              <form action="<?php echo e(route('profile.img.upload')); ?>" method="post" enctype="multipart/form-data">
                                  <div>
                                      <?php echo e(csrf_field()); ?>

                                      <div class="form-group <?php echo e($errors->has('img') ? ' has-error' : ''); ?>">
                                          <div class="input-group">
                                            <span class="input-group-btn">
                                                <span class="btn btn-default btn-file">
                                                    Browse… <input type="file" name="img" id="imgInp">
                                                </span>
                                            </span>
                                              <input type="text" class="form-control" readonly>
                                          </div>
                                          <img id='img-upload' width="100%">

                                          <?php if($errors->has('img')): ?>
                                              <span class="help-block">
                                                <strong><?php echo e($errors->first('img')); ?></strong>
                                              </span>
                                          <?php endif; ?>
                                      </div>
                                  </div>
                                  <div>
                                      <input class="btn btn-success btn-sm btn-block" type="submit" value="<?php echo e(trans('interface.upload')); ?>">
                                  </div>
                              </form>
                      </div>


                      <div class="col-md-9 col-sm-9">

                              <div class="row">
                                  <form action="<?php echo e(route('update.profile')); ?>" method="post">
                                      <?php echo e(csrf_field()); ?>

                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                          <?php if($errors->any()): ?>
                                              <div class="alert alert-danger alert-dismissible fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                  <ul>
                                                      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                          <li><?php echo e($error); ?></li>
                                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                  </ul>
                                              </div>
                                          <?php endif; ?>
                                      </div>

                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                              <label for="name"><?php echo e(trans('interface.name')); ?>:</label>
                                              <input type="text" class="form-control" id="name" name="name" value="<?php echo e(Auth::user()->name); ?>">
                                              <div class="errorMessage" style="display:none"></div>
                                              <?php if($errors->has('name')): ?>
                                                  <span class="help-block">
                                                    <strong><?php echo e($errors->first('name')); ?></strong>
                                                  </span>
                                              <?php endif; ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <div class="form-group <?php echo e($errors->has('last_name') ? ' has-error' : ''); ?>">
                                              <label for="last_name"><?php echo e(trans('interface.last_name')); ?>:</label>
                                              <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo e(Auth::user()->last_name); ?>">
                                              <div class="errorMessage" style="display:none"></div>
                                              <?php if($errors->has('last_name')): ?>
                                                  <span class="help-block">
                                                    <strong><?php echo e($errors->first('last_name')); ?></strong>
                                                  </span>
                                              <?php endif; ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <div class="form-group">
                                              <label for="email"><?php echo e(trans('interface.email')); ?>:</label>
                                              <input type="text" readonly class="form-control" id="email" value="<?php echo e(Auth::user()->email); ?>">
                                          </div>
                                      </div>


                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <div class="form-group <?php echo e($errors->has('contact') ? ' has-error' : ''); ?>">
                                              <label for="contact"><?php echo e(trans('interface.telephone')); ?>:</label>
                                              <input type="text" class="form-control" id="contact" name="contact" value="<?php echo e(Auth::user()->contact); ?>">
                                              <div class="errorMessage" style="display:none"></div>
                                              <?php if($errors->has('contact')): ?>
                                                  <span class="help-block">
                                                 <strong><?php echo e($errors->first('contact')); ?></strong>
                                              </span>
                                              <?php endif; ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <div class="form-group <?php echo e($errors->has('address') ? ' has-error' : ''); ?>">
                                              <label for="address"><?php echo e(trans('interface.address_profile')); ?>:</label>
                                              <input type="text" class="form-control" id="address" name="address" value="<?php echo e(Auth::user()->address); ?>">
                                              <div class="errorMessage" style="display:none"></div>
                                              <?php if($errors->has('address')): ?>
                                                  <span class="help-block">
                                                         <strong><?php echo e($errors->first('address')); ?></strong>
                                                      </span>
                                              <?php endif; ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <div class="form-group <?php echo e($errors->has('passport') ? ' has-error' : ''); ?>">
                                              <label for="passport"><?php echo e(trans('interface.passport_number')); ?>:</label>
                                              <input type="text" class="form-control" id="passport" name="passport" maxlength="80" value="<?php echo e(Auth::user()->passport); ?>">
                                              <?php if($errors->has('passport')): ?>
                                                  <span class="help-block">
                                                            <strong><?php echo e($errors->first('passport')); ?></strong>
                                                      </span>
                                              <?php endif; ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-6 col-sm-offset-6">
                                          <div class="form-group <?php echo e($errors->has('fin_code') ? ' has-error' : ''); ?>">
                                              <label for="fin_code"><?php echo e(trans('interface.passport_fin_code')); ?>:</label>
                                              <span style="cursor: pointer;" data-toggle="modal" data-target="#myModal"><img src="<?php echo e(asset('own/info.png')); ?>"></span>
                                              <input type="text" class="form-control" id="fin_code" name="fin_code" maxlength="80" value="<?php echo e(Auth::user()->fin_code); ?>">
                                              <?php if($errors->has('fin_code')): ?>
                                                  <span class="help-block">
                                                            <strong><?php echo e($errors->first('fin_code')); ?></strong>
                                                      </span>
                                              <?php endif; ?>
                                          </div>
                                      </div>

                                      <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                          <button type="submit" class="btn btn-success btn-sm text-uppercase"><?php echo e(trans('interface.save')); ?></button>
                                      </div>
                                  </form>
                              </div>

                          <div style="clear: both;"></div>
                          <hr>

                          <div class="row">
                              <form action="<?php echo e(route('profile.change.password')); ?>" method="post">
                                  <?php echo e(csrf_field()); ?>

                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      <div class="form-group">
                                          <label for="old_pass"><?php echo e(trans('interface.old_password')); ?>:</label>
                                          <input type="password" class="form-control" id="old_pass" name="old_pass">
                                      </div>
                                  </div>

                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                                          <label for="password"><?php echo e(trans('interface.password')); ?>:</label>
                                          <input class="form-control" size="60"  maxlength="256" name="password" id="password" type="password"  >
                                          <div class="errorMessage" style="display:none"></div>
                                          <?php if($errors->has('password')): ?>
                                              <span class="help-block">
                                            <strong><?php echo e($errors->first('password')); ?></strong>
                                            </span>
                                          <?php endif; ?>
                                      </div>

                                      <div class="form-group">
                                          <label for="password-confirm"><?php echo e(trans('interface.repeat_password')); ?>:</label>
                                          <input id="password-confirm" class="form-control"  size="60" maxlength="256" name="password_confirmation" type="password" >
                                          <div class="errorMessage" id="Users_repeatPassword_msg" style="display:none"></div>
                                      </div>
                                  </div>


                                  <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                      <button type="submit" class="btn btn-success btn-sm text-uppercase"><?php echo e(trans('interface.update_password')); ?></button>
                                  </div>
                              </form>

                                  <div class="modal fade" id="myModal" role="dialog">
                                      <div class="modal-dialog modal-lg">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title"></h4>
                                              </div>
                                              <div class="modal-body text-center">
                                                  <img class="img-rounded" style="width: 100%" src="<?php echo e(asset('own/vesiqe.png')); ?>">
                                              </div>
                                              <div class="modal-footer">
                                                  <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo e(trans('interface.close')); ?></button>
                                              </div>
                                          </div>
                                      </div>
                                  </div>

                          </div>
                          <div style="clear: both;"><br></div>
                  </div>
              </div>
          </div>
              <?php echo $__env->make('user.beyanname', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <div style="clear: both"><br></div>
      </div>
      </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>