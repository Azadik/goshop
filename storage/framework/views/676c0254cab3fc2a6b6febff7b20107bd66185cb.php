<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        
                    </div>

                </div>
                <div class="panel-body">
                  <div class="row">
                      <div class="col-md-12 col-sm-12">
                          <?php if(Session::has('msg')): ?>
                              <?php echo Session::get('msg'); ?>

                          <?php endif; ?>
                      </div>

                      <div class="col-sm-6">
                          <form action="<?php echo e(route('add.menu')); ?>" method="post">
                              <?php echo e(csrf_field()); ?>

                              <div class="form-group">
                                  <label for="title_az">title az:</label>
                                  <input type="text" class="form-control" id="title_az" name="title_az" required>
                              </div>

                              <div class="form-group">
                                  <label for="title_en">title en:</label>
                                  <input type="text" class="form-control" id="title_en" name="title_en" required>
                              </div>

                              <div class="form-group">
                                  <label for="title_ru">title ru:</label>
                                  <input type="text" class="form-control" id="title_ru" name="title_ru" required>
                              </div>

                              <div class="form-group">
                                  <label for="url">url</label>
                                  <input type="text" class="form-control" id="url" name="url">
                              </div>

                              <div class="form-group">
                                  <label for="sort">sort number</label>
                                  <input type="number" class="form-control" id="sort" name="sort">
                              </div>

                              <button class="btn btn-sm btn-success">Add</button>
                          </form>
                      </div>


                      <div class="col-sm-5 col-sm-offset-1">
                            <table>
                            <form action="<?php echo e(route('menusort')); ?>" method="post">
                                <?php echo e(csrf_field()); ?>

                                <?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><a href="<?php echo e(route('edit.menu', $menu->id)); ?>"> <?php echo $menu->title_az; ?></a> </td>
                                    <td>
                                        <a style="padding:2px 6px;" href="<?php echo e(route('delete.menu', $menu->id)); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this item?');">
                                            &nbsp;<span class="glyphicon glyphicon-remove"></span>
                                        </a>

                                        <input style="width: 40px; text-align: center; margin-left: 5px" type="number" name="sort[<?php echo e($menu->id); ?>]" value="<?php echo e($menu->sort); ?>"></td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="submit" class="btn btn-sm btn-success btn-block" value="Sort">
                                    </td>
                                </tr>
                            </form>
                            </table>
                        </div>

                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>