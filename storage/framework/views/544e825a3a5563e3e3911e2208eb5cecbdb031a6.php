<?php $__env->startSection('content'); ?>
    <div class="row">
        <?php echo $__env->make('user.content_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div style="clear: both"><br></div>


        <div class="col-md-12 col-sm-12">
            <div class="order_show_page">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <?php if(Session::has('msg')): ?>
                            <?php echo Session::get('msg'); ?>

                        <?php endif; ?>
                        <div id="d_o"></div>
                    </div>

                    <div class="col-md-12 col-sm-12">
                        <?php if(!$orders->isEmpty()): ?>
                            <h2 class="text-center"><?php echo e(trans('interface.basket')); ?></h2>
                            <form action="<?php echo e(route('allPay')); ?>" method="get">
                            <table class="table mobile-table">
                                    <tbody>
                                    <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td class="text-left"><input type="checkbox" name="orders[]" value="<?php echo e($order->id); ?>"></td>
                                            <td style="position: relative">
                                                <?php if($order->img): ?>
                                                    <img src="<?php echo e(asset('products')); ?>/<?php echo e($order->img); ?>">
                                                    <?php else: ?>
                                                    <img width="60" src="<?php echo e(asset('products/no_img.png')); ?>">
                                                <?php endif; ?>
                                                <span class="productcode"><?php echo e(trans('interface.code')); ?>:<?php echo e($order->id); ?></span>
                                            </td>
                                            <td>
                                                <a href="<?php echo e($order->url); ?>" target="_blank" title="<?php echo e($order->url); ?>">
                                                    <span class="content-hide-in-mobile">
                                                        <?php echo substr($order->url, 0, 60) ?> ...
                                                    </span>
                                                    <p>
                                                        <?php echo e($order->note); ?>

                                                    </p>
                                                    <span class="content-hide-in-desktop"><?php echo e(trans('interface.url')); ?></span>
                                                </a>
                                            </td>
                                            <td class="desktop_orders_text"><?php echo e($order->order_date); ?> <?php echo e($order->order_time); ?></td>
                                            <td class="text-center"><?php echo e(trans('interface.count')); ?>: <?php echo e($order->count); ?></td>
                                            <td class="text-center"><?php echo e($order->price); ?> TL </td>
                                            <td style="width: 89px">
                                                <a class="btn btn-sm btn-warning edit-my-order" order-id="<?php echo e($order->id); ?>" title="Düzəliş"><span class="glyphicon glyphicon-edit"></span></a>
                                                <a class="btn btn-danger btn-sm d" order-id="<?php echo e($order->id); ?>"><span class="glyphicon glyphicon-trash"></span></a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="6"></td>
                                            <td class="content-hide-in-mobile">
                                                <a href="<?php echo e(route('pay',[$order->id])); ?>" class="btn btn-success btn-sm btn-block"><?php echo e(trans('interface.pay')); ?></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    <?php if($cargo_info): ?>
                                        <tr>
                                            <th colspan="7">
                                                Türkiyə kargosu
                                            </th>
                                        </tr>

                                        <?php $__currentLoopData = $cargo_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td colspan="7">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-xs-6 text-left"><?php echo e($key); ?></div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                                            <?php if($value['price'] == 0): ?>
                                                                Kargo bedava
                                                            <?php else: ?>
                                                                <?php echo e($value['price']); ?> TL
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>

                                    <?php if($haverushproduct): ?>
                                        <tr>
                                            <td colspan="7">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6 text-left">Təcili sifariş</div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6 text-right">1 $</div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                    </tbody>
                            </table>

                            <div class="col-md-12 text-right"><strong><?php echo e(trans('interface.total_price')); ?>: <?php echo e($total_basket_price->price_tl + $cargo_prices); ?> TL</strong></div>
                            <div class="col-md-12 text-right">
                                <br>
                                <button class="btn btn-success btn-sm">
                                    <span class="content-hide-in-mobile"><?php echo e(trans('interface.total_pay')); ?></span>
                                    <span class="content-hide-in-desktop"><?php echo e(trans('interface.pay')); ?></span>
                                </button>
                            </div>
                            </form>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- Order edit -->
        <div class="modal fade" id="myOrderEdit" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button class="btn btn-danger btn-sm own-close" data-dismiss="modal">&times;</button>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>
        <?php echo $__env->make('user.beyanname', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div style="clear: both"><br></div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>