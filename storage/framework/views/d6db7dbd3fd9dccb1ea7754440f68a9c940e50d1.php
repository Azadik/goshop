<?php $__env->startSection('content'); ?>
            <h2 class="own-header text-center"><?php echo e(trans('interface.faq')); ?></h2>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                        <div class="panel-group" id="accordion">
                            <?php $__currentLoopData = $faqs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $faq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo e($faq->id); ?>"><?php echo e($faq->$title); ?></a>
                                        </h4>
                                    </div>
                                    <div id="collapse<?php echo e($faq->id); ?>" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <?php echo $faq->$text; ?>

                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                </div>
            </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>