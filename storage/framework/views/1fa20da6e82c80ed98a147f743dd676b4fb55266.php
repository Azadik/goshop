<!DOCTYPE html>
<html lang="en">
<head>
    <title>Goshop.az</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://goshop.az/own/favicon.ico">
</head>
<body>


<div id="" class="main">
    <div class="container-fluid own-head" style=" padding: 0">
        <div class="container-fluid" style="background-color: #1796fb;">
            <br>
            <div class="row desktop-header">
               <div class="col-md-12 text-center" style="text-align: center;">
                   <a class="text-center" href="/"><img class="logo" src="https://goshop.az/own/logo.png"></a>
               </div>
            </div>
            <br>
        </div>
    </div>


    <br>
    <div class="container content padding-top-15">
       <div class="row">
           <div class="col-md-12"  style="text-align: center;">
               <h1>
                   <?php echo e($name); ?> <?php echo e($last_name); ?> - <?php echo e($amount); ?> TL ödəniş edib
               </h1>
           </div>
       </div>
    </div>

    <footer class="container-fluid">
        <div class="container own-footer-main">
            <div class="own-footer-div">
                <div class="own-footer-div-child"></div>
            </div>
        </div>


        <div class="container">
            <div class="col-sm-4 text-left"  style="text-align: center;">Copyright © <?=date('Y')?> GoShop.az is protected.</div>
            <br>
            <div class="col-sm-4 text-center" style="text-align: center;">
                <a href="https://wa.me/994555715692"><img src="https://goshop.az/own/whatsapp.png"></a>
                <a href="https://www.instagram.com/goshop.az"><img width="26" src="https://goshop.az/own/instagram.png"></a>&nbsp;
                <a href="https://m.me/goshop.az"><img width="26" src="https://goshop.az/own/messenger.png"></a>
            </div>
            <br>
            <div class="col-sm-4 text-right"  style="text-align: center;"><img src="https://goshop.az/own/visa.png">&nbsp;<img src="https://goshop.az/own/mastercard.png"></div>
        </div>
    </footer>
</div>
</body>
</html>