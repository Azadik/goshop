<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        
                    </div>

                </div>
                <div class="panel-body">
                  <div class="row">
                      <div class="col-md-12 col-sm-12">
                          <?php if(Session::has('msg')): ?>
                              <?php echo Session::get('msg'); ?>

                          <?php endif; ?>
                      </div>

                      <div class="col-sm-12">
                          <form action="<?php echo e(route('update.faq')); ?>" method="post">
                              <?php echo e(csrf_field()); ?>

                              <div class="row">
                                  <div class="col-sm-4">
                                      <div class="form-group">
                                          <label for="title_az">title az:</label>
                                          <input type="text" class="form-control" id="title_az" name="title_az" required value="<?php echo e($page->title_az); ?>">
                                      </div>
                                  </div>
                                  <div class="col-sm-4">
                                      <div class="form-group">
                                          <label for="title_en">title en:</label>
                                          <input type="text" class="form-control" id="title_en" name="title_en" required value="<?php echo e($page->title_en); ?>">
                                      </div>
                                  </div>
                                  <div class="col-sm-4">
                                      <div class="form-group">
                                          <label for="title_ru">title ru:</label>
                                          <input type="text" class="form-control" id="title_ru" name="title_ru" required value="<?php echo e($page->title_ru); ?>">
                                      </div>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label for="text_az">Text az:</label>
                                  <textarea class="form-control" rows="5" id="text_az" name="text_az"><?php echo e($page->text_az); ?></textarea>
                                  <script type="text/javascript">
                                      CKEDITOR.replace( 'text_az' );
                                  </script>
                              </div>

                              <div class="form-group">
                                  <label for="text_en">Text en:</label>
                                  <textarea class="form-control" rows="5" id="text_en" name="text_en"><?php echo e($page->text_en); ?></textarea>
                                  <script type="text/javascript">
                                      CKEDITOR.replace( 'text_en' );
                                  </script>
                              </div>

                              <div class="form-group">
                                  <label for="text_ru">Text ru:</label>
                                  <textarea class="form-control" rows="5" id="text_ru" name="text_ru"><?php echo e($page->text_ru); ?></textarea>
                                  <script type="text/javascript">
                                      CKEDITOR.replace( 'text_ru' );
                                  </script>
                              </div>
                              <input type="hidden" name="id" value="<?php echo e($page->id); ?>">

                              <button class="btn btn-sm btn-success btn-block">Update Faq</button>
                          </form>
                      </div>

                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>