<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            
                        </div>

                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <?php if(Session::has('msg')): ?>
                                    <?php echo Session::get('msg'); ?>

                                <?php endif; ?>
                            </div>

                            <div class="col-sm-12">
                                <form action="<?php echo e(route('update.menu')); ?>" method="post">
                                    <?php echo e(csrf_field()); ?>

                                    <div class="form-group">
                                        <label for="title_az">title az:</label>
                                        <input type="text" class="form-control" id="title_az" name="title_az" required value="<?php echo e($menu->title_az); ?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="title_en">title en:</label>
                                        <input type="text" class="form-control" id="title_en" name="title_en" required value="<?php echo e($menu->title_en); ?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="title_ru">title ru:</label>
                                        <input type="text" class="form-control" id="title_ru" name="title_ru" required value="<?php echo e($menu->title_ru); ?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="url">url</label>
                                        <input type="text" class="form-control" id="url" name="url" value="<?php echo e($menu->url); ?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="sort">sort number</label>
                                        <input type="number" class="form-control" id="sort" name="sort" value="<?php echo e($menu->sort); ?>">
                                    </div>

                                    <input type="hidden" name="id" value="<?php echo e($menu->id); ?>">
                                    <div class="col-sm-12 text-right">
                                        <button class="btn btn-sm btn-success">Update</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>