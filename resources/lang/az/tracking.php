<?php
/**
 * Created by PhpStorm.
 * User: Azad
 * Date: 21.03.2019
 * Time: 12:00
 */

return [
    'was_ordered'           =>'Sifariş verilib',
    'overseas_terminal'     =>'Xaricdəki anbardadır',
    'sent_to_baku'          =>'Bakıya göndərilib',
    'in_baku_office'        =>'Bakı ofisimizdədir',
    'was_handed_over'       =>'Təhvil verilib',
];