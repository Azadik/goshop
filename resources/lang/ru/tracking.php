<?php
/**
 * Created by PhpStorm.
 * User: Azad
 * Date: 21.03.2019
 * Time: 12:00
 */

return [
    'was_ordered'           =>'Заказан',
    'overseas_terminal'     =>'В амбаре',
    'sent_to_baku'          =>'Отправлен в Баку',
    'in_baku_office'        =>'В Бакинском оффисе',
    'was_handed_over'       =>'Был передан',
];