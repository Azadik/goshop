<?php
/**
 * Created by PhpStorm.
 * User: Azad
 * Date: 21.03.2019
 * Time: 12:00
 */

return [
    'was_ordered'           =>'Was ordered',
    'overseas_terminal'     =>'Overseas terminal',
    'sent_to_baku'          =>'Sent to Baku',
    'in_baku_office'        =>'In Baku`s office',
    'was_handed_over'       =>'Was handed over',
];