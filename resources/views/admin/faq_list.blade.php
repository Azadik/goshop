@extends('admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        {{--@include('admin.menu')--}}
                    </div>

                </div>
                <div class="panel-body">
                  <div class="row">
                      <div class="col-md-12 col-sm-12">
                          @if(Session::has('msg'))
                              {!! Session::get('msg') !!}
                          @endif
                      </div>

                      <div class="col-sm-12">
                          <table class="table table-hover">
                              <tbody>
                                  @foreach($pages as $page)
                                      <tr>
                                          <td>
                                              <a href="{{route('edit.faq', $page->id)}}">{{$page->title_az}}</a>
                                          </td>

                                          <td class="text-right">
                                              <a title="Delete" href="{{route('delete.faq', $page->id)}}" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this item?');">
                                                  <span class="glyphicon glyphicon-remove"></span>
                                              </a>
                                          </td>
                                      </tr>
                                  @endforeach
                              </tbody>
                          </table>
                      </div>

                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
