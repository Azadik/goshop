<nav class="navbar navbar-default">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li>
                <a class="btn btn-sm btn-danger" href="{{route('admin.dashboard')}}">Menu</a>
            </li>
            <li>
                <a class="btn btn-sm btn-danger" href="{{route('page')}}">Add Page</a>
            </li>
            <li>
                <a class="btn btn-sm btn-danger" href="{{route('page.list')}}">Pages</a>
            </li>
            <li>
                <a class="btn btn-sm btn-danger" href="{{route('faq')}}">Add Faq</a>
            </li>
            <li>
                <a class="btn btn-sm btn-danger" href="{{route('faq.list')}}">Faq list</a>
            </li>
            <li>
                <a class="btn btn-sm btn-danger" href="{{route('agreement')}}">Agreement</a>
            </li>
            <li>
                <a class="btn btn-sm btn-danger" href="{{route('rules')}}">Rules</a>
            </li>
        </ul>
    </div>
</nav>