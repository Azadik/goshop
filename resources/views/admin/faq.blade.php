@extends('admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        {{--@include('admin.menu')--}}
                    </div>

                </div>
                <div class="panel-body">
                  <div class="row">
                      <div class="col-md-12 col-sm-12">
                          @if(Session::has('msg'))
                              {!! Session::get('msg') !!}
                          @endif
                      </div>

                      <div class="col-sm-12">
                          <form action="{{route('add.faq')}}" method="post">
                              {{csrf_field()}}
                              <div class="row">
                                  <div class="col-sm-4">
                                      <div class="form-group">
                                          <label for="title_az">title az:</label>
                                          <input type="text" class="form-control" id="title_az" name="title_az" required>
                                      </div>
                                  </div>
                                  <div class="col-sm-4">
                                      <div class="form-group">
                                          <label for="title_en">title en:</label>
                                          <input type="text" class="form-control" id="title_en" name="title_en" required>
                                      </div>
                                  </div>
                                  <div class="col-sm-4">
                                      <div class="form-group">
                                          <label for="title_ru">title ru:</label>
                                          <input type="text" class="form-control" id="title_ru" name="title_ru" required>
                                      </div>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label for="text_az">Text az:</label>
                                  <textarea class="form-control" rows="5" id="text_az" name="text_az"></textarea>
                                  <script type="text/javascript">
                                      CKEDITOR.replace( 'text_az' );
                                  </script>
                              </div>

                              <div class="form-group">
                                  <label for="text_en">Text en:</label>
                                  <textarea class="form-control" rows="5" id="text_en" name="text_en"></textarea>
                                  <script type="text/javascript">
                                      CKEDITOR.replace( 'text_en' );
                                  </script>
                              </div>

                              <div class="form-group">
                                  <label for="text_ru">Text ru:</label>
                                  <textarea class="form-control" rows="5" id="text_ru" name="text_ru"></textarea>
                                  <script type="text/javascript">
                                      CKEDITOR.replace( 'text_ru' );
                                  </script>
                              </div>

                              <button class="btn btn-sm btn-success btn-block">Add Faq</button>
                          </form>
                      </div>

                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
