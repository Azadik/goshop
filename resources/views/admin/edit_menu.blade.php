@extends('admin.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            {{--@include('admin.menu')--}}
                        </div>

                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                @if(Session::has('msg'))
                                    {!! Session::get('msg') !!}
                                @endif
                            </div>

                            <div class="col-sm-12">
                                <form action="{{route('update.menu')}}" method="post">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="title_az">title az:</label>
                                        <input type="text" class="form-control" id="title_az" name="title_az" required value="{{$menu->title_az}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="title_en">title en:</label>
                                        <input type="text" class="form-control" id="title_en" name="title_en" required value="{{$menu->title_en}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="title_ru">title ru:</label>
                                        <input type="text" class="form-control" id="title_ru" name="title_ru" required value="{{$menu->title_ru}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="url">url</label>
                                        <input type="text" class="form-control" id="url" name="url" value="{{$menu->url}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="sort">sort number</label>
                                        <input type="number" class="form-control" id="sort" name="sort" value="{{$menu->sort}}">
                                    </div>

                                    <input type="hidden" name="id" value="{{$menu->id}}">
                                    <div class="col-sm-12 text-right">
                                        <button class="btn btn-sm btn-success">Update</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
