@extends('user.app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 text-center"><h1>Beyannameler</h1></div>
        @include('user.content_header')

        <div style="clear: both"><br></div>


        <div class="col-md-12 col-sm-12">
            <div class="order_show_page">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        @if(Session::has('msg'))
                            {!! Session::get('msg') !!}
                        @endif
                        <div id="d_o"></div>
                    </div>

                    <div class="col-md-12 col-sm-12">
                        <ul class="nav nav-tabs own_tabs_menu">
                            <li @if($status == 1) class="active" @endif><a href="{{route('user.delivery', 1)}}">
                                    <span class="mobile_orders_icons">
                                        <img src="{{asset('own/ordered.png')}}">
                                        <span class="label label-primary">{{$packages_count}}</span>
                                    </span>
                                    <span class="desktop_orders_text">{{trans('interface.ordered')}}
                                        <span class="label label-primary">{{$packages_count}}</span>
                                    </span>
                                </a>
                            </li>
                            <li @if($status == 2) class="active" @endif>
                                <a href="{{route('user.delivery', 2)}}">
                                    <span class="mobile_orders_icons">
                                        <img src="{{asset('own/warehouse.png')}}">
                                        <span class="label label-primary">{{$packages_count_inbaku}}</span>
                                   </span>

                                    <span class="desktop_orders_text">{{trans('interface.warehouse')}}
                                        <span class="label label-primary">{{$packages_count_inbaku}}</span>
                                    </span>
                                </a>
                            </li>
                            <li @if($status == 3) class="active" @endif>
                                <a href="{{route('user.delivery', 3)}}">
                                    <span class="mobile_orders_icons">
                                        <img src="{{asset('own/over_handed.png')}}">
                                        <span class="label label-primary">{{$packages_count_handet}}</span>
                                    </span>
                                    <span class="desktop_orders_text">{{trans('interface.was_handed_over')}}
                                        <span class="label label-primary">{{$packages_count_handet}}</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <br>

                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                    @foreach($packages as $package)
                                    <div class="row order-box-parent">
                                        <div class="col-md-12 col-sm-12 order-box">
                                            <div class="col-md-12 col-sm-12 order-box-header">
                                                <div class="row">
                                                <div class="col-md-10 col-sm-10">
                                                    <strong>{{trans('interface.shop')}}:
                                                            <span class="package_name_color">{{$package->magazin_name}}</span> | {{trans('interface.package_code')}}: {{$package->id}} | Dükandan alınan tarix: {{$package->date}}
                                                    </strong>
                                                </div>
                                                <div class="col-md-2 col-sm-2">
                                                    <strong>{{trans('interface.country')}}:
                                                        {{trans('interface.turkey')}}
                                                    </strong>
                                                </div>
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-sm-2 order-box-img">
                                                <img src="{{asset('own/order.png')}}" height="79" class="img-rounded" alt="Cinque Terre">
                                                <div class="order-count">{{$package->package_count}}</div>
                                            </div>
                                            <div class="col-md-10 col-sm-10 order-box-content">
                                                <table class="table table-bordered order-box-body-table">
                                                    <thead>
                                                    <tr>
                                                        <th>Sifariş nömrəsi</th>
                                                        <th>Kargo izləmə kodu</th>
                                                        <th>{{trans('interface.price')}}</th>
                                                        <th>{{trans('interface.weight')}}</th>
                                                        <th>{{trans('interface.delivery_fee')}}</th>
                                                        <th>{{trans('interface.dimensions')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>{{$package->sifarish_number}}</td>
                                                        <td>{{$package->tracking_number}}</td>
                                                        <td>
                                                            {{$package->price}}
                                                        </td>


                                                        @if($package->shipping_price)
                                                            <td>
                                                                @if($package->weight)
                                                                    {{$package->weight}} kg
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($package->shipping_price)
                                                                    {{$package->shipping_price}} $
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($package->size)
                                                                    {{$package->size}}
                                                                @endif
                                                            </td>
                                                        @else
                                                            <td colspan="3" class="alert alert-success">{{trans('interface.will_show_warehouse')}}</td>
                                                        @endif
                                                    </tr>
                                                    </tbody>
                                                </table>

                                                <table class="table table-bordered order-box-footer-table">
                                                    <tbody>
                                                    <tr>
                                                        <td class="show-product text-center">{{trans('interface.description')}}</td>
                                                        <td class="text-center a b_id blinking" b-id="{{$package->id}}"> {{trans('interface.package_moving')}}</td>
                                                        <td class="text-center"><button  class="btn btn-block btn-success edit-my-beyanname" package-id="{{$package->id}}" style="padding: 0px 4px;"><i class="glyphicon glyphicon-edit"></i> {{trans('interface.edit')}}</button>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 product-list" style="display: none;">
                                            <table class="table table-bordered products-in-order">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            @if($package->invois)
                                                                <img src="{{asset('beyanname')}}/{{$package->invois}}" class="img-responsive" alt="{{$package->magazin_name}}">
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {{$package->description}}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <hr>
                                    @endforeach
                            </div>
                        </div>

                        <div class="text-center">
                            {{$packages->links()}}
                        </div>

                    </div>
                </div>
            </div>
        </div>



        <!-- Package tracking -->
        <div class="modal fade" id="showTracking" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">{{trans('interface.package_moving')}}</h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{trans('interface.close')}}</button>
                    </div>
                </div>
            </div>
        </div>

        {{--edit beyanname--}}
        <div id="edit_beyanname" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">BƏYANNAMƏ BLANKI TAM VƏ DƏQİQ DOLDURULMALIDIR</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                </div>
            </div>
        </div>
        @include('user.beyanname')
        <div style="clear: both"><br></div>
    </div>
@endsection
