<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style type="text/css"> form { margin: 0 auto; width: 400px; }

        form div + div {
            margin-top: 1em;
        }

        label {
            display: inline-block;
            width: 90px;
            text-align: right;
        }

        input, select {
            width: 200px;
        }

        .button {
            padding-left: 90px;
        }

        button {
            margin-top: 1em;
            align: center;
            margin-left: 2em;
        } </style>
</head>
<body>
<h1 align="center">Test purchase</h1>


<form action="purchase.php" method="post">
    <div><label for="amount">Amount:</label> <input type="text" id="amount" name="amount" maxlength="12"></div>
    <div><label for="currency">Currency:</label> <select id="currency" name="currency">
            <option value="944">Azerbaijan manat (AZN)</option>
            <option value="840">USA dollar (USD)</option>
            <option value="978">Euro (EUR)</option>
        </select></div>
    <div><label for="language">Language:</label> <select id="language" name="language">
            <option value="az">Azerbaijan</option>
            <option value="en">English</option>
            <option value="ru">Russian</option>
        </select></div>
    <input type="hidden" id="description" name="description" value="Test purchase">
    <div class="button">
        <button type="submit" id="submit" name="submit">Submit payment</button>
    </div>
</form>



</body>
</html>