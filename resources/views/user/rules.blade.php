@extends('user.app')

@section('content')
            <h2 class="own-header text-center">{!! $rules->$title !!}</h2>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    {!! $rules->$text !!}
                </div>
            </div>
@endsection
