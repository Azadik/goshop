@extends('user.app')

@section('content')
            <h2 class="own-header text-center">{{trans('interface.faq')}}</h2>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                        <div class="panel-group" id="accordion">
                            @foreach($faqs as $faq)
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$faq->id}}">{{$faq->$title}}</a>
                                        </h4>
                                    </div>
                                    <div id="collapse{{$faq->id}}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            {!! $faq->$text !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                </div>
            </div>
@endsection
