@extends('user.app')

@section('content')
    <aside class="col-sm-6 col-sm-offset-3">
        <article class="card">
            <div class="card-body p-5">
                <p class="text-center">
                    <img src="{{asset('own/cchead.png')}}">
                </p>
                <p class="alert alert-info text-center"><strong>Məbləğ: {{$amount}} TL</strong></p>

                <form action="{{route('request.paymes')}}" method="post">
                    <div class="form-group">
                        <label for="username">Full name (on the card)</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input id="username" type="text" class="form-control" name="owner" required placeholder="Azer Babayev">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="cardNumber">Card number</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-credit-card"></i></span>
                            <input id="cardNumber" type="number" class="form-control" name="number" required placeholder="1231 1233 ...">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label><span class="hidden-xs">Kartın istifadə müddətinin bitmə tarixi:</span> </label>
                                <div class="form-inline">
                                    <select class="form-control" style="width:45%" name="expiryMonth" required>
                                            <option value="">Month</option>
                                            <option value="01">01 | Yanvar</option>
                                            <option value="02">02 | Fevral</option>
                                            <option value="03">03 | Mart</option>
                                            <option value="04">04 | Aprel</option>
                                            <option value="05">05 | May</option>
                                            <option value="06">06 | İyun</option>
                                            <option value="07">07 | İyul</option>
                                            <option value="08">08 | Avqust</option>
                                            <option value="09">09 | Sentyabr</option>
                                            <option value="10">10 | Oktyabr</option>
                                            <option value="11">11 | Noyabr</option>
                                            <option value="12">12 | Dekabr</option>
                                    </select>
                                    <span style="width:10%; text-align: center"> / </span>
                                    <select class="form-control" style="width:45%" name="expiryYear" required>
                                            <option value="">Year</option>
                                            <option value="16">2016</option>
                                            <option value="17">2017</option>
                                            <option value="18">2018</option>
                                            <option value="19">2019</option>
                                            <option value="20">2020</option>
                                            <option value="21">2021</option>
                                            <option value="22">2022</option>
                                            <option value="23">2023</option>
                                            <option value="24">2024</option>
                                            <option value="25">2025</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label data-toggle="tooltip">cvv2 / cvc2 <span style="cursor: pointer;" data-toggle="modal" data-target="#myModal"><img src="{{asset('own/info.png')}}"></span></label>
                                <input class="form-control" required="" type="text" name="cvv">

                                @foreach($products as $product=>$value)
                                    <input type="hidden" name="products[]" value="{{$value}}">
                                @endforeach

                                <input type="hidden" name="productPrice" value="{{$amount}}">
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-info btn-block"> Təsdiq  </button>
                </form>
            </div>
        </article>
    </aside>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body text-center">
                    <img class="img-rounded" style="width: 100%" src="{{asset('own/cvv.png')}}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">{{trans('interface.close')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection