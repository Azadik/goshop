@extends('user.app')

@section('content')
    <div class="row">
        @include('user.content_header')

        <div style="clear: both"><br></div>


        <div class="col-md-12 col-sm-12">
            <div class="order_show_page">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        @if(Session::has('msg'))
                            {!! Session::get('msg') !!}
                        @endif
                        <div id="d_o"></div>
                    </div>

                    <div class="col-md-12 col-sm-12">
                        @if(!$orders->isEmpty())
                            <h2 class="text-center">{{trans('interface.basket')}}</h2>
                            <form action="{{route('allPay')}}" method="get">
                            <table class="table mobile-table">
                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr>
                                            <td class="text-left"><input type="checkbox" name="orders[]" value="{{$order->id}}"></td>
                                            <td style="position: relative">
                                                @if($order->img)
                                                    <img src="{{asset('products')}}/{{$order->img}}">
                                                    @else
                                                    <img width="60" src="{{asset('products/no_img.png')}}">
                                                @endif
                                                <span class="productcode">{{trans('interface.code')}}:{{$order->id}}</span>
                                            </td>
                                            <td>
                                                <a href="{{$order->url}}" target="_blank" title="{{$order->url}}">
                                                    <span class="content-hide-in-mobile">
                                                        <?php echo substr($order->url, 0, 60) ?> ...
                                                    </span>
                                                    <p>
                                                        {{$order->note}}
                                                    </p>
                                                    <span class="content-hide-in-desktop">{{trans('interface.url')}}</span>
                                                </a>
                                            </td>
                                            <td class="desktop_orders_text">{{$order->order_date}} {{$order->order_time}}</td>
                                            <td class="text-center">{{trans('interface.count')}}: {{$order->count}}</td>
                                            <td class="text-center">{{$order->price}} TL </td>
                                            <td style="width: 89px">
                                                <a class="btn btn-sm btn-warning edit-my-order" order-id="{{$order->id}}" title="Düzəliş"><span class="glyphicon glyphicon-edit"></span></a>
                                                <a class="btn btn-danger btn-sm d" order-id="{{$order->id}}"><span class="glyphicon glyphicon-trash"></span></a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="6"></td>
                                            <td class="content-hide-in-mobile">
                                                <a href="{{route('pay',[$order->id])}}" class="btn btn-success btn-sm btn-block">{{trans('interface.pay')}}</a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    @if($cargo_info)
                                        <tr>
                                            <th colspan="7">
                                                Türkiyə kargosu
                                            </th>
                                        </tr>

                                        @foreach($cargo_info as $key=>$value)
                                            <tr>
                                                <td colspan="7">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-xs-6 text-left">{{$key}}</div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                                            @if($value['price'] == 0)
                                                                Kargo bedava
                                                            @else
                                                                {{$value['price']}} TL
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif

                                    @if($haverushproduct)
                                        <tr>
                                            <td colspan="7">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6 text-left">Təcili sifariş</div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6 text-right">1 $</div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                            </table>

                            <div class="col-md-12 text-right"><strong>{{trans('interface.total_price')}}: {{$total_basket_price->price_tl + $cargo_prices}} TL</strong></div>
                            <div class="col-md-12 text-right">
                                <br>
                                <button class="btn btn-success btn-sm">
                                    <span class="content-hide-in-mobile">{{trans('interface.total_pay')}}</span>
                                    <span class="content-hide-in-desktop">{{trans('interface.pay')}}</span>
                                </button>
                            </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <!-- Order edit -->
        <div class="modal fade" id="myOrderEdit" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button class="btn btn-danger btn-sm own-close" data-dismiss="modal">&times;</button>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>
        @include('user.beyanname')
        <div style="clear: both"><br></div>
    </div>
@endsection
