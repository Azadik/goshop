@extends('user.app')

@section('content')
      <div class="row">
          <div class="col-md-12 col-sm-12">
              <div class="white-background">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Img</th>
                                    <th>Title</th>
                                    <th>Sifarişin qeydi</th>
                                    <th>Sifariş tarixi</th>
                                    <th class="text-center">Say</th>
                                    <th class="text-right">Qiymət</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <?php
                                        if($order['country'] == 1){
                                            $valyuta = 'TL';
                                            $price = $order['price'];
                                        } else {
                                            $valyuta = 'USD';
                                            $price = $order['price_usa'];
                                        }

                                        if($order['img']){
                                            echo '<img src="'.asset('products/').'/'.$order['img'].'" alt="'.$order['title'].'" width="50">';
                                        }else{
                                            echo '<img src="'.asset('products/no_img.png').'" width="50">';
                                        }
                                        ?>
                                    </td>
                                    <td style="max-width: 300px">
                                        @if($order['title'])
                                            <a href="{{$order['url']}}" target="_blank">{{$order['title']}}</a>
                                        @else
                                            <a href="{{$order['url']}}" target="_blank">{{$order['url']}}</a>
                                        @endif
                                    </td>
                                    <td>{{$order['note']}}</td>
                                    <td>{{$order['order_date']}} <sup>{{$order['order_time']}}</sup></td>
                                    <td class="text-center">{{$order['count']}}</td>
                                    <td class="text-right">{{$price}} {{$valyuta}}</td>
                                </tr>

                                @if($cargo_info)
                                    <tr>
                                        <th colspan="6">
                                            Türkiyə kargosu
                                        </th>
                                    </tr>

                                    @foreach($cargo_info as $key=>$value)
                                        <tr>
                                            <td colspan="5">{{$key}}</td>
                                            <td class="text-right">
                                                @if($value['price'] == 0)
                                                    Kargo bedava
                                                @else
                                                    {{$value['price']}} TL
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif

                                @if($haverushproduct)
                                    <tr>
                                        <td colspan="5">Təcili sifariş</td>
                                        <td class="text-right">1 $</td>
                                    </tr>
                                @endif

                                <tr>
                                    <td colspan="5"></td>
                                    <td class="text-right"><b style="font-size: 16px">Total price: {{$total_prices->total_tl + $cargo_prices}} TL</b></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-12 text-center">
                            <form action="{{route('paymes.form')}}" method="post">
                                <p>
                                    <label><input type="checkbox" required value="1"> <a  target="_blank" href="{{route('guest.getpage', [8, 'xidmet-shertleri'])}}">Şərtlərlə</a> və <a target="_blank" href="{{route('user.rules')}}">qaydalarla</a> razıyam</label>
                                </p>

                                <input type="hidden" name="products[]" value="{{$order['id']}}">
                                <input type="hidden" name="amount" value="{{$total_prices->total_tl + $cargo_prices }}" maxlength="12">
                                <input type="hidden" id="description" name="description" value="{{Auth::user()->name}} {{Auth::user()->last_name}} -- {{$total_prices->total_tl}} TL / {{$total_prices->total_az + $cargo_prices}} AZN">
                                <button class="btn btn-success btn-sm">Ödə</button>
                            </form>
                        </div>

                    </div>
              </div>
          </div>
      </div>
      <br>
      <div style="clear: both"></div>

@endsection
