<div class="col-md-12 col-sm-12">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-6">
            <button class="btn btn-success" data-toggle="modal" data-target="#beyanname">Bəyannamə Əlavə Et</button>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
            <button class="btn btn-info mobile_hide_customer_code">{{trans('interface.customer_code')}}: {{Auth::user()->reference_code}}</button>
            <a href="{{route('addOrder')}}" class="btn btn-danger">
                <span class="glyphicon glyphicon-plus"></span> {{trans('interface.order_it')}}
            </a>
        </div>
    </div>
</div>