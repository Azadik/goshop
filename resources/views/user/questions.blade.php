@extends('user.app')

@section('content')
      <div class="row">
          <div class="col-md-12 col-sm-12 text-center"><h1>{{trans('interface.questions')}}</h1></div>
          <div class="col-md-12 col-sm-12">
              <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-6">
                      <button class="btn btn-success" data-toggle="modal" data-target="#myModal">
                          <i class="glyphicon glyphicon-plus"></i> Sorğu yarat
                      </button>
                      <button class="btn btn-success" data-toggle="modal" data-target="#beyanname">Bəyannamə Əlavə Et</button>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                      <button class="btn btn-info mobile_hide_customer_code">{{trans('interface.customer_code')}}: {{Auth::user()->reference_code}}</button>
                      <a href="{{route('addOrder')}}" class="btn btn-danger">
                          <span class="glyphicon glyphicon-plus"></span> {{trans('interface.order_it')}}
                      </a>
                  </div>
              </div>
          </div>

          @if ($errors->any())
          <div class="col-md-12 col-sm-12">
              <div class="row">
                  <div class="col-md-12">
                          <div class="alert alert-danger">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </div>
                  </div>
              </div>
          </div>
          @endif

          <div style="clear: both"><br></div>

          <div class="col-md-12 col-sm-12">
              <div class="white-background">
                  <div class="row">
                      <div class="col-md-12 col-sm-12">
                          @if(Session::has('msg'))
                              {!! Session::get('msg') !!}
                          @endif
                      </div>

                      <div class="col-md-12 col-sm-12">
                          <table class="table">
                              <thead>
                              <tr>
                                  <th>№</th>
                                  <th>{{trans('interface.title')}}</th>
                                  <th class="desktop_questions_text">{{trans('interface.date')}}</th>
                                  <th class="text-center desktop_questions_text">{{trans('interface.date_finish')}}</th>
                                  <th class="text-center">{{trans('interface.look')}}</th>
                              </tr>
                              </thead>
                              <tbody>

                              @foreach($questions as $question)
                              <tr>
                                  <td>{{$question->id}}</td>
                                  <td>{{$question->title}}</td>
                                  <td class="desktop_questions_text">{{$question->start_date}}</td>
                                  <td class="text-center desktop_questions_text">
                                      @if($question->status == 1)
                                          {{$question->finish_date}}
                                       @else
                                          <button data-id="{{$question->id}}" class="btn btn-success btn-sm finishQuestion" style="padding: 0px 4px;">
                                              <span class="glyphicon glyphicon-ok"></span> Bitir
                                          </button>
                                      @endif
                                  </td>
                                  <td  class="text-center">
                                      <a href="{{route('get.question', $question->id)}}" class="btn btn-info btn-sm" style="padding: 0px 4px;">
                                          <span class="glyphicon glyphicon-eye-open"></span>
                                      </a>
                                  </td>
                              </tr>
                              @endforeach
                              </tbody>
                          </table>

                          <div class="text-center">
                              {{$questions->links()}}
                          </div>
                      </div>

                  </div>
              </div>
          </div>
          <div style="clear: both"><br></div>
      </div>


      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">{{trans('interface.create_new_question')}}</h4>
                  </div>
                  <div class="modal-body">
                      <form action="{{route('create.question')}}" method="post" enctype="multipart/form-data">
                          {{csrf_field()}}

                          <div class="row">
                              <div class="col-md-6 col-sm-6 col-xs-12">

                                  <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                      <label for="title">{{trans('interface.title')}} <span class="own_danger_text">*</span></label>
                                      <input type="text" class="form-control" id="title" name="title" required value="{{ old('title') }}">
                                      @if ($errors->has('title'))
                                          <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                          </span>
                                      @endif
                                  </div>

                                  <div class="form-group">
                                      <label for="prioritet_id">{{trans('interface.prioritet')}} <span class="own_danger_text">*</span></label>
                                      <select class="form-control" id="prioritet_id" name="prioritet_id">
                                          @foreach($prioritets as $prioritet)
                                              <option value="{{$prioritet->id}}">{{$prioritet->$title}}</option>
                                          @endforeach
                                      </select>
                                  </div>


                                  <div class="form-group">
                                      <label for="link">{{trans('interface.url')}}</label>
                                      <input type="text" class="form-control" id="link" name="link" value="{{ old('link') }}">
                                  </div>

                                  <div class="form-group {{ $errors->has('img') ? ' has-error' : '' }}">
                                      <label>{{trans('interface.file_upload')}}</label>
                                      <div class="input-group">
                                            <span class="input-group-btn">
                                                <span class="btn btn-default btn-file">
                                                    {{trans('interface.browse')}}… <input type="file" name="img" id="imgInp">
                                                </span>
                                            </span>
                                          <input type="text" class="form-control" readonly>
                                      </div>
                                      <img id='img-upload'/>

                                      @if ($errors->has('img'))
                                          <span class="help-block">
                                            <strong>{{ $errors->first('img') }}</strong>
                                          </span>
                                      @endif
                                  </div>

                              </div>

                              <div class="col-md-6 col-sm-6 col-xs-12">
                                  <div class="form-group">
                                      <label for="movzu_id">{{trans('interface.theme')}} <span class="own_danger_text">*</span></label>
                                      <select class="form-control" id="movzu_id" name="movzu_id">
                                          @foreach($movzu as $movz)
                                              <option value="{{$movz->id}}">{{$movz->$title}}</option>
                                          @endforeach
                                      </select>
                                  </div>

                                  <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                                      <label for="comment">{{trans('interface.your_question')}} <span class="own_danger_text">*</span></label>
                                      <textarea class="form-control" rows="5" id="comment" name="text" required>{{ old('text') }}</textarea>
                                      @if ($errors->has('text'))
                                          <span class="help-block">
                                            <strong>{{ $errors->first('text') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>

                              <div class="col-md-12 text-right">
                                  <input class="btn btn-success btn-sm btn-block" type="submit" value="{{trans('interface.send')}}">
                              </div>
                          </div>
                      </form>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">{{trans('interface.close')}}</button>
                  </div>
              </div>
          </div>
      </div>
      @include('user.beyanname')
@endsection
