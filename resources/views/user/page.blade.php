@extends('user.app')

@section('content')
            <h2 class="own-header text-center">{!! $page->$title !!}</h2>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    {!! $page->$text !!}
                </div>
            </div>
@endsection
