@extends('user.app')

@section('content')
      <div class="row">
          @include('user.content_header')

          <div style="clear: both"><br></div>


          <div class="col-md-12 col-sm-12">
              <div class="white-background">
                  <div class="row">
                      <div class="col-md-12 col-sm-12">
                          @if(Session::has('msg'))
                              {!! Session::get('msg') !!}
                          @endif
                      </div>

                      <div class="col-md-3">
                              <div>
                                  <img
                                          @if(Auth::user()->img)
                                            src="{{asset('profile')}}/{{Auth::user()->img}}"
                                          @else
                                              @if(Auth::user()->gender == 1)
                                              src="{{asset('own/man.png')}}"
                                              @else
                                              src="{{asset('own/girl.png')}}"
                                              @endif
                                          @endif
                                       class="img-thumbnail" alt="{{Auth::user()->name}} {{Auth::user()->last_name}}" width="100%">
                              </div>
                              <form action="{{route('profile.img.upload')}}" method="post" enctype="multipart/form-data">
                                  <div>
                                      {{csrf_field()}}
                                      <div class="form-group {{ $errors->has('img') ? ' has-error' : '' }}">
                                          <div class="input-group">
                                            <span class="input-group-btn">
                                                <span class="btn btn-default btn-file">
                                                    Browse… <input type="file" name="img" id="imgInp">
                                                </span>
                                            </span>
                                              <input type="text" class="form-control" readonly>
                                          </div>
                                          <img id='img-upload' width="100%">

                                          @if ($errors->has('img'))
                                              <span class="help-block">
                                                <strong>{{ $errors->first('img') }}</strong>
                                              </span>
                                          @endif
                                      </div>
                                  </div>
                                  <div>
                                      <input class="btn btn-success btn-sm btn-block" type="submit" value="{{trans('interface.upload')}}">
                                  </div>
                              </form>
                      </div>


                      <div class="col-md-9 col-sm-9">

                              <div class="row">
                                  <form action="{{route('update.profile')}}" method="post">
                                      {{csrf_field()}}
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                          @if ($errors->any())
                                              <div class="alert alert-danger alert-dismissible fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                  <ul>
                                                      @foreach ($errors->all() as $error)
                                                          <li>{{ $error }}</li>
                                                      @endforeach
                                                  </ul>
                                              </div>
                                          @endif
                                      </div>

                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                              <label for="name">{{trans('interface.name')}}:</label>
                                              <input type="text" class="form-control" id="name" name="name" value="{{Auth::user()->name}}">
                                              <div class="errorMessage" style="display:none"></div>
                                              @if ($errors->has('name'))
                                                  <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                              <label for="last_name">{{trans('interface.last_name')}}:</label>
                                              <input type="text" class="form-control" id="last_name" name="last_name" value="{{Auth::user()->last_name}}">
                                              <div class="errorMessage" style="display:none"></div>
                                              @if ($errors->has('last_name'))
                                                  <span class="help-block">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <div class="form-group">
                                              <label for="email">{{trans('interface.email')}}:</label>
                                              <input type="text" readonly class="form-control" id="email" value="{{Auth::user()->email}}">
                                          </div>
                                      </div>


                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <div class="form-group {{ $errors->has('contact') ? ' has-error' : '' }}">
                                              <label for="contact">{{trans('interface.telephone')}}:</label>
                                              <input type="text" class="form-control" id="contact" name="contact" value="{{Auth::user()->contact}}">
                                              <div class="errorMessage" style="display:none"></div>
                                              @if ($errors->has('contact'))
                                                  <span class="help-block">
                                                 <strong>{{ $errors->first('contact') }}</strong>
                                              </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                              <label for="address">{{trans('interface.address_profile')}}:</label>
                                              <input type="text" class="form-control" id="address" name="address" value="{{Auth::user()->address}}">
                                              <div class="errorMessage" style="display:none"></div>
                                              @if ($errors->has('address'))
                                                  <span class="help-block">
                                                         <strong>{{ $errors->first('address') }}</strong>
                                                      </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <div class="form-group {{ $errors->has('passport') ? ' has-error' : '' }}">
                                              <label for="passport">{{trans('interface.passport_number')}}:</label>
                                              <input type="text" class="form-control" id="passport" name="passport" maxlength="80" value="{{Auth::user()->passport}}">
                                              @if ($errors->has('passport'))
                                                  <span class="help-block">
                                                            <strong>{{ $errors->first('passport') }}</strong>
                                                      </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-6 col-sm-offset-6">
                                          <div class="form-group {{ $errors->has('fin_code') ? ' has-error' : '' }}">
                                              <label for="fin_code">{{trans('interface.passport_fin_code')}}:</label>
                                              <span style="cursor: pointer;" data-toggle="modal" data-target="#myModal"><img src="{{asset('own/info.png')}}"></span>
                                              <input type="text" class="form-control" id="fin_code" name="fin_code" maxlength="80" value="{{Auth::user()->fin_code}}">
                                              @if ($errors->has('fin_code'))
                                                  <span class="help-block">
                                                            <strong>{{ $errors->first('fin_code') }}</strong>
                                                      </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                          <button type="submit" class="btn btn-success btn-sm text-uppercase">{{trans('interface.save')}}</button>
                                      </div>
                                  </form>
                              </div>

                          <div style="clear: both;"></div>
                          <hr>

                          <div class="row">
                              <form action="{{route('profile.change.password')}}" method="post">
                                  {{csrf_field()}}
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      <div class="form-group">
                                          <label for="old_pass">{{trans('interface.old_password')}}:</label>
                                          <input type="password" class="form-control" id="old_pass" name="old_pass">
                                      </div>
                                  </div>

                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                          <label for="password">{{trans('interface.password')}}:</label>
                                          <input class="form-control" size="60"  maxlength="256" name="password" id="password" type="password"  >
                                          <div class="errorMessage" style="display:none"></div>
                                          @if ($errors->has('password'))
                                              <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                          @endif
                                      </div>

                                      <div class="form-group">
                                          <label for="password-confirm">{{trans('interface.repeat_password')}}:</label>
                                          <input id="password-confirm" class="form-control"  size="60" maxlength="256" name="password_confirmation" type="password" >
                                          <div class="errorMessage" id="Users_repeatPassword_msg" style="display:none"></div>
                                      </div>
                                  </div>


                                  <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                      <button type="submit" class="btn btn-success btn-sm text-uppercase">{{trans('interface.update_password')}}</button>
                                  </div>
                              </form>

                                  <div class="modal fade" id="myModal" role="dialog">
                                      <div class="modal-dialog modal-lg">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title"></h4>
                                              </div>
                                              <div class="modal-body text-center">
                                                  <img class="img-rounded" style="width: 100%" src="{{asset('own/vesiqe.png')}}">
                                              </div>
                                              <div class="modal-footer">
                                                  <button type="button" class="btn btn-danger" data-dismiss="modal">{{trans('interface.close')}}</button>
                                              </div>
                                          </div>
                                      </div>
                                  </div>

                          </div>
                          <div style="clear: both;"><br></div>
                  </div>
              </div>
          </div>
              @include('user.beyanname')
          <div style="clear: both"><br></div>
      </div>
      </div>
@endsection
