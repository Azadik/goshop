@extends('user.app')

@section('content')
      <div class="row">
          @include('user.content_header')
          <div style="clear: both"><br></div>
          <div class="col-md-12 col-sm-12">
              <div class="white-background">
                  <div class="row">
                      <div class="col-md-12 col-sm-12">
                          @if(Session::has('msg'))
                              {!! Session::get('msg') !!}
                          @endif
                      </div>

                      <div class="col-md-12 col-sm-12">
                          <div class="panel panel-default">
                              <div class="panel-heading sorgu_img">
                                  @if($question->img)
                                      <img id="myImg" src="{{asset('questions')}}/{{$question->img}}" alt="Snow" style="width:100%;max-width:100px">
                                      <div id="myModal" class="modal">
                                          <span class="close_task_img">&times;</span>
                                          <img class="modal-content" id="img01">
                                          <div id="caption"></div>
                                      </div>
                                  @endif
                                  {{$question->text}}
                              </div>

                              <div class="panel-body chat_message">
                                  <div class="text-right">
                                      @if($question->status == 1)
                                          {{$question->finish_date}}
                                      @else
                                          <button data-id="{{$question->id}}" class="btn btn-success btn-sm finishQuestion" style="padding: 0px 4px;">
                                              <span class="glyphicon glyphicon-ok"></span> {{trans('interface.to_end')}}
                                          </button>
                                      @endif
                                  </div>
                                  <br>

                                  @foreach($comments as $comment)
                                      <div class="container_msg
                                            @if(Auth::user()->id != $comment->user_id)
                                              {{ 'darker' }}
                                              @endif
                                                ">

                                          @if($comment->user_id)
                                              @if($comment->user_img)
                                                <img src="{{asset('profile')}}/{{Auth::user()->img}}" alt="{{Auth::user()->name}} {{Auth::user()->last_name}}"
                                                     @if(Auth::user()->id == $comment->user_id)
                                                             {{ 'class=right' }}
                                                             @endif
                                                     style="width:100%;">
                                              @else
                                                  <?php
                                                  $f_name = substr(Auth::user()->name, 0,1);
                                                  $f_lastname = substr(Auth::user()->last_name, 0,1);
                                                  $profile_name = $f_name.'.'.$f_lastname
                                                  ?>
                                                  <div class="profile_name_right">{{$profile_name}}</div>
                                              @endif
                                          @else
                                              <img src="{{asset('profile')}}/admin.jpg" alt="Avatar"  style="width:100%;">
                                          @endif

                                          <p>{{$comment->text}}</p>
                                          <span style="font-size: 12px" class="
                                            @if(Auth::user()->id != $comment->user_id)
                                              {{ 'time-right' }}
                                            @else
                                              {{ 'time-left' }}
                                            @endif
                                           ">{{$comment->date}}</span>
                                      </div>
                                  @endforeach

                                      <form action="{{route('create.comment')}}" method="post">
                                          {{csrf_field()}}
                                          <input type="hidden" name="id" value="{{$question->id}}">
                                          <div class="form-group">
                                              <textarea class="form-control" rows="2" id="comment" name="comment" required></textarea>
                                          </div>

                                          <div class="text-right">
                                              <button class="btn btn-success btn-sm text-right"><i class="glyphicon glyphicon-send"></i>&nbsp; {{trans('interface.send')}}</button>
                                          </div>
                                      </form>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          @include('user.beyanname')
          <div style="clear: both"><br></div>
      </div>
@endsection
