@extends('user.app')

@section('content')
      <div class="row">
          {{--<div class="col-md-12 col-sm-12 text-center"><h1>Balans ({{$current_balance}} AZN)</h1></div>--}}
          @include('user.content_header')
          <div style="clear: both"><br></div>
          <div class="col-md-12 col-sm-12">
              <div class="col-md-12 col-sm-12 white-background">
                  <div class="row">

                      <div class="col-md-4 col-sm-4 col-xs-12">
                          <div class="balance_cart">
                              <img src="{{asset('own/mastercard_b.png')}}" width="100%">
                              <span class="balance_amout"><span><?php echo round($current_balance, 2); ?></span> AZN</span>
                          </div>
                            <br>
                          <div>
                              <form action="{{route('post.terminal')}}" method="post">
                                  {{csrf_field()}}
                                  <div class="form-group">
                                      <input class="form-control" type="text" name="amount" onkeyup="this.value=this.value.replace(/[^\d,.]*/g, '').replace(/([,.])[,.]+/g, '$1').replace(/^[^\d]*(\d+([.]\d{0,5})?).*$/g, '$1');" maxlength="12" required>
                                      <input type="hidden" id="description" name="description" value="{{Auth::user()->name}} {{Auth::user()->last_name}} -- Post terminal">
                                  </div>
                                  <button class="btn btn-info text-center own-btn-info text-uppercase btn-block">{{trans('interface.top_up_balance')}}</button>
                              </form>
                          </div>
                      </div>

                      {{--<div class="col-md-4 col-sm-4 col-xs-12 mobile-margin-top-10">--}}
                          {{--<div class="alert alert-danger"><strong>Borc: -{{Auth::user()->debt}} AZN</strong></div>--}}
                      {{--</div>--}}
                      <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-12 mobile-margin-top-10">
                          <div class="panel panel-default text-center">
                              <div class="panel-heading">
                                  {{trans('interface.last_30_expenses')}}
                              </div>
                              <div class="panel-body">
                                  <h3>{{$month_quota}} $</h3>

                                  <div>
                                      <br>
                                      <button class="btn btn-default" data-toggle="modal" data-target="#monthQuota">{{trans('interface.what_is_this')}}</button>
                                  </div>
                              </div>
                          </div>

                          <!-- Modal -->
                          <div class="modal fade" id="monthQuota" role="dialog">
                              <div class="modal-dialog modal-lg">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title"></h4>
                                      </div>
                                      <div class="modal-body">
                                          <p class="alert-info alert">{{trans('interface.tax_info')}}</p>
                                      </div>
                                      <div class="modal-footer">
                                          <button type="button" class="btn btn-danger" data-dismiss="modal">{{trans('interface.close')}}</button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <!-- Modal -->
                      </div>

                      <div style="clear: both"><br></div>

                      <div class="col-md-12 col-sm-12">
                          <table class="table table-striped">
                              <thead>
                              <tr>
                                  <th>{{trans('interface.date')}}</th>
                                  <th>{{trans('interface.operation')}}</th>
                              </tr>
                              </thead>
                              <tbody>
                              @foreach($balance as $bal)
                                  <tr>
                                      <td>{{$bal->date}} {{$bal->time}}</td>
                                      <td>{{$bal->balance}} AZN - Balans: {{$bal->current_balance}} AZN</td>
                                  </tr>
                              @endforeach
                              </tbody>
                          </table>
                      </div>

                      <div class="col-md-12 col-sm-12 text-center">
                          {{$balance->links()}}
                      </div>
                  </div>
              </div>
          </div>

          @include('user.beyanname')
          <div style="clear: both"><br></div>
      </div>
@endsection