@extends('user.app')

@section('content')
      <div class="row">
          <div class="col-md-12 col-sm-12">
              <div class="white-background">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-center alert alert-danger" style="padding-top: 100px; padding-bottom: 100px;">Daxil etdiyiniz məlumatlar yanlışdır! Kod: {{$trans_id}}</h2>
                        </div>
                    </div>
              </div>
          </div>
      </div>
      <br>
      <div style="clear: both"></div>
@endsection
