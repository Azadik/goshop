<div class="col-md-3 col-sm-3 content-hide-in-mobile">
    <div class="profil-items">
        <nav>
            <ul>
                {{--<li @if($type == 3) class="active" @endif><a  href="{{route('abroadAddress')}}"><i class="glyphicon glyphicon-user"></i> Xaricdəki ünvanlarım</a></li>--}}
                <li @if($type == 2) class="active" @endif><a href="{{route('myOrders')}}"><i class="glyphicon glyphicon-shopping-cart"></i> Sifarişlərim</a></li>
                <li @if($type == 4) class="active" @endif><a href="{{route('profile')}}"><i class="glyphicon glyphicon-cog"></i> Tənzimləmələr</a></li>
                <li @if($type == 7) class="active" @endif><a href="{{route('balance')}}"><i class="glyphicon glyphicon-credit-card"></i> Balans</a></li>
                <li @if($type == 5 or $type == 6) class="active" @endif><a href="{{route('questions')}}"><i class="glyphicon glyphicon-question-sign"></i> Sorğular<span class="badge badge-danger pull-right">{{$questions_count}}</span></a></li>
                {{--<li><a href=""><i class="glyphicon glyphicon-envelope"></i> Mesajlar<span class="badge badge-danger pull-right">0</span></a></li>--}}
                {{--<li><a href="{{ route('user.logout') }}"--}}
                       {{--onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('logout-form').submit();"><i class="glyphicon glyphicon-log-out"></i> Çıxış</a></li>--}}
            </ul>
        </nav>
    </div>

    <br>
    <div class="last-30-day">
        <div class="panel panel-default text-center">
            <div class="panel-heading" style="cursor: pointer;" data-toggle="modal" data-target="#monthQuota">
                Son 30 gün ərzində (?)
            </div>
            <div class="panel-body">@if($month_quota) {{$month_quota}} $ @else 0 @endif</div>
        </div>
    </div>
    <br>

    {{--<div class="balance">--}}
        {{--<div class="panel panel-default text-center">--}}
            {{--<div class="panel-heading">Balansim</div>--}}
            {{--<div class="panel-body"><strong>{{$current_balance}} AZN</strong></div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <!-- Modal -->
    <div class="modal fade" id="monthQuota" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <p>This is a large modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->

</div>