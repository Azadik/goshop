@extends('user.app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 text-center"><h1>{{trans('interface.my_orders')}}</h1></div>
        @include('user.content_header')

        <div style="clear: both"><br></div>


        <div class="col-md-12 col-sm-12">
            <div class="order_show_page">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        @if(Session::has('msg'))
                            {!! Session::get('msg') !!}
                        @endif
                        <div id="d_o"></div>
                    </div>

                    <div class="col-md-12 col-sm-12">
                        <div class="row">
                            @foreach($tm_products as $tm_product)
                                <div class="col-md-3 col-sm-3 col-xs-12" style="margin-bottom: 5px;">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            @if($tm_product->img)
                                                <a href="{{$tm_product->url}}" target="_blank" title="@if($tm_product->title) {{$tm_product->url}}  @endif">
                                                <img src="{{asset('products')}}/{{$tm_product->img}}" class="img-rounded" alt="{{$tm_product->img}}" width="100%">
                                                </a>
                                            @else
                                                <a href="{{$tm_product->url}}" target="_blank" title="@if($tm_product->title) {{$tm_product->url}}  @endif">
                                                <img src="{{asset('products/no_img.png')}}" class="img-rounded" width="100%">
                                                </a>
                                            @endif
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="text-center">
                                            @if($tm_product->insurance)
                                                <img src="{{asset('own/crown.png')}}" title="Bu məhsul sığortalanıb"><br>
                                            @endif
                                            @if(!$tm_product->insurance)

                                                <input class="check_insurance" type="checkbox" name="type" value="{{$tm_product->id}}" title="Sığortalamaq üçün klik edin">
                                            @endif
                                            </div>

                                            <div class="progress">
                                                <?php $random_progressbar = rand(25, 90); ?>
                                                <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="{{$random_progressbar}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$random_progressbar}}%">
                                                    {{$random_progressbar}}% Process
                                                </div>
                                            </div>
                                            <kbd>{{trans('interface.code')}}: {{$tm_product->id}}</kbd>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <br>

                        <ul class="nav nav-tabs own_tabs_menu">
                            <li class="active">
                                <a data-toggle="tab" href="#home">
                                    <span class="mobile_orders_icons">
                                        <img src="{{asset('own/ordered.png')}}">
                                        <span class="label label-primary">{{$count_sifarish}}</span>
                                    </span>
                                    <span class="desktop_orders_text">{{trans('interface.ordered')}}
                                        <span class="label label-primary">{{$count_sifarish}}</span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#menu1">
                                   <span class="mobile_orders_icons">
                                        <img src="{{asset('own/warehouse.png')}}">
                                        <span class="label label-primary">{{$count_in_baku}}</span>
                                   </span>

                                    <span class="desktop_orders_text">{{trans('interface.warehouse')}}
                                        <span class="label label-primary">{{$count_in_baku}}</span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#menu2">
                                    <span class="mobile_orders_icons">
                                        <img src="{{asset('own/over_handed.png')}}">
                                        <span class="label label-primary">{{$count_tehvil}}</span>
                                    </span>
                                    <span class="desktop_orders_text">{{trans('interface.was_handed_over')}}
                                        <span class="label label-primary">{{$count_tehvil}}</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <br>

                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                @foreach($products as $key=>$product)
                                    <div class="row order-box-parent">
                                        <div class="col-md-12 col-sm-12 order-box">
                                            <div class="col-md-12 col-sm-12 order-box-header">
                                                <div class="col-md-6 col-sm-6">
                                                    <strong>{{trans('interface.shop')}}:
                                                        @if($boutique_name[$key])
                                                            <span class="package_name_color">{{$boutique_name[$key]->name}}</span> | {{trans('interface.package_code')}}: {{$baglamalar[$key]->id}}
                                                        @endif
                                                    </strong>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <strong>{{trans('interface.country')}}:
                                                        <?php
                                                        foreach ($product as $p){
                                                            if($p->country == 1){
                                                                echo trans('interface.turkey');
                                                            }else{
                                                                echo trans('interface.usa');
                                                            }
                                                            break;
                                                        }
                                                        ?>
                                                    </strong>
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-sm-2 order-box-img">
                                                <img src="{{asset('own/order.png')}}" height="79" class="img-rounded" alt="Cinque Terre">
                                                <div class="order-count">{{$counter[$key]}}</div>
                                            </div>
                                            <div class="col-md-10 col-sm-10 order-box-content">
                                                <table class="table table-bordered order-box-body-table">
                                                    <thead>
                                                    <tr>
                                                        <th>{{trans('interface.date')}}</th>
                                                        <th>{{trans('interface.price')}}</th>
                                                        <th>{{trans('interface.weight')}}</th>
                                                        <th>{{trans('interface.delivery_fee')}}</th>
                                                        <th>{{trans('interface.dimensions')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>@if($baglamalar[$key]) {{$baglamalar[$key]->date}} @endif</td>
                                                        <td>
                                                            <?php
                                                            $total_price = 0.00;
                                                            foreach ($product as $p){
                                                                    $p->cargo_az ? $cargopr = $p->cargo_az : $cargopr = 0;
                                                                    $total_price = ($p->price_az * $p->count ) + $total_price  + $cargopr;
                                                            }
                                                            echo   sprintf("%.2f" . "", $total_price). ' AZN';
                                                            ?>
                                                        </td>

                                                        @if($baglamalar[$key])
                                                            <td>{{$baglamalar[$key]->weight}}</td>
                                                            <td>@if($baglamalar[$key]->shipping_price) {{$baglamalar[$key]->shipping_price}} @else 0 @endif $ </td>
                                                            <td>{{$baglamalar[$key]->size}}</td>
                                                        @else
                                                            <td colspan="3" class="text-center">{{trans('interface.will_show_warehouse')}}</td>
                                                        @endif

                                                    </tr>
                                                    </tbody>
                                                </table>

                                                <table class="table table-bordered order-box-footer-table">
                                                    <tbody>
                                                    <tr>
                                                        <td class="show-product text-center">{{trans('interface.show_products_inside_package')}}</td>
                                                        <td class="text-center a baglama_id" baglama-id="{{$baglamalar[$key]->id}}"> {{trans('interface.package_moving')}}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 product-list" style="display: none">
                                            <table class="table table-bordered products-in-order">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">{{trans('interface.number')}}</th>
                                                    <th>{{trans('interface.photo')}}</th>
                                                    <th>{{trans('interface.url')}}</th>
                                                    <th><span class="content-hide-in-mobile">{{trans('interface.client_note')}}</span><span class="content-hide-in-desktop">{{trans('interface.note')}}</span></th>
                                                    <th class="text-center">{{trans('interface.count')}}</th>
                                                    <th>{{trans('interface.price')}}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($product as $product_show)
                                                    @if($product_show->lost)<tr class="alert-danger alert"><td colspan="6" class="text-center">Bu mehsul anbara chatmayib ararshdirilir</td></tr>@endif
                                                    <tr @if($product_show->lost) class="alert-danger alert" @endif>
                                                        <td class="text-center">
                                                            @if($product_show->insurance)
                                                                <img src="{{asset('own/crown.png')}}" title="Bu məhsul sığortalanıb"><br>
                                                            @endif
                                                            {{$product_show->id}}
                                                            <br>
                                                            @if(!$product_show->insurance)
                                                                <input class="check_insurance" type="checkbox" name="type" value="{{$product_show->id}}">
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($product_show->img)
                                                                <img src="{{asset('products')}}/{{$product_show->img}}" class="img-rounded" width="80">
                                                            @else
                                                                <img src="{{asset('products/no_img.png')}}" class="img-rounded" width="80">
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a href="{{$product_show->url}}" target="_blank">
                                                                  <span class="content-hide-in-mobile">
                                                                      @if($product_show->title)
                                                                          {{$product_show->title}}
                                                                      @else
                                                                          <?php echo substr($product_show->url, 0, 35) ?> ...
                                                                      @endif
                                                                  </span>
                                                                <span class="content-hide-in-desktop">link</span>
                                                            </a>
                                                        </td>
                                                        <td>{{$product_show->note}}</td>
                                                        <td class="text-center">{{$product_show->count}}</td>
                                                        <td>
                                                            <?php
                                                                $price = $product_show->price_az * $product_show->count;
                                                                echo   sprintf("%.2f" . "", $price). ' AZN';
                                                            ?>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <hr>
                                @endforeach
                            </div>

                            <div id="menu1" class="tab-pane fade">
                                @foreach($products_b as $key=>$product)
                                    <div class="row order-box-parent">
                                        <div class="col-md-12 col-sm-12 order-box">
                                            <div class="col-md-12 col-sm-12 order-box-header">
                                                <div class="col-md-6 col-sm-6">
                                                    <strong>{{trans('interface.shop')}}:
                                                        @if($boutique_name_b[$key])
                                                            <span class="package_name_color">{{$boutique_name_b[$key]->name}}</span> | {{trans('interface.package_code')}}: {{$baglamalar_b[$key]->id}}
                                                        @endif
                                                    </strong>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <strong>{{trans('interface.country')}}:
                                                        <?php
                                                        foreach ($product as $p){
                                                            if($p->country == 1){
                                                                echo trans('interface.turkey');
                                                            }else{
                                                                echo trans('interface.usa');
                                                            }
                                                            break;
                                                        }
                                                        ?>
                                                    </strong>
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-sm-2 order-box-img">
                                                <img src="{{asset('own/order.png')}}" height="79" class="img-rounded" alt="Cinque Terre">
                                                <div class="order-count">{{$counter_b[$key]}}</div>
                                            </div>
                                            <div class="col-md-10 col-sm-10 order-box-content">
                                                <table class="table table-bordered order-box-body-table">
                                                    <thead>
                                                    <tr>
                                                        <th>{{trans('interface.date')}}</th>
                                                        <th>{{trans('interface.price')}}</th>
                                                        <th>{{trans('interface.weight')}}</th>
                                                        <th>{{trans('interface.delivery_fee')}}</th>
                                                        <th>{{trans('interface.dimensions')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>@if($baglamalar_b[$key]) {{$baglamalar_b[$key]->date}} @endif</td>
                                                        <td>
                                                            <?php
                                                            $total_price = 0.00;
                                                            foreach ($product as $p){
                                                                $p->cargo_az ? $cargopr = $p->cargo_az : $cargopr = 0;
                                                                $total_price = ($p->price_az * $p->count ) + $total_price  + $cargopr;
                                                            }
                                                            echo   sprintf("%.2f" . "", $total_price). ' AZN';
                                                            ?>
                                                        </td>

                                                        @if($baglamalar_b[$key])
                                                            <td>{{$baglamalar_b[$key]->weight}}</td>
                                                            <td>
                                                                @if($baglamalar_b[$key]->shipping_price) {{$baglamalar_b[$key]->shipping_price}} @else 0 @endif $</td>
                                                            <td>{{$baglamalar_b[$key]->size}}</td>
                                                        @else
                                                            <td colspan="3" class="text-center">{{trans('interface.will_show_warehouse')}}</td>
                                                        @endif
                                                    </tr>
                                                    </tbody>
                                                </table>

                                                <table class="table table-bordered order-box-footer-table">
                                                    <tbody>
                                                    <tr>
                                                        <td class="show-product text-center">{{trans('interface.show_products_inside_package')}}</td>
                                                        <td class="text-center a baglama_id" baglama-id="{{$baglamalar_b[$key]->id}}"> {{trans('interface.package_moving')}}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 product-list" style="display: none">
                                            <table class="table table-bordered products-in-order">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">{{trans('interface.number')}}</th>
                                                    <th>{{trans('interface.photo')}}</th>
                                                    <th>{{trans('interface.url')}}</th>
                                                    <th><span class="content-hide-in-mobile">{{trans('interface.client_note')}}</span><span class="content-hide-in-desktop">{{trans('interface.note')}}</span></th>
                                                    <th class="text-center">{{trans('interface.count')}}</th>
                                                    <th>{{trans('interface.price')}}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($product as $product_show)
                                                    <tr>
                                                        <td class="text-center">
                                                            @if($product_show->insurance)
                                                                <img src="{{asset('own/crown.png')}}" title="Bu məhsul sığortalanıb"><br>
                                                            @endif
                                                            {{$product_show->id}}
                                                            <br>
                                                            @if(!$product_show->insurance)
                                                                <input class="check_insurance" type="checkbox" name="type" value="{{$product_show->id}}">
                                                            @endif
                                                        </td>
                                                        <td><img src="{{asset('products')}}/{{$product_show->img}}" class="img-rounded" width="80"></td>
                                                        <td>
                                                            <a href="{{$product_show->url}}" target="_blank">
                                                                  <span class="content-hide-in-mobile">
                                                                      @if($product_show->title)
                                                                          {{$product_show->title}}
                                                                      @else
                                                                          <?php echo substr($product_show->url, 0, 35) ?> ...
                                                                      @endif
                                                                  </span>
                                                                <span class="content-hide-in-desktop">{{trans('interface.url')}}</span>
                                                            </a>
                                                        </td>
                                                        <td>{{$product_show->note}}</td>
                                                        <td class="text-center">{{$product_show->count}}</td>
                                                        <td>
                                                            <?php
                                                            $price = $product_show->price_az * $product_show->count;
                                                            echo   sprintf("%.2f" . "", $price). ' AZN';
                                                            ?>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <hr>
                                @endforeach
                            </div>

                            <div id="menu2" class="tab-pane fade">
                                @foreach($products_t as $key=>$product)
                                    <div class="row order-box-parent">
                                            <div class="col-md-12 col-sm-12 order-box">
                                                <div class="col-md-12 col-sm-12 order-box-header">
                                                    <div class="col-md-6 col-sm-6">
                                                        <strong>{{trans('interface.shop')}}:
                                                            @if($boutique_name_t[$key])
                                                                <span class="package_name_color">{{$boutique_name_t[$key]->name}}</span> | {{trans('interface.package_code')}}: {{$baglamalar_t[$key]->id}}
                                                            @endif
                                                        </strong>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <strong>{{trans('interface.country')}}:
                                                            <?php
                                                            foreach ($product as $p){
                                                                if($p->country == 1){
                                                                    echo trans('interface.turkey');
                                                                }else{
                                                                    echo trans('interface.usa');
                                                                }
                                                                break;
                                                            }
                                                            ?>
                                                        </strong>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 col-sm-2 order-box-img">
                                                    <img src="{{asset('own/order.png')}}" height="79" class="img-rounded" alt="Cinque Terre">
                                                    <div class="order-count">{{$counter_t[$key]}}</div>
                                                </div>
                                                <div class="col-md-10 col-sm-10 order-box-content">
                                                    <table class="table table-bordered order-box-body-table">
                                                        <thead>
                                                        <tr>
                                                            <th>{{trans('interface.date')}}</th>
                                                            <th>{{trans('interface.price')}}</th>
                                                            <th>{{trans('interface.weight')}}</th>
                                                            <th>{{trans('interface.delivery_fee')}}</th>
                                                            <th>{{trans('interface.dimensions')}}</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>@if($baglamalar_t[$key]) {{$baglamalar_t[$key]->date}} @endif</td>
                                                            <td>
                                                                <?php
                                                                $total_price = 0.00;
                                                                foreach ($product as $p){
                                                                    $p->cargo_az ? $cargopr = $p->cargo_az : $cargopr = 0;
                                                                    $total_price = ($p->price_az * $p->count ) + $total_price  + $cargopr;
                                                                }
                                                                echo   sprintf("%.2f" . "", $total_price). ' AZN';
                                                                ?>
                                                            </td>

                                                            @if($baglamalar_t[$key])
                                                                <td>{{$baglamalar_t[$key]->weight}}</td>
                                                                <td>
                                                                    @if($baglamalar_t[$key]->shipping_price) {{$baglamalar_t[$key]->shipping_price}} @else 0 @endif $</td>
                                                                <td>{{$baglamalar_t[$key]->size}}</td>
                                                            @else
                                                                <td colspan="3" class="text-center">{{trans('interface.will_show_warehouse')}}</td>
                                                            @endif

                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                    <table class="table table-bordered order-box-footer-table">
                                                        <tbody>
                                                        <tr>
                                                            <td class="show-product text-center">{{trans('interface.show_products_inside_package')}}</td>
                                                            <td class="text-center a baglama_id" baglama-id="{{$baglamalar_t[$key]->id}}"> {{trans('interface.package_moving')}}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12 product-list" style="display: none">
                                                <table class="table table-bordered products-in-order">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">{{trans('interface.number')}}</th>
                                                        <th>{{trans('interface.photo')}}</th>
                                                        <th>{{trans('interface.url')}}</th>
                                                        <th><span class="content-hide-in-mobile">{{trans('interface.client_note')}}</span><span class="content-hide-in-desktop">{{trans('interface.note')}}</span></th>
                                                        <th class="text-center">{{trans('interface.count')}}</th>
                                                        <th>{{trans('interface.price')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($product as $product_show)
                                                        <tr>
                                                            <td class="text-center">{{$product_show->id}}</td>
                                                            <td><img src="{{asset('products')}}/{{$product_show->img}}" class="img-rounded" width="80"></td>
                                                            <td>
                                                                <a href="{{$product_show->url}}" target="_blank">
                                                                  <span class="content-hide-in-mobile">
                                                                      @if($product_show->title)
                                                                          {{$product_show->title}}
                                                                      @else
                                                                          <?php echo substr($product_show->url, 0, 35) ?> ...
                                                                      @endif
                                                                  </span>
                                                                    <span class="content-hide-in-desktop">{{trans('interface.url')}}</span>
                                                                </a>
                                                            </td>
                                                            <td>{{$product_show->note}}</td>
                                                            <td class="text-center">{{$product_show->count}}</td>
                                                            <td>
                                                                <?php
                                                                $price = $product_show->price_az * $product_show->count;
                                                                echo   sprintf("%.2f" . "", $price). ' AZN';
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                    </div>
                                    <hr>
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Order edit -->
        <div class="modal fade" id="myOrderEdit" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button class="btn btn-danger btn-sm own-close" data-dismiss="modal">&times;</button>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>

        <!-- Package tracking -->
        <div class="modal fade" id="showTracking" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">{{trans('interface.package_moving')}}</h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{trans('interface.close')}}</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- Sigorta -->
        <div class="modal fade" id="insurance_modal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content insurance_data">
                </div>
            </div>
        </div>

        @include('user.beyanname')

        <div style="clear: both"><br></div>
    </div>

    <button class="btn btn-danger insurance_button">Sığortala</button>
@endsection
