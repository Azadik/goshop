@extends('user.app')
@section('content')
        <div class="row">
            <div class="col-lg-6 col-lg-push-3">
                <div class="panel padding-25 margin-top-25" style="margin-bottom: 50px; box-shadow: 0 6px 22px 1px rgba(0, 0, 0, 0.15);">
                    <div class="text-center margin-bottom-25">
                        <h3 class="heading text-uppercase">{!! trans('interface.register') !!}</h3>
                    </div>
                    <form role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <input class="form-control" size="60" placeholder="{!! trans('interface.name') !!}" maxlength="128" value="{{ old('name') }}" name="name" id="Users_name" type="text" required>
                                    <div class="errorMessage" id="Users_name_msg" style="display:none"></div>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <input class="form-control" size="60" maxlength="128" placeholder="{!! trans('interface.last_name') !!}" value="{{ old('last_name') }}" name="last_name" id="Users_lastname" type="text" required>
                                    <div class="errorMessage" id="Users_lastname_msg" style="display:none"></div>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-12 text-center">
                                <div class="form-group">
                                    <select class="form-control" name="gender" required>
                                        <option value="" disabled="" selected="" hidden="">{!! trans('interface.gender') !!}</option>
                                        <option value="0">{!! trans('interface.female') !!}</option>
                                        <option value="1">{!! trans('interface.male') !!}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-12 text-center">
                                <div class="form-group text-center inline-block">
                                    <div class="input-group">
                                        <select class="form-control" name="day" required>
                                            <option value="">{!! trans('interface.day') !!}</option>
                                            <option value="01">1</option>
                                            <option value="02">2</option>
                                            <option value="03">3</option>
                                            <option value="04">4</option>
                                            <option value="05">5</option>
                                            <option value="06">6</option>
                                            <option value="07">7</option>
                                            <option value="08">8</option>
                                            <option value="09">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                        </select>
                                        <span class="input-group-btn" style="width: 120px;">
                                <select class="form-control" name="month" required>
									<option value="">{!! trans('interface.month') !!}</option>
									  <option value="01">{!! trans('interface.january') !!}</option>
                                      <option value="02">{!! trans('interface.february') !!}</option>
                                      <option value="03">{!! trans('interface.march') !!}</option>
                                      <option value="04">{!! trans('interface.april') !!}</option>
                                      <option value="05">{!! trans('interface.may') !!}</option>
                                      <option value="06">{!! trans('interface.june') !!}</option>
                                      <option value="07">{!! trans('interface.july') !!}</option>
                                      <option value="08">{!! trans('interface.august') !!}</option>
                                      <option value="09">{!! trans('interface.september') !!}</option>
                                      <option value="10">{!! trans('interface.october') !!}</option>
                                      <option value="11">{!! trans('interface.november') !!}</option>
                                      <option value="12">{!! trans('interface.december') !!}</option>
								</select>

                                </span>
                                        <span class="input-group-btn" style="width: 100px;">
                                <select class="form-control" name="year" required>
									<option value="">{!! trans('interface.year') !!}</option>
                                    <?php
                                        for($i=1900; $i<=2018; $i++){
                                         echo '<option value="'.$i.'">'.$i.'</option>';
                                        }
                                    ?>
                                </select>
                                </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input class="form-control" placeholder="{!! trans('interface.email') !!}" size="60" maxlength="128" value="{{ old('email') }}" name="email" id="Users_email" type="text" required>
                                    <div class="errorMessage" id="Users_email_msg" style="display:none"></div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group{{ $errors->has('contact') ? ' has-error' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-btn" style="width: 80px;">
                                            <select class="form-control" name="operator" id="Users_operator">
                                                <option value="055">055</option>
                                                <option value="050">050</option>
                                                <option value="051">051</option>
                                                <option value="070">070</option>
                                                <option value="077">077</option>
                                            </select>
                                        </span>
                                        <input class="form-control" size="60" maxlength="7" placeholder="xxx xx xx" name="contact" id="Users_mobile" type="text" value="{{ old('contact') }}" required>
                                        <div class="errorMessage" id="Users_mobile_msg" style="display:none"></div>
                                    </div>
                                    @if ($errors->has('contact'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('contact') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <input class="form-control" size="60" placeholder="{!! trans('interface.password') !!}" maxlength="256" name="password" id="password" type="password"  required>
                                            <div class="errorMessage" id="Users_password_msg" style="display:none"></div>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input id="password-confirm" class="form-control" placeholder="{!! trans('interface.confirm_password') !!}" size="60" maxlength="256" name="password_confirmation" type="password" required>
                                            <div class="errorMessage" id="Users_repeatPassword_msg" style="display:none"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input class="form-control" placeholder="{!! trans('interface.address') !!}" value="{{ old('address') }}" size="60" maxlength="200" name="address" id="Users_address" type="text">
                                    <div class="errorMessage" id="Users_address_em_" style="display:none"></div>
                                </div>
                            </div>


                            <div class="col-lg-12">
                                <div class="form-group{{ $errors->has('passport') ? ' has-error' : '' }}">
                                    <input class="form-control" size="8" maxlength="8" placeholder="{!! trans('interface.passport') !!}" value="{{ old('passport') }}" name="passport" id="Users_passport" type="text" required>
                                    <div class="errorMessage" id="Users_passport_msg" style="display:none"></div>
                                    @if ($errors->has('passport'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('passport') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>



                            <div class="col-lg-12 text-right">
                                <span style="cursor: pointer;" data-toggle="modal" data-target="#myModal"><img src="{{asset('own/info.png')}}"></span>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input class="form-control"  placeholder="{{trans('interface.passport_fin_code')}}" value="{{ old('fin_code') }}" name="fin_code"  type="text" required>
                                </div>
                            </div>



                            <div class="col-lg-12 text-center">
                                <div class="form-group">
                                    <select class="form-control" name="social">
                                        <option value="" disabled="" selected="" hidden="">{!! trans('interface.how_find_us') !!}</option>
                                        <option value="1">{!! trans('interface.fb') !!}</option>
                                        <option value="2">{!! trans('interface.insta') !!}</option>
                                        <option value="3">{!! trans('interface.google') !!}</option>
                                        <option value="4">{!! trans('interface.radio') !!}</option>
                                        <option value="5">{!! trans('interface.tv') !!}</option>
                                        <option value="6">{!! trans('interface.from_friend') !!}</option>
                                        <option value="7">{!! trans('interface.other') !!}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-12 text-center diger" style="display: none;">
                                <div class="form-group">
                                    <textarea class="form-control" name="other"></textarea>
                                </div>
                            </div>

                        </div>

                        <div class="row margin-top-10">
                            <div class="col-lg-12 text-center">
                                <button type="submit" class="text-uppercase btn own-btn-info">{!! trans('interface.confirm') !!}</button>
                            </div>
                        </div>

                        <hr>

                        <div class="row margin-top-10">
                            <div class="col-lg-12 text-center">
                                <span class="innerText">{!! trans('interface.rules1') !!} <label class="">
                                        <a target="_blank" href="{{route('guest.agreement')}}" class="own-label-color">{!! trans('interface.rules2') !!}</a>
                                    </label> {!! trans('interface.rules3') !!} </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body text-center">
                        <img class="img-rounded" style="width: 100%" src="{{asset('own/vesiqe.png')}}">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{trans('interface.close')}}</button>
                    </div>
                </div>
            </div>
        </div>
@endsection
