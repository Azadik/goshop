<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('last_name')->nullable();
            $table->date('date')->nullable();
            $table->integer('gender')->nullable();
            $table->integer('reference_code')->nullable();
            $table->string('email')->unique();
            $table->decimal('debt', 11, 2);
            $table->string('password');
            $table->string('contact', 100)->nullable();
            $table->string('address', 100)->nullable();
            $table->string('passport', 100)->nullable();
            $table->string('fin_code', 20)->nullable();
            $table->string('social', 100)->nullable();
            $table->string('balance', 20)->nullable();
            $table->string('img', 100)->nullable();
            $table->text('other')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
