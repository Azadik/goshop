<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaglamaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baglama', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('order_id')->nullable();
            $table->decimal('weight', 11, 4)->nullable();
            $table->decimal('shipping_price', 11, 2)->nullable();
            $table->decimal('size', 11, 2)->nullable();
            $table->integer('boutique_id')->nullable();
            $table->date('date')->nullable();
            $table->date('outside_date')->nullable();
            $table->date('send_baku_date')->nullable();
            $table->date('in_baku_date')->nullable();
            $table->date('handed_over_date')->nullable();
            $table->integer('status')->nullable();
            $table->integer('country')->nullable();
            $table->decimal('shipping_price_az', 11, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('baglama');
    }
}
