<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->string('url', 150)->nullable();
            $table->string('country', 150)->nullable();
            $table->string('order_time', 150);
            $table->text('note')->nullable();
            $table->date('order_date')->nullable();
            $table->integer('count')->nullable();
            $table->decimal('price', 11, 2)->nullable();
            $table->decimal('weight', 11, 2)->nullable();
            $table->decimal('shipping_price', 11, 2)->nullable();
            $table->string('img', 100)->nullable();
            $table->integer('status')->nullable();
            $table->integer('boutique_id')->nullable();
            $table->decimal('price_az', 11,2)->nullable();
            $table->decimal('price_usa', 11,2)->nullable();
            $table->decimal('all_usd', 11,2)->nullable();
            $table->decimal('own_price', 11,2)->nullable();
            $table->decimal('order_number', 11,2)->nullable();
            $table->decimal('cargo', 11,2)->nullable();
            $table->decimal('cargo_az', 11,2)->nullable();
            $table->string('title', 100)->nullable();
            $table->integer('baglama_id')->nullable();
            $table->integer('img_status')->nullable();
            $table->integer('manual')->nullable();
            $table->integer('lost')->nullable();
            $table->integer('insurance')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
