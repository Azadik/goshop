<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $name;
    protected $last_name;
    protected $amount;

    public function __construct($name, $last_name, $amount)
    {
        $this->name = $name;
        $this->last_name = $last_name;
        $this->amount = $amount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.geted_order')
            ->with([
                'name'=> $this->name,
                'last_name'=> $this->last_name,
                'amount'=> $this->amount
            ])
            ->subject('New Order - '.$this->amount.' TL');
    }
}
