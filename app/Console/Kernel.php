<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Tools;
use App\Models\Slider;
use Illuminate\Support\Facades\DB;
use SimpleXMLElement;
use App\Http\Controllers\Cronscript\CopyDataController;

class Kernel extends ConsoleKernel
{
    public function tools(){
        return new Tools();
    }

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->call(function () {
            if($this->tools()->trendyol()){
                $slider_data = Slider::where('status', 1)->get();

                foreach ($slider_data as $s){
                    @unlink(public_path().'/slider/'.$s->img);
                    Slider::where('id', $s->id)->delete();
                }

                foreach ($this->tools()->trendyol() as $data){
                    $file = file_get_contents($data['img']);
                    $filename = rand(1, 100).'_'.time().'.jpg';
                    file_put_contents(public_path().'/slider/'.$filename, $file);

                    $slider = new Slider();
                    $slider->img = $filename;
                    $slider->title = $data['title'];
                    $slider->url = $data['url'];
                    $slider->status = 1;
                    $slider->save();
                }
            }else{
                $slider_data = Slider::where('status', 1)->get();
                foreach ($slider_data as $s){
                    @unlink(public_path().'/slider/'.$s->img);
                    Slider::where('id', $s->id)->delete();
                }
            }
        })->twiceDaily(1, 13);


        $schedule->call(function () {
                $current_date = date('d.m.Y');
                $url = 'https://www.cbar.az/currencies/'.$current_date.'.xml';
                $url = simplexml_load_file($url);

                foreach ($url->ValType[1]->Valute as $valyuta){
                    if($valyuta->attributes()['Code'] == 'USD'){
                        $usd = $valyuta->Value;
                    }

                    if($valyuta->attributes()['Code'] == 'TRY'){
                        $tl = $valyuta->Value;
                    }
                }

                DB::table('course_valute')->where('id', 1)->update([
                    'tl'=>$tl,
                    'usa'=>$usd
                ]);
        })->twiceDaily(6, 14);

        $schedule->call(function (){
            $copyImgTitle = new CopyDataController();
            $copyImgTitle->parseImageAndTitle();
        })->hourly();

        $schedule->call(function (){
            $copyImgTitle = new CopyDataController();
            $copyImgTitle->parseImageAndTitleTemp();
//            $copyImgTitle->setNullImgStatus();
        })->everyMinute();

//        $schedule->call(function (){
//            DB::table('orders')->whereNull('buy')->delete();
//        })->everyMinute();

        }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
