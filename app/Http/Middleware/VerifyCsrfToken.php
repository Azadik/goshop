<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */

    protected $except = [
        'orders/*',
        '/home/pay_mes',
        '/ru/home/pay_mes',
        '/en/home/pay_mes',
        '/home/paymes_form',
        '/ru/home/paymes_form',
        '/en/home/paymes_form',
        '/home/b',
        '/ru/home/b',
        '/en/home/b',
        '/home/b_update',
        '/ru/home/b_update',
        '/en/home/b_update',
        '/home/showinsurance',
        '/ru/home/showinsurance',
        '/en/home/showinsurance',
        '/home/insurancepay',
        '/ru/home/insurancepay',
        '/en/home/insurancepay',
    ];
}
