<?php

namespace App\Http\Controllers\Cronscript;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use SimpleXMLElement;

class CourseValuteController extends Controller
{
    public function getCourseValute(){
        $current_date = date('d.m.Y');
        $url = 'https://www.cbar.az/currencies/'.$current_date.'.xml';
        $url = simplexml_load_file($url);

//        dd($url->ValType[1]->Valute);
        foreach ($url->ValType[1]->Valute as $valyuta){
            if($valyuta->attributes()['Code'] == 'USD'){
                $usd = $valyuta->Value;
            }

            if($valyuta->attributes()['Code'] == 'TRY'){
                $tl = $valyuta->Value;
            }
        }

        DB::table('course_valute')->where('id', 1)->update([
            'tl'=>$tl,
            'usa'=>$usd
        ]);
    }
}
