<?php

namespace App\Http\Controllers\Cronscript;

use App\Models\Product;
use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use function PHPSTORM_META\type;
use Tools;
use Intervention\Image\Facades\Image;

class CopyDataController extends Controller
{

    public function tools(){
        return new Tools();
    }

    public function copyProductFromOneToOther(){
        DB::select('INSERT INTO products SELECT * FROM tm_products  WHERE status=3');
        DB::table('tm_products')->where('status',3)->delete();
    }


    public function parseImageAndTitle(){
        $products = DB::table('products')->whereNull('img')->whereNull('img_status')->get();

        foreach ($products as $product){
            $user_id = $product->user_id;
            $url = $product->url;

            $data = $this->tools()->siteParser($url);

            $img_url = $data['img'];
            $title = $data['title'];

            if($img_url){
                $u = $this->tools()->getSiteName($url);

                if($u == 'www.trendyol.com' || $u == 'trendyol.com'){
                    $img_url = 'https://www.trendyol.com/'.$img_url;
                }

                $file = file_get_contents($img_url);
                $filename = $user_id.'_'.time().'.png';
                file_put_contents(public_path().'/products/'.$filename, $file);
                $thumbnailpath = public_path().'/products/'.$filename;
                $img = Image::make($thumbnailpath)->resize(350, 200, function($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save($thumbnailpath);
            }else{
                $filename = null;
            }

            DB::table('products')->where('id', $product->id)->update([
                'img' => $filename,
                'title' => $title,
                'img_status' => 1,
            ]);
        }
    }

    public function parseImageAndTitleTemp(){
        $products = DB::table('tm_products')->whereNull('img')->whereNull('img_status')->get();
//        dd(count($products));

        foreach ($products as $product){
            $user_id = $product->user_id;
            $url = $product->url;

            $data = $this->tools()->siteParser($url);
            $img_url = $data['img'];
            $title = $data['title'];

            if($img_url){
                $u = $this->tools()->getSiteName($url);

                if($u == 'www.trendyol.com' || $u == 'trendyol.com'){
                    $img_url = 'https://www.trendyol.com/'.$img_url;
                }

                $file = file_get_contents($img_url);
                $filename = $user_id.'_'.time().'.png';
                file_put_contents(public_path().'/products/'.$filename, $file);
                $thumbnailpath = public_path().'/products/'.$filename;
                $img = Image::make($thumbnailpath)->resize(350, 200, function($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save($thumbnailpath);
            }else{
                $filename = null;
            }

            DB::table('tm_products')->where('id', $product->id)->update([
                'img' => $filename,
                'title' => $title,
                'img_status' => 1,
            ]);
        }
    }

    public function getSliderFromTrendyol(){
            if($this->tools()->trendyol()){
                $slider_data = Slider::where('status', 1)->get();

                foreach ($slider_data as $s){
                    @unlink(public_path().'/slider/'.$s->img);
                    Slider::where('id', $s->id)->delete();
                }


                foreach ($this->tools()->trendyol() as $data){
                    $file = file_get_contents($data['img']);
                    $filename = rand(1, 100).'_'.time().'.jpg';
                    file_put_contents(public_path().'/slider/'.$filename, $file);

                    $slider = new Slider();
                    $slider->img = $filename;
                    $slider->title = $data['title'];
                    $slider->url = $data['url'];
                    $slider->status = 1;
                    $slider->save();
                }

            }else{
                $slider_data = Slider::where('status', 1)->get();
                foreach ($slider_data as $s){
                    @unlink(public_path().'/slider/'.$s->img);
                    Slider::where('id', $s->id)->delete();
                }
            }
    }


    public function setNullImgStatus(){
        DB::select('UPDATE products SET img_status = NULL WHERE url LIKE "%trendyol.com%" and img is NULL or img = ""');
    }

}
