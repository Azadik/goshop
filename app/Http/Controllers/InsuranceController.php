<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;

class InsuranceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function generateprice($price)
    {
        if($price > 150){
            $without = $price - 150;
            $a = ceil($without/50);
            $result = $a * 1 + 2;
            return $result;
        }else{
            return 2;
        }

    }


    public function showInsurance(Request $request)
    {
        $course_valute = DB::table('course_valute')->find(1);
        $usd = $course_valute->usa;
        $insurance_price = 0;
        $products = Product::whereIn('id', $request->data)->get();
        echo '<form action="'.route('pay.insurance').'" method="post"><div class="modal-header">
                        <div class="text-right">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Bağla</button>
                        </div>
                        <h4 class="modal-title text-center">Siyahıdaki malların sığortası</h4>
                    </div>
                    <div class="modal-body alert-info">';
                foreach ($products as $product)
                {
                    echo '<div class="col-md-4 col-sm-4 col-xs-12">Malın nömrəsi: '.$product->id.' &nbsp; &nbsp; &nbsp; <a href="'.$product->url.'" target="_blank">Link</a></div>
                            <div class="col-md-4 col-sm-4 col-xs-12">Malın qiyməti: '.$product->own_price.' Tl</div>
                            <div class="col-md-4 col-sm-4 col-xs-12">Sığorta giyməti: '.$this->generateprice($product->own_price).' $</div><input type="hidden" name="products[]" value="'.$product->id.'">';
                    $insurance_price+=$this->generateprice($product->own_price);
                }
                $azn = $usd * $insurance_price;
                echo '<br>
                        <br>
                        <br>
                        <div class="col-md-4 col-md-offset-8"><strong>Ümumi qiymət: '.$insurance_price.' $</strong></div>
                        <div style="clear: both"></div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="price" value="'.$azn.'">
                            <button class="btn btn-success">Göndər</button>
                        </div></form>';
    }
}
