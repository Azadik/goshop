<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use function PHPSTORM_META\type;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $data['contact'] = $data['operator'].$data['contact'];
        return Validator::make($data, [
            'name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'contact' => 'required|max:255|unique:users',
            'passport' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:8|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $contact = $data['operator'].$data['contact'];
        $date    = $data['year'].'-'.$data['month'].'-'.$data['day'];

        $last_id = User::latest('id')->first();
        $reference_code = intval($last_id->id.rand(9999,99999));

        return User::create([
            'name'      => $data['name'],
            'last_name' => $data['last_name'],
            'email'     => $data['email'],
            'gender'    => $data['gender'],
            'address'   => $data['address'],
            'social'    => $data['social'],
            'other'     => $data['other'],
            'passport'  => $data['passport'],
            'fin_code'  => $data['fin_code'],
            'contact'   => $contact,
            'date'      => $date,
            'reference_code'      => $reference_code,
            'balance'   => 0,
            'password'  => bcrypt($data['password']),
        ]);
    }
}
