<?php

namespace App\Http\Controllers\Extensions;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    public function getProducts()
    {
        $producs = Product::where('user_id', 11)->get();
        return $producs;
    }
}
