<?php

namespace App\Http\Controllers;

use App\Models\Beyanname;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Question;
use App\Models\Admin\Menu;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;

class BeyannameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function beyanname(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'magazin_name' => 'required|min:2|max:255',
            'price' => 'required|min:1',
            'file' => 'required|mimes:jpeg,jpg,png',
        ]);

        if ($validator->fails())
        {
//            return response()->json(['errors'=> [0=>json_decode($request['json'])]] );
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $file = $request->file;
        $destinationPath = public_path() . '/beyanname/';
        $extension = $request->file->getClientOriginalExtension();
        $filenametostore = $user->id.'_'.rand(1, 100).'_'.time().'.'.$extension;
        $file->move($destinationPath, $filenametostore);


        $beyanname                      = new Beyanname();
        $beyanname->country             = $request->country;
        $beyanname->package_count       = 1;
        $beyanname->magazin_name        = $request->magazin_name;
//        $beyanname->sifarish_number     = $request->sifarish_number;
        $beyanname->price               = $request->price;
        $beyanname->price_type          = $request->price_type;
        $beyanname->date                = $request->date;
        $beyanname->mehsul_type         = $request->mehsul_type;
        $beyanname->tracking_number     = $request->tracking_number;
        $beyanname->invois              = $filenametostore;
        $beyanname->description         = $request->description;
        $beyanname->user_id             = $user->id;
        $beyanname->status              = 3;
        $beyanname->save();

        return response()->json(['success'=>'Bəyannaməniz uğurla əlavə edildi!']);
//        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Success!</div>');
    }


    public function delivery($status)
    {
        $user = Auth::user();
        if($status == 1){
            $packages       = Beyanname::whereIn('status', [3,4,5])->orderBy('id', 'DESC')->where('user_id', $user->id)->paginate(20);
        }elseif ($status == 2){
            $packages       = Beyanname::where('status', 6)->orderBy('id', 'DESC')->where('user_id', $user->id)->paginate(20);
        }else{
            $packages       = Beyanname::where('status', 7)->orderBy('id', 'DESC')->where('user_id', $user->id)->paginate(20);
        }
        $packages_count = Beyanname::whereIn('status', [3,4,5])->where('user_id', $user->id)->count();
        $packages_count_inbaku = Beyanname::where('status', 6)->where('user_id', $user->id)->count();
        $packages_count_handet = Beyanname::where('status', 7)->where('user_id', $user->id)->count();
        
        $addToCartProducts = DB::table('tm_products')->where('user_id', $user->id)->count();

        return view('user.delivery_list', [
            'title' => 'title_'.Config::get('app.locale'),
            'menus' => Menu::orderBy('sort', 'ASC')->get(),
            'type'              => 8,
            'addToCartProducts' => $addToCartProducts,
            'current_balance'   => $user->balance,
            'questions_count'   => Question::where('user_id', $user->id)->whereNull('status')->count(),

            'status'            => $status,
            'packages'          => $packages,
            'packages_count'    => $packages_count,
            'packages_count_inbaku'    => $packages_count_inbaku,
            'packages_count_handet'    => $packages_count_handet,
        ]);
    }

    //show Traking
    public function showTracking(Request $request){
        $baglama = DB::table('beyanname')->find($request->order_id);
        $language = new Lang();
        echo '
        <div class="row bs-wizard" style="border-bottom:0;">
                              <div class="col-xs-2 col-xs-offset-1 bs-wizard-step complete">
                                  <div class="text-center bs-wizard-stepnum">'.$language::get('tracking.was_ordered').'</div>
                                  <div class="progress"><div class="progress-bar"></div></div>
                                  <a href="#" class="bs-wizard-dot"></a>
                                  <div class="bs-wizard-info text-center">'.$baglama->date.'</div>
                              </div>

                              <div class="col-xs-2 bs-wizard-step '; if($baglama->outside_date){ echo 'complete'; }else{ echo 'disabled'; }  echo '"><!-- complete -->
                                  <div class="text-center bs-wizard-stepnum">'.$language::get('tracking.overseas_terminal').'</div>
                                  <div class="progress"><div class="progress-bar"></div></div>
                                  <a href="#" class="bs-wizard-dot"></a>
                                  <div class="bs-wizard-info text-center">'.$baglama->outside_date.'</div>
                              </div>

                              <div class="col-xs-2 bs-wizard-step '; if($baglama->send_baku_date){ echo 'complete'; }else{ echo 'disabled'; }  echo '"><!-- complete -->
                                  <div class="text-center bs-wizard-stepnum">'.$language::get('tracking.sent_to_baku').'</div>
                                  <div class="progress"><div class="progress-bar"></div></div>
                                  <a href="#" class="bs-wizard-dot"></a>
                                  <div class="bs-wizard-info text-center">'.$baglama->send_baku_date.'</div>
                              </div>

                              <div class="col-xs-2 bs-wizard-step '; if($baglama->in_baku_date){ echo 'complete'; }else{ echo 'disabled'; }  echo '"><!-- active -->
                                  <div class="text-center bs-wizard-stepnum">'.$language::get('tracking.in_baku_office').'</div>
                                  <div class="progress"><div class="progress-bar"></div></div>
                                  <a href="#" class="bs-wizard-dot"></a>
                                  <div class="bs-wizard-info text-center">'.$baglama->in_baku_date.'</div>
                              </div>

                              <div class="col-xs-2 bs-wizard-step '; if($baglama->handed_over_date){ echo 'complete'; }else{ echo 'disabled'; }  echo '"><!-- active -->
                                  <div class="text-center bs-wizard-stepnum">'.$language::get('tracking.was_handed_over').'</div>
                                  <div class="progress"><div class="progress-bar"></div></div>
                                  <a href="#" class="bs-wizard-dot"></a>
                                  <div class="bs-wizard-info text-center">'.$baglama->handed_over_date.'</div>
                              </div>
                          </div>
        ';
    }

    public function editBeyanname(Request $request)
    {
        $beyanname = Beyanname::find($request->order_id);
        $language = new Lang();
        echo '
                        <div class="alert alert-danger b-alert-danger" style="display:none"></div>
                        <div class="alert alert-success b-alert-success text-center text-uppercase" style="display:none"></div>
                        <div class="well b_e_well">
                            <form enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="hidden"  id="b_id" value="'.$request->order_id.'">
                                      
                                        <div class="form-group">
                                            <label>Mağaza adı <span class="own_danger_text">*</span></label>
                                            <input id="b_magazin_name" type="text" class="form-control" name="magazin_name" value="'.$beyanname->magazin_name.'" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Sifariş izləmə kodu</label>
                                            <input id="b_tracking_number" type="text" class="form-control" name="tracking_number"  placeholder="Sifarişiniz Türkiyə ofisinə təslim edildikdən sonra əlavə edilməlidir" value="'.$beyanname->tracking_number.'">
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                <div class="form-group">
                                                    <label>Sifariş tarixi</label>
                                                    <input id="b_date" type="text" class="form-control" name="date" value="'.$beyanname->date.'">
                                                </div>
                                            </div>
                                            <div class="col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="f_a_date">Xaricdəki Anbardadır</label>
                                                    <input id="f_a_date" type="text" class="form-control" value="'.$beyanname->f_a_date.'">
                                                </div>
                                            </div>
                                        </div>
                                        

                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Məhsul tipi <span class="own_danger_text">*</span></label>
                                            <input id="b_mehsul_type" type="text" class="form-control" name="mehsul_type" value="'.$beyanname->mehsul_type.'" required>
                                        </div>

                                        <div class="form-group">
                                            <label >Məhsulun qiyməti <span class="own_danger_text">*</span></label>
                                            <div class="input-group" style="width: 100%;">
                                                <input id="b_price" class="form-control" name="price" type="text" value="'.$beyanname->price.'">
                                                <span class="input-group-btn" style="width: 80px;">
                                                <select id="b_price_type" class="form-control" name="price_type">';
                                                   if($beyanname->price_type == 1){
                                                        echo '<option selected="selected" value="1">USD</option>
                                                        <option value="2">EUR</option>
                                                        <option value="3">TRY</option>';
                                                   }

                                                    if($beyanname->price_type == 2){
                                                        echo '<option value="1">USD</option>
                                                        <option selected="selected" value="2">EUR</option>
                                                        <option value="3">TRY</option>';
                                                    }

                                                    if($beyanname->price_type == 3){
                                                        echo '<option value="1">USD</option>
                                                        <option value="2">EUR</option>
                                                        <option selected="selected" value="3">TRY</option>';
                                                    }
                                            echo '</select>
                                            </span>
                                            </div>
                                        </div>

                                        <label>Invois (jpeg,jpg,png)</label>
                                        <div class="form-group">
                                            <input id="b_img" type="file"  class="form-control">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="text-danger">Təslimat nömrəsi, məhsul barədə məlumatı və bağlamada neçə malınız varsa bura yazın</label>
                                            <textarea id="b_description" class="form-control" rows="6" name="description">'.$beyanname->description.'</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-6 text-left"><button class="btn btn-success" id="beyanname_edit">'.$language::get('interface.edit').'</button></div>
                            </form>
                            <div class="col-md-6 col-sm-6 col-xs-6 text-right"><button type="button" class="btn btn-danger" data-dismiss="modal">'.$language::get('interface.close').'</button></div>

                            <div style="clear: both"></div>
                        </div>
        ';
    }

    public function updateBeyanname(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'magazin_name' => 'required|min:2|max:255',
            'price' => 'required|min:1',
        ]);

        if ($validator->fails())
        {
//            return response()->json(['errors'=> [0=>json_decode($request['json'])]] );
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        if($request->file != 'undefined'){
            $validator = Validator::make($request->all(), [
                'file' => 'required|mimes:jpeg,jpg,png',
            ]);

            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }


            $file = $request->file;
            $destinationPath = public_path() . '/beyanname/';
            $extension = $request->file->getClientOriginalExtension();
            $filenametostore = $user->id.'_'.rand(1, 100).'_'.time().'.'.$extension;
            $file->move($destinationPath, $filenametostore);

            $b = Beyanname::find($request->id);
            $img = $b->invois;
            @unlink($destinationPath.$img);

            Beyanname::where('id', $request->id)->update([
                'invois' => $filenametostore,
            ]);
        }

        Beyanname::where('id', $request->id)->update([
//            'package_count'       => $request->package_count,
            'magazin_name'        => $request->magazin_name,
//            'sifarish_number'     => $request->sifarish_number,
            'price'               => $request->price,
            'price_type'          => $request->price_type,
            'date'                => $request->date,
            'f_a_date'            => $request->f_a_date,
            'mehsul_type'         => $request->mehsul_type,
            'tracking_number'     => $request->tracking_number,
            'description'         => $request->description,
        ]);

        return response()->json(['success'=>'Bəyannaməniz uğurla yeniləndi!']);
    }
}