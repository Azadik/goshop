<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Admin\Menu;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use App\Models\Question;
use Tools;

class BalanceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function balance(){
        $user_id = Auth::user()->id;
        $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        $tools = new Tools();

        return view('user.balance', [
            'menus'             => Menu::orderBy('sort', 'ASC')->get(),
            'title'             => 'title_'.Config::get('app.locale'),
            'type'              => 7,
            'questions_count'   => Question::where('user_id', $user_id)->whereNull('status')->count(),
            'month_quota'       => $tools->totalUsdMonthQuota(),
            'addToCartProducts' => $addToCartProducts,
            'current_balance'   => Auth::user()->balance,
            'balance'           => DB::table('balance')->where('user_id', $user_id)->orderBy('id', 'DESC')->paginate(15),
        ]);
    }
}
