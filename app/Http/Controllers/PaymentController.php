<?php

namespace App\Http\Controllers;

use App\Models\InsuranceProduct;
use App\Models\InsuranceTransaction;
use App\Models\Order;
use App\Models\PaymEsTransaction;
use App\Models\PostTerminal;
use App\Models\TransactionData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Tools;
use App\Models\Admin\Menu;
use App\Models\Question;
use App\Models\CargoPrices;
use App\Models\Product;

class PaymentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


//Pashabank Paymen request
    protected function payment(Request $request){
        $ca = app_path('certificate/pasha/PSroot.pem');
        $key = app_path('certificate/privkey.pem');
        $cert = app_path('certificate/pasha/certificate.0010877.pem');
        $password = "goshop1";
        $merchant_handler = "https://ecomm.pashabank.az:18443/ecomm2/MerchantHandler";
        $client_handler = "https://ecomm.pashabank.az:8463/ecomm2/ClientHandler";
        $system_malfunction_page = "system_malfunction.html";
        $amount = filter_input(INPUT_POST, 'amount');
        $amount_to_bd = $amount;
        $amount = $amount * 100;

//        amount not numeric
        if (!is_numeric($amount) || strlen($amount) < 1 || strlen($amount) > 12) {
            session()->flash('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Yalnız rəqəm olmalıdır!</div>');
            return redirect()->route('payment.errors');
        }

        $currency = 944;

        $description = filter_input(INPUT_POST, 'description');

        if (strlen($description) > 125) {
            session()->flash('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Description problem!</div>');
            return redirect()->route('payment.errors');
        }

        $language = Config::get('app.locale');

        if (!ctype_alpha($language)) {
            session()->flash('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Language problem!</div>');
            return redirect()->route('payment.errors');
        }

        $params['command'] = "V";
        $params['amount'] = $amount;
        $params['currency'] = $currency;
        $params['description'] = $description;
        $params['language'] = $language;
        $params['msg_type'] = "SMS";

        if (filter_input(INPUT_SERVER, 'REMOTE_ADDR') != null) {
            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
        }elseif (filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR') != null) {
            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR');
        }elseif (filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP') != null) {
            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP');
        }else{
            $params['client_ip_addr'] = "127.0.0.1";
        }


        $qstring = http_build_query($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $merchant_handler);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $qstring);
        curl_setopt($ch, CURLOPT_SSLCERT, $cert);
        curl_setopt($ch, CURLOPT_SSLKEY, $key);
        curl_setopt($ch, CURLOPT_SSLKEYTYPE, "PEM");
        curl_setopt($ch, CURLOPT_SSLKEYPASSWD, $password);
        curl_setopt($ch, CURLOPT_CAPATH, $ca);
        curl_setopt($ch, CURLOPT_SSLCERTTYPE, "PEM");
        curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        $result = curl_exec($ch);

        if (curl_error($ch)) {
//            header("Location: " . $system_malfunction_page);
            session()->flash('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Daxil etdiyiniz məlumatlar yanlışdır!</div>');
            return redirect()->route('payment.errors');
        }
        curl_close($ch);

        $trans_ref = explode(' ', $result)[1];
        $transaction_id = $trans_ref;

        //        transactions data
        $user_id = Auth::user()->id;
        $transaction_data = new TransactionData();
        $transaction_data->user_id = $user_id;
        $transaction_data->trans_id = $transaction_id;
        $transaction_data->save();
        //        end

        $trans_ref = urlencode($trans_ref);
        $client_url = $client_handler . "?trans_id=" . $trans_ref;
//        end payment


        $course_valute = DB::table('course_valute')->find(1);
        $price_rushorder = $course_valute->usa * 1;
        $order = new Order();

        $rush_products_with_time = DB::table('tm_products')
            ->whereIn('id', $request->products)
            ->whereNotNull('boutique_time')
            ->where('user_id', $user_id)
            ->where('status', 1)
            ->orderBy('boutique_time', 'ASC')->first();


        if($rush_products_with_time){ //srochniy zakaz esli ukazan so vremenem
            $order->user_id = $user_id;
            $order->time = $rush_products_with_time->boutique_time;
            $order->status = 1;
            $order->price = $price_rushorder;
            $order->trans_id = $transaction_id;
            $order->amount = $amount_to_bd;
            $order->save();
        }else{
            $rush_products = DB::table('tm_products')
                ->whereIn('id', $request->products)
                ->whereNull('boutique_time')
                ->where('user_id', $user_id)
                ->where('status', 1)
                ->first();
            if($rush_products){  //srochniy zakaz bez ukazaniya vremeni
                $order->user_id = $user_id;
                $order->status = 1;
                $order->price = $price_rushorder;
                $order->trans_id = $transaction_id;
                $order->amount = $amount_to_bd;
                $order->save();
            }else{ // obichniy zakaz
                $order->user_id = $user_id;
                $order->trans_id = $transaction_id;
                $order->amount = $amount_to_bd;
                $order->save();
            }
        }



//      add cargo prices
        $tl = $course_valute->tl;
        $usd = $course_valute->usa;

        $tools = new Tools();
        $produklar = DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->get();

        $sites = [];
        foreach ($produklar as $pro){
            if(!in_array($tools->getSiteName($pro->url), $sites)){
                $price = DB::select('SELECT SUM(own_price * `count`) as total_price, country from tm_products WHERE url LIKE "%'.$tools->getSiteName($pro->url).'%" AND id IN ('.implode(',', $request->products).') group by country');
                $sites[$tools->getSiteName($pro->url)] = array('country'=> $price[0]->country, 'price' => $price[0]->total_price);
            }
        }

        foreach ($sites as $key=>$value){
            if($value['country'] == 1){
                $a = CargoPrices::where('site',  $key)->first();
                if($a){
                    if($value['price'] >= $a->limit ){
                        if($a->limit == 0){ //terndyol kimiler
                            $pr = DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->where('url', 'like', "%$key%")->first();
                            DB::table('tm_products')->where('id', $pr->id)->update([
                                'cargo' => $a->price,
                                'cargo_az' => $a->price * $tl,
                            ]);
                        }else{
                            $pr = DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->where('url', 'like', "%$key%")->first();
                            DB::table('tm_products')->where('id', $pr->id)->update([
                                'cargo' => $a->price,
                                'cargo_az' => $a->price * $tl,
                            ]);
                        }
                    }else{
                        $pr = DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->where('url', 'like', "%$key%")->first();
                        DB::table('tm_products')->where('id', $pr->id)->update([
                            'cargo' => $a->price,
                            'cargo_az' => $a->price * $tl,
                        ]);
                    }
                }else{
                    $pr = DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->where('url', 'like', "%$key%")->first();
                    DB::table('tm_products')->where('id', $pr->id)->update([
                        'cargo' => 7.99,
                        'cargo_az' => 7.99 * $tl,
                    ]);
                }
            }else{
//                USA sites
            }
        }
//      end


        DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->update([
            'temp_order_id' => $order->id,
        ]);

        header('Location: ' . $client_url);
    }


    public function postTerminal(Request $request){
        $ca = app_path('certificate/pasha/PSroot.pem');
        $key = app_path('certificate/privkey.pem');
        $cert = app_path('certificate/pasha/certificate.0010877.pem');
        $password = "goshop1";
        $merchant_handler = "https://ecomm.pashabank.az:18443/ecomm2/MerchantHandler";
        $client_handler = "https://ecomm.pashabank.az:8463/ecomm2/ClientHandler";
        $amount = filter_input(INPUT_POST, 'amount');
        $amount_to_bd = $amount;
        $amount = $amount * 100;

//        amount not numeric
        if (!is_numeric($amount) || strlen($amount) < 1 || strlen($amount) > 12) {
            session()->flash('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Yalnız rəqəm olmalıdır!</div>');
            return redirect()->route('payment.errors');
        }

        $currency = 944;

        $description = filter_input(INPUT_POST, 'description');

        if (strlen($description) > 125) {
            session()->flash('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Description problem!</div>');
            return redirect()->route('payment.errors');
        }

        $language = Config::get('app.locale');

        if (!ctype_alpha($language)) {
            session()->flash('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Language problem!</div>');
            return redirect()->route('payment.errors');
        }

        $params['command'] = "V";
        $params['amount'] = $amount;
        $params['currency'] = $currency;
        $params['description'] = $description;
        $params['language'] = $language;
        $params['msg_type'] = "SMS";

        if (filter_input(INPUT_SERVER, 'REMOTE_ADDR') != null) {
            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
        }elseif (filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR') != null) {
            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR');
        }elseif (filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP') != null) {
            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP');
        }else{
            $params['client_ip_addr'] = "127.0.0.1";
        }


        $qstring = http_build_query($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $merchant_handler);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $qstring);
        curl_setopt($ch, CURLOPT_SSLCERT, $cert);
        curl_setopt($ch, CURLOPT_SSLKEY, $key);
        curl_setopt($ch, CURLOPT_SSLKEYTYPE, "PEM");
        curl_setopt($ch, CURLOPT_SSLKEYPASSWD, $password);
        curl_setopt($ch, CURLOPT_CAPATH, $ca);
        curl_setopt($ch, CURLOPT_SSLCERTTYPE, "PEM");
        curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        $result = curl_exec($ch);

        if (curl_error($ch)) {
//            header("Location: " . $system_malfunction_page);
            session()->flash('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Daxil etdiyiniz məlumatlar yanlışdır!</div>');
            return redirect()->route('payment.errors');
        }
        curl_close($ch);

        $trans_ref = explode(' ', $result)[1];
        $transaction_id = $trans_ref;

        //        transactions data
        $user_id = Auth::user()->id;
        $transaction_data = new TransactionData();
        $transaction_data->user_id = $user_id;
        $transaction_data->trans_id = $transaction_id;
        $transaction_data->save();
        //        end

        $trans_ref = urlencode($trans_ref);
        $client_url = $client_handler . "?trans_id=" . $trans_ref;

        $post_terminal = new PostTerminal();
        $post_terminal->user_id = $user_id;
        $post_terminal->trans_id = $transaction_id;
        $post_terminal->amount = $amount_to_bd;
        $post_terminal->save();

        header('Location: ' . $client_url);
    }


    public function setPay(Request $request){
        $ca = app_path('certificate/pasha/PSroot.pem');
        $key = app_path('certificate/privkey.pem');
        $cert = app_path('certificate/pasha/certificate.0010877.pem');
        $password = "goshop1";
        $merchant_handler = "https://ecomm.pashabank.az:18443/ecomm2/MerchantHandler";
        $client_handler = "https://ecomm.pashabank.az:8463/ecomm2/ClientHandler";
        $system_malfunction_page = "system_malfunction.html";
        $amount = filter_input(INPUT_POST, 'amount');
        $amount = $amount * 100;


        if (!is_numeric($amount) || strlen($amount) < 1 || strlen($amount) > 12) {
            dd('amount not numeric');
        }

//        $currency = filter_input(INPUT_POST, 'currency');
//        if (!is_numeric($currency) || strlen($currency) != 3) {
//            dd('currency problem');
//        }

        $currency = 944;

        $description = filter_input(INPUT_POST, 'description');

        if (strlen($description) > 125) {
            dd('Descriptionda problem');
        }

        $language = Config::get('app.locale');


        if (!ctype_alpha($language)) {
            dd('language problem');
        }

        $params['command'] = "V";
        $params['amount'] = $amount;
        $params['currency'] = $currency;
        $params['description'] = $description;
        $params['language'] = $language;
        $params['msg_type'] = "SMS";

        if (filter_input(INPUT_SERVER, 'REMOTE_ADDR') != null) {
            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
        }elseif (filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR') != null) {
            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR');
        }elseif (filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP') != null) {
            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP');
        }else{
            $params['client_ip_addr'] = "127.0.0.1";
        }


        $qstring = http_build_query($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $merchant_handler);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $qstring);
        curl_setopt($ch, CURLOPT_SSLCERT, $cert);
        curl_setopt($ch, CURLOPT_SSLKEY, $key);
        curl_setopt($ch, CURLOPT_SSLKEYTYPE, "PEM");
        curl_setopt($ch, CURLOPT_SSLKEYPASSWD, $password);
        curl_setopt($ch, CURLOPT_CAPATH, $ca);
        curl_setopt($ch, CURLOPT_SSLCERTTYPE, "PEM");
        curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        $result = curl_exec($ch);

        if (curl_error($ch)) { header("Location: " . $system_malfunction_page); }
        curl_close($ch);

        $trans_ref = explode(' ', $result)[1];
        $trans_ref = urlencode($trans_ref);
        $client_url = $client_handler . "?trans_id=" . $trans_ref;

        header('Location: ' . $client_url);
    }


    public function successPayment(){
        $user_id = Auth::user()->id;
        $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        $tools = new Tools();

        return view('user.success',[
            'menus' => Menu::orderBy('sort', 'ASC')->get(),
            'title' => 'title_'.Config::get('app.locale'),
            'type' => 3,
            'current_balance'   => Auth::user()->balance,
            'month_quota'       => $tools->totalUsdMonthQuota(),
            'questions_count'   => Question::where('user_id', $user_id)->whereNull('status')->count(),
            'addToCartProducts' => $addToCartProducts,
        ]);
    }

    public function systemMalfunctionPage($trans_id){
        $user_id = Auth::user()->id;
        $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        $tools = new Tools();

        return view('user.system_malfunction_page',[
            'menus'             => Menu::orderBy('sort', 'ASC')->get(),
            'title'             => 'title_'.Config::get('app.locale'),
            'type'              => 3,
            'current_balance'   => Auth::user()->balance,
            'month_quota'       => $tools->totalUsdMonthQuota(),
            'questions_count'   => Question::where('user_id', $user_id)->whereNull('status')->count(),
            'addToCartProducts' => $addToCartProducts,
            'trans_id'          => $trans_id,
        ]);
    }

    public function insufficientFundsPage($trans_id){
        $user_id = Auth::user()->id;
        $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        $tools = new Tools();

        return view('user.insufficient_funds_page',[
            'menus'             => Menu::orderBy('sort', 'ASC')->get(),
            'title'             => 'title_'.Config::get('app.locale'),
            'type'              => 3,
            'current_balance'   => Auth::user()->balance,
            'month_quota'       => $tools->totalUsdMonthQuota(),
            'questions_count'   => Question::where('user_id', $user_id)->whereNull('status')->count(),
            'addToCartProducts' => $addToCartProducts,
            'trans_id'          => $trans_id,
        ]);
    }

    public function cardExpiredPage($trans_id){
        $user_id = Auth::user()->id;
        $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        $tools = new Tools();

        return view('user.card_expired_page',[
            'menus'             => Menu::orderBy('sort', 'ASC')->get(),
            'title'             => 'title_'.Config::get('app.locale'),
            'type'              => 3,
            'current_balance'   => Auth::user()->balance,
            'month_quota'       => $tools->totalUsdMonthQuota(),
            'questions_count'   => Question::where('user_id', $user_id)->whereNull('status')->count(),
            'addToCartProducts' => $addToCartProducts,
            'trans_id'          => $trans_id,
        ]);
    }


    public function paymentErrors(){
        $user_id = Auth::user()->id;
        $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        $tools = new Tools();
        return view('user.payment_errors', [
            'menus'             => Menu::orderBy('sort', 'ASC')->get(),
            'title'             => 'title_'.Config::get('app.locale'),
            'type'              => 3,
            'current_balance'   => Auth::user()->balance,
            'month_quota'       => $tools->totalUsdMonthQuota(),
            'questions_count'   => Question::where('user_id', $user_id)->whereNull('status')->count(),
            'addToCartProducts' => $addToCartProducts,
        ]);
    }

//    public function test(){
//        return \redirect()->route('success.payment');
//    }

        public function tools(){
            return new Tools();
        }



//  https://paym.es
    public function paymEsForm(Request $request){

        if(Auth::check()){
            $user_id            = Auth::user()->id;
            $tools              = new Tools();
            $current_balance    = Auth::user()->balance;
            $month_quota        = $tools->totalUsdMonthQuota();
            $questions_count    = Question::where('user_id', $user_id)->whereNull('status')->count();
            $addToCartProducts  = DB::table('tm_products')->where('user_id', $user_id)->count();
        } else {
            $current_balance    = 0;
            $month_quota        = 0;
            $questions_count    = 0;
            $addToCartProducts  = 0;
        }


        $amount = $request->amount;
        $products = $request->products;


        return view('user.paymes', [
            'type'              => 0,
            'menus'             => Menu::orderBy('sort', 'ASC')->get(),
            'title'             => 'title_'.Config::get('app.locale'),
            'text'              => 'text_'.Config::get('app.locale'),
            'current_balance'   => $current_balance,
            'month_quota'       => $month_quota,
            'questions_count'   => $questions_count,
            'addToCartProducts' => $addToCartProducts,

            'amount'            => $amount,
            'products'          => $products,
        ]);

    }


    public function paymEs(Request $request){
        // chtobi poluchit random product name
        $a = $request->productPrice;
        if($a <= 50){
            $min=0;
            $max = 50;
        }elseif ($a > 50 && $a <= 100){
            $min=50;
            $max = 100;
        }elseif ($a > 100 && $a <= 200){
            $min=100;
            $max = 200;
        }else{
            $min = 200;
            $max = 10000;
        }

        $get_products = DB::table('products')
            ->whereNotNull('title')
            ->where('own_price','>=' ,$min)
            ->where('own_price','<=', $max)
            ->inRandomOrder()->first();
        // end

        //$product_data = DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', Auth::user()->id)->first();
        $secret             ='pm3jrklo8B7liufNWq9S';
        $operationId        = 1;
        $number             =$request->number;
        $installmentsNumber =1;
        $expiryMonth        =$request->expiryMonth;
        $expiryYear         =$request->expiryYear;
        $cvv                =$request->cvv;
        $owner              =$request->owner;
        $billingFirstname   = Auth::user()->name;
        $billingLastname    = Auth::user()->last_name;
        $billingEmail       = Auth::user()->email;
        $billingPhone       = Auth::user()->contact;
        $billingCountrycode = 'AZ';
        $billingAddressline1 = Auth::user()->address;
        $billingCity        = 'Baku';
        $deliveryFirstname  = Auth::user()->name;
        $deliveryLastname   = Auth::user()->last_name;
        $deliveryPhone      = Auth::user()->contact;
        $deliveryAddressline1 = Auth::user()->address;
        $deliveryCity       = 'Baku';
        $clientIp           = $_SERVER['REMOTE_ADDR'];
        $productName        = $get_products->title;
        $productSku         = $get_products->count;
        $productQuantity    = $get_products->count;
        $productPrice       = $request->productPrice;
        $currency           = 'TRY';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://web.paym.es/api/authorize",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "secret=$secret&operationId=$operationId&number=$number&installmentsNumber=$installmentsNumber&expiryMonth=$expiryMonth&expiryYear=$expiryYear&cvv=$cvv&owner=$owner&billingFirstname=$billingFirstname&billingLastname=$billingLastname&billingEmail=$billingEmail&billingPhone=$billingPhone&billingCountrycode=$billingCountrycode&billingAddressline1=$billingAddressline1&billingCity=$billingCity&deliveryFirstname=$deliveryFirstname&deliveryLastname=$deliveryLastname&deliveryPhone=$deliveryPhone&deliveryAddressline1=$deliveryAddressline1&deliveryCity=$deliveryCity&clientIp=$clientIp&productName=$productName&productSku=$productSku&productQuantity=$productQuantity&productPrice=$productPrice&currency=$currency",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response, true);

            if(isset($response['paymentResult']['url'])){
                $transaction_id = $response['payuPaymentReference'];

//                *******************************

                //        transactions data
                $user_id = Auth::user()->id;
                $transaction_data = new PaymEsTransaction();
                $transaction_data->user_id = $user_id;
                $transaction_data->trans_id = $transaction_id;
                $transaction_data->amount = $request->productPrice;
                $transaction_data->save();
                //        end


                $course_valute = DB::table('course_valute')->find(1);
                $price_rushorder = $course_valute->usa * 1;

                $order = new Order();

                $rush_products_with_time = DB::table('tm_products')
                    ->whereIn('id', $request->products)
                    ->whereNotNull('boutique_time')
                    ->where('user_id', $user_id)
                    ->where('status', 1)
                    ->orderBy('boutique_time', 'ASC')->first();


                if($rush_products_with_time){ //srochniy zakaz esli ukazan so vremenem
                    $order->user_id = $user_id;
                    $order->time = $rush_products_with_time->boutique_time;
                    $order->status = 1;
                    $order->price = $price_rushorder;
                    $order->trans_id = $transaction_id;
                    $order->amount = $productPrice;
                    $order->save();
                }else{
                    $rush_products = DB::table('tm_products')
                        ->whereIn('id', $request->products)
                        ->whereNull('boutique_time')
                        ->where('user_id', $user_id)
                        ->where('status', 1)
                        ->first();
                    if($rush_products){  //srochniy zakaz bez ukazaniya vremeni
                        $order->user_id = $user_id;
                        $order->status = 1;
                        $order->price = $price_rushorder;
                        $order->trans_id = $transaction_id;
                        $order->amount = $productPrice;
                        $order->save();
                    }else{ // obichniy zakaz
                        $order->user_id = $user_id;
                        $order->trans_id = $transaction_id;
                        $order->amount = $productPrice;
                        $order->save();
                    }
                }



//      add cargo prices
                $tl = $course_valute->tl;
                $usd = $course_valute->usa;

                $tools = new Tools();
                $produklar = DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->get();

                $sites = [];
                foreach ($produklar as $pro){
                    if(!in_array($tools->getSiteName($pro->url), $sites)){
                        $price = DB::select('SELECT SUM(own_price * `count`) as total_price, country from tm_products WHERE url LIKE "%'.$tools->getSiteName($pro->url).'%" AND id IN ('.implode(',', $request->products).') group by country');
                        $sites[$tools->getSiteName($pro->url)] = array('country'=> $price[0]->country, 'price' => $price[0]->total_price);
                    }
                }

                foreach ($sites as $key=>$value){
                    if($value['country'] == 1){
                        $a = CargoPrices::where('site',  $key)->first();
                        if($a){
                            if($value['price'] >= $a->limit ){
                                if($a->limit == 0){ //terndyol kimiler
                                    $pr = DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->where('url', 'like', "%$key%")->first();
                                    DB::table('tm_products')->where('id', $pr->id)->update([
                                        'cargo' => $a->price,
                                        'cargo_az' => $a->price * $tl,
                                    ]);
                                }else{
                                    $pr = DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->where('url', 'like', "%$key%")->first();
                                    DB::table('tm_products')->where('id', $pr->id)->update([
                                        'cargo' => $a->price,
                                        'cargo_az' => $a->price * $tl,
                                    ]);
                                }
                            }else{
                                $pr = DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->where('url', 'like', "%$key%")->first();
                                DB::table('tm_products')->where('id', $pr->id)->update([
                                    'cargo' => $a->price,
                                    'cargo_az' => $a->price * $tl,
                                ]);
                            }
                        }else{
                            $pr = DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->where('url', 'like', "%$key%")->first();
                            DB::table('tm_products')->where('id', $pr->id)->update([
                                'cargo' => 7.99,
                                'cargo_az' => 7.99 * $tl,
                            ]);
                        }
                    }else{
//                USA sites
                    }
                }
//      end


                DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->update([
                    'temp_order_id' => $order->id,
                ]);
//                *******************************


                $url = $response['paymentResult']['url'];
                return Redirect::to($url);
            }else{
                session()->flash('msg', $response['message']);
                return redirect()->route('paymes.payment.result');
            }
        }
    }

    protected function paymentTESTING(Request $request){
//        payment
//        $ca = app_path('certificate/pasha/PSroot.pem');
//        $key = app_path('certificate/privkey.pem');
//        $cert = app_path('certificate/pasha/certificate.0010877.pem');
//        $password = "goshop1";
//        $merchant_handler = "https://ecomm.pashabank.az:18443/ecomm2/MerchantHandler";
//        $client_handler = "https://ecomm.pashabank.az:8463/ecomm2/ClientHandler";
//        $system_malfunction_page = "system_malfunction.html";
//        $amount = filter_input(INPUT_POST, 'amount');
//        $amount_to_bd = $amount;
//        $amount = $amount * 100;
//
//
////        amount not numeric
//        if (!is_numeric($amount) || strlen($amount) < 1 || strlen($amount) > 12) {
//            session()->flash('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Yalnız rəqəm olmalıdır!</div>');
//            return redirect()->route('payment.errors');
//        }
//
//        $currency = 944;
//
//        $description = filter_input(INPUT_POST, 'description');
//
//        if (strlen($description) > 125) {
//            session()->flash('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Description problem!</div>');
//            return redirect()->route('payment.errors');
//        }
//
//        $language = Config::get('app.locale');
//
//        if (!ctype_alpha($language)) {
//            session()->flash('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Language problem!</div>');
//            return redirect()->route('payment.errors');
//        }
//
//        $params['command'] = "V";
//        $params['amount'] = $amount;
//        $params['currency'] = $currency;
//        $params['description'] = $description;
//        $params['language'] = $language;
//        $params['msg_type'] = "SMS";
//
//        if (filter_input(INPUT_SERVER, 'REMOTE_ADDR') != null) {
//            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
//        }elseif (filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR') != null) {
//            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR');
//        }elseif (filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP') != null) {
//            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP');
//        }else{
//            $params['client_ip_addr'] = "127.0.0.1";
//        }
//
//
//        $qstring = http_build_query($params);
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $merchant_handler);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $qstring);
//        curl_setopt($ch, CURLOPT_SSLCERT, $cert);
//        curl_setopt($ch, CURLOPT_SSLKEY, $key);
//        curl_setopt($ch, CURLOPT_SSLKEYTYPE, "PEM");
//        curl_setopt($ch, CURLOPT_SSLKEYPASSWD, $password);
//        curl_setopt($ch, CURLOPT_CAPATH, $ca);
//        curl_setopt($ch, CURLOPT_SSLCERTTYPE, "PEM");
//        curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
//        $result = curl_exec($ch);
//
//        if (curl_error($ch)) {
////            header("Location: " . $system_malfunction_page);
//            session()->flash('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Daxil etdiyiniz məlumatlar yanlışdır!</div>');
//            return redirect()->route('payment.errors');
//        }
//        curl_close($ch);
//
//        $trans_ref = explode(' ', $result)[1];
//        $transaction_id = $trans_ref;
//
//        //        transactions data
        $user_id = Auth::user()->id;
//        $transaction_data = new TransactionData();
//        $transaction_data->user_id = $user_id;
//        $transaction_data->trans_id = $transaction_id;
//        $transaction_data->save();
//        //        end
//
//        $trans_ref = urlencode($trans_ref);
//        $client_url = $client_handler . "?trans_id=" . $trans_ref;
//        end payment


        $course_valute = DB::table('course_valute')->find(1);
        $price_rushorder = $course_valute->usa * 1;
        $order = new Order();

        $rush_products_with_time = DB::table('tm_products')
            ->whereIn('id', $request->products)
            ->whereNotNull('boutique_time')
            ->where('user_id', 1)
            ->where('status', 1)
            ->orderBy('boutique_time', 'ASC')->first();


        if($rush_products_with_time){ //srochniy zakaz esli ukazan so vremenem
            $order->user_id = $user_id;
            $order->time = $rush_products_with_time->boutique_time;
            $order->status = 1;
            $order->price = $price_rushorder;
            $order->trans_id = 123;
            $order->amount = 11;
            $order->save();
        }else{
            $rush_products = DB::table('tm_products')
                ->whereIn('id', $request->products)
                ->whereNull('boutique_time')
                ->where('user_id', $user_id)
                ->where('status', 1)
                ->first();
            if($rush_products){  //srochniy zakaz bez ukazaniya vremeni
                $order->user_id = $user_id;
                $order->status = 1;
                $order->price = $price_rushorder;
                $order->trans_id = 111;
                $order->amount = 11;
                $order->save();
            }else{ // obichniy zakaz
                $order->user_id = $user_id;
                $order->trans_id = 11;
                $order->amount = 111;
                $order->save();
            }
        }

//      add cargo prices
        $course_valute = DB::table('course_valute')->find(1);
        $tl = $course_valute->tl;
        $usd = $course_valute->usa;

        $tools = new Tools();
        $orders = DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->get();

        $sites = [];
        foreach ($orders as $order){
            if(!in_array($tools->getSiteName($order->url), $sites)){
                $price = DB::select('SELECT SUM(own_price * `count`) as total_price, country from tm_products WHERE url LIKE "%'.$tools->getSiteName($order->url).'%" AND id IN ('.implode(',', $request->products).') group by country');
                $sites[$tools->getSiteName($order->url)] = array('country'=> $price[0]->country, 'price' => $price[0]->total_price);
            }
        }


        foreach ($sites as $key=>$value){
            if($value['country'] == 1){
                $a = CargoPrices::where('site',  $key)->first();
                if($a){
                    if($value['price'] >= $a->limit ){
                        if($a->limit == 0){ //terndyol kimiler
                            $pr = DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->where('url', 'like', "%$key%")->first();
                            DB::table('tm_products')->where('id', $pr->id)->update([
                                'cargo' => $a->price,
                                'cargo_az' => $a->price * $tl,
                            ]);
                        }else{
                            $pr = DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->where('url', 'like', "%$key%")->first();
                            DB::table('tm_products')->where('id', $pr->id)->update([
                                'cargo' => $a->price,
                                'cargo_az' => $a->price * $tl,
                            ]);
                        }
                    }else{
                        $pr = DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->where('url', 'like', "%$key%")->first();
                        DB::table('tm_products')->where('id', $pr->id)->update([
                            'cargo' => $a->price,
                            'cargo_az' => $a->price * $tl,
                        ]);
                    }
                }else{
                    $pr = DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->where('url', 'like', "%$key%")->first();
                    DB::table('tm_products')->where('id', $pr->id)->update([
                        'cargo' => 7.99,
                        'cargo_az' => 7.99 * $tl,
                    ]);
                }
            }else{
//                USA sites
            }
        }
//      end

        DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->update([
            'order_id' => $order->id,
        ]);

//        header('Location: ' . $client_url);

//        DB::select('INSERT INTO products SELECT * FROM tm_products  WHERE id IN ('.implode(',', $request->products).')');
//        DB::table('tm_products')->whereIn('id', $request->products)->where('user_id', $user_id)->delete();

//        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Success!</div>');
    }

    public function paymEsGetMessage(){
        $user_id            = Auth::user()->id;
        $addToCartProducts  = DB::table('tm_products')->where('user_id', $user_id)->count();
        $tools              = new Tools();

        return view('user.paymes_message',[
            'menus'             => Menu::orderBy('sort', 'ASC')->get(),
            'title'             => 'title_'.Config::get('app.locale'),
            'type'              => 3,
            'current_balance'   => Auth::user()->balance,
            'month_quota'       => $tools->totalUsdMonthQuota(),
            'questions_count'   => Question::where('user_id', $user_id)->whereNull('status')->count(),
            'addToCartProducts' => $addToCartProducts,
//            'trans_id'          => $trans_id,
        ]);
    }


//mali sigortalama modolu
    protected function generateprice($price)
    {
        if($price > 150){
            $without = $price - 150;
            $a = ceil($without/50);
            $result = $a * 1 + 2;
            return $result;
        }else{
            return 2;
        }

    }
    public function insurancePayment(Request $request){
        $ca = app_path('certificate/pasha/PSroot.pem');
        $key = app_path('certificate/privkey.pem');
        $cert = app_path('certificate/pasha/certificate.0010877.pem');
        $password = "goshop1";
        $merchant_handler = "https://ecomm.pashabank.az:18443/ecomm2/MerchantHandler";
        $client_handler = "https://ecomm.pashabank.az:8463/ecomm2/ClientHandler";
        $amount = filter_input(INPUT_POST, 'price');
        $amount_to_bd = $amount;
        $amount = $amount * 100;

//        amount not numeric
        if (!is_numeric($amount) || strlen($amount) < 1 || strlen($amount) > 12) {
            session()->flash('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Yalnız rəqəm olmalıdır!</div>');
            return redirect()->route('payment.errors');
        }

        $currency = 944;
        $language = Config::get('app.locale');
        if (!ctype_alpha($language)) {
            session()->flash('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Language problem!</div>');
            return redirect()->route('payment.errors');
        }

        $params['command'] = "V";
        $params['amount'] = $amount;
        $params['currency'] = $currency;
        $params['language'] = $language;
        $params['msg_type'] = "SMS";

        if (filter_input(INPUT_SERVER, 'REMOTE_ADDR') != null) {
            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
        }elseif (filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR') != null) {
            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR');
        }elseif (filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP') != null) {
            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP');
        }else{
            $params['client_ip_addr'] = "127.0.0.1";
        }


        $qstring = http_build_query($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $merchant_handler);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $qstring);
        curl_setopt($ch, CURLOPT_SSLCERT, $cert);
        curl_setopt($ch, CURLOPT_SSLKEY, $key);
        curl_setopt($ch, CURLOPT_SSLKEYTYPE, "PEM");
        curl_setopt($ch, CURLOPT_SSLKEYPASSWD, $password);
        curl_setopt($ch, CURLOPT_CAPATH, $ca);
        curl_setopt($ch, CURLOPT_SSLCERTTYPE, "PEM");
        curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        $result = curl_exec($ch);

        if (curl_error($ch)) {
            session()->flash('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Daxil etdiyiniz məlumatlar yanlışdır!</div>');
            return redirect()->route('payment.errors');
        }
        curl_close($ch);

        $trans_ref = explode(' ', $result)[1];
        $transaction_id = $trans_ref;


        $trans_ref = urlencode($trans_ref);
        $client_url = $client_handler . "?trans_id=" . $trans_ref;



        $user_id = Auth::user()->id;
        $insurance_price = 0;
        $products = Product::whereIn('id', $request->products)->get();

        foreach ($products as $product)
        {
            $insProduct = new InsuranceProduct();
            $insProduct->user_id = $user_id;
            $insProduct->trans_id = $transaction_id;
            $insProduct->product_id = $product->id;
            $insProduct->product_price = $product->own_price;
            $insProduct->insurance_price = $this->generateprice($product->own_price);
            $insProduct->save();
            $insurance_price+=$this->generateprice($product->own_price);
        }

        // transactions data
        $transaction_data = new InsuranceTransaction();
        $transaction_data->user_id = $user_id;
        $transaction_data->trans_id = $transaction_id;
        $transaction_data->amount = $insurance_price;
        $transaction_data->save();
        // end


        header('Location: ' . $client_url);
    }
}
