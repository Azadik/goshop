<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Movzu;
use App\Models\Prioritet;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Models\Admin\Menu;
use Illuminate\Support\Facades\Config;
use Tools;


class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function questions(){
        $tools = new Tools();
        $user_id = Auth::user()->id;
        $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        return view('user.questions',[
            'menus'             => Menu::orderBy('sort', 'ASC')->get(),
            'title'             => 'title_'.Config::get('app.locale'),
            'type'              => 5,
            'current_balance'   => Auth::user()->balance,
            'prioritets'        => Prioritet::all(),
            'movzu'             => Movzu::all(),
            'questions'         => Question::where('user_id', $user_id)->orderBy('id', 'DESC')->paginate(20),
            'questions_count'   => Question::where('user_id', $user_id)->whereNull('status')->count(),
            'month_quota'       => $tools->totalUsdMonthQuota(),
            'addToCartProducts' => $addToCartProducts,
        ]);
    }


    public function getQuestion($id){
        $tools = new Tools();
        $user_id = Auth::user()->id;
        $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        $question = DB::table('questions')->where('id', $id)->where('user_id', $user_id)->first();
        $comments = DB::table('comments')
            ->leftJoin('users', 'comments.user_id', '=', 'users.id')
            ->where('questions_id', $id)
            ->select('users.name as user_name', 'users.last_name as user_lastname', 'users.img as user_img', 'comments.*')
            ->orderBy('id', 'DESC')->get();

        return view('user.get_question', [
            'menus' => Menu::orderBy('sort', 'ASC')->get(),
            'title' => 'title_'.Config::get('app.locale'),
            'type'              => 4,
            'current_balance'   => Auth::user()->balance,
            'question'          => $question,
            'comments'          => $comments,
            'questions_count'   => Question::where('user_id', $user_id)->whereNull('status')->count(),
            'month_quota'      => $tools->totalUsdMonthQuota(),
            'addToCartProducts'      => $addToCartProducts,
        ]);
    }

    public function createQuestion(Request $request){
        $user_id = Auth::user()->id;

        Validator::make($request->all(), [
            'title' => 'required|min:3|max:255',
            'text' => 'required|min:10',
            'img' => 'nullable|mimes:jpeg,jpg,png|max:2000',
        ])->validate();

        $questions = new Question();
        $questions->title = $request->title;
        $questions->text = $request->text;
        $questions->prioritet_id = $request->prioritet_id;
        $questions->link = $request->link;
        $questions->movzu_id = $request->movzu_id;
        $questions->user_id = $user_id;
        $questions->start_date = date('d-m-Y H:i:s');
        $questions->save();
        $question_id = $questions->id;


        if($request->img) {
            $file = $request->file('img');
            $destinationPath = public_path() . '/questions/';
            //get filename with extension
            $filenamewithextension = $request->file('img')->getClientOriginalName();

            //get filename without extension
//            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('img')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $user_id.'_'.rand(1, 100).'_'.time().'.'.$extension;

            $file->move($destinationPath, $filenametostore);


            //Resize image here
            $thumbnailpath = $destinationPath.$filenametostore;
            $img = Image::make($thumbnailpath)->resize(600, 300, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);

//            Image::make($thumbnailpath)->resizeCanvas(400, 400)->save($thumbnailpath);

            DB::table('questions')->where('id', $question_id)->update([
                'img' => $filenametostore
            ]);

        }
        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Sorgu uğurla göndərildi!</div>');
    }


    public function createComment(Request $request){
            $comment = new Comment();
            $comment->questions_id = $request->id;
            $comment->text = $request->comment;
            $comment->user_id = Auth::user()->id;
            $comment->date = date('d-m-Y H:i:s');
            $comment->save();
        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Success!</div>');
    }


    public function questionsFinish(Request $request){
        DB::table('questions')->where('id', $request->id)->update([
            'status' => 1,
            'finish_date' => date('d-m-Y H:i:s'),
            ]);
        return array("status" => 1);
    }

}
