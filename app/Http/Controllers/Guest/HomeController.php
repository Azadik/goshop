<?php

namespace App\Http\Controllers\Guest;

use App\Models\Admin\Menu;
use App\Models\Faq;
use App\Models\Page;
use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Tools;
use App\Models\Question;

class HomeController extends Controller
{
    public function index(){
        if(Auth::check()){
            $user_id = Auth::user()->id;
            $tools = new Tools();
            $current_balance = Auth::user()->balance;
            $month_quota     = $tools->totalUsdMonthQuota();
            $questions_count = Question::where('user_id', $user_id)->whereNull('status')->count();
            $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        }else{
            $current_balance = 0;
            $month_quota = 0;
            $questions_count = 0;
            $addToCartProducts = 0;
        }

        $course_valute = DB::table('course_valute')->find(1);
        $slider = Slider::all();

        return view('user.home', [
            'type' => 0,
            'course_valute' => $course_valute,
            'menus' => Menu::orderBy('sort', 'ASC')->get(),
            'title' => 'title_'.Config::get('app.locale'),
            'current_balance' => $current_balance,
            'month_quota'     => $month_quota,
            'questions_count'   => $questions_count,
            'addToCartProducts'   => $addToCartProducts,
            'slider'   => $slider,
        ]);
    }


    public function getPage($id, $string){
        if(Auth::check()){
            $user_id = Auth::user()->id;
            $tools = new Tools();
            $current_balance = Auth::user()->balance;
            $month_quota     = $tools->totalUsdMonthQuota();
            $questions_count = Question::where('user_id', $user_id)->whereNull('status')->count();
            $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        }else{
            $current_balance = 0;
            $month_quota = 0;
            $questions_count = 0;
            $addToCartProducts = 0;
        }


        return view('user.page', [
            'type'                  => 0,
            'menus'                 => Menu::orderBy('sort', 'ASC')->get(),
            'title'                 => 'title_'.Config::get('app.locale'),
            'text'                  => 'text_'.Config::get('app.locale'),
            'page'                  => Page::find($id),
            'current_balance'       => $current_balance,
            'month_quota'           => $month_quota,
            'questions_count'       => $questions_count,
            'addToCartProducts'     => $addToCartProducts,
        ]);
    }

    public function getPrice(){
        $course_valute = DB::table('course_valute')->find(1);
        if(Auth::check()){
            $user_id = Auth::user()->id;
            $tools = new Tools();
            $current_balance = Auth::user()->balance;
            $month_quota     = $tools->totalUsdMonthQuota();
            $questions_count = Question::where('user_id', $user_id)->whereNull('status')->count();
            $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        }else{
            $current_balance = 0;
            $month_quota = 0;
            $questions_count = 0;
            $addToCartProducts = 0;
        }

        return view('user.price', [
            'type' => 0,
            'course_valute' => $course_valute,
            'menus' => Menu::orderBy('sort', 'ASC')->get(),
            'title' => 'title_'.Config::get('app.locale'),
            'current_balance' => $current_balance,
            'month_quota'     => $month_quota,
            'questions_count'   => $questions_count,
            'addToCartProducts'   => $addToCartProducts,
        ]);
    }

    public function getSites(){
        if(Auth::check()){
            $user_id = Auth::user()->id;
            $tools = new Tools();
            $current_balance = Auth::user()->balance;
            $month_quota     = $tools->totalUsdMonthQuota();
            $questions_count = Question::where('user_id', $user_id)->whereNull('status')->count();
            $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        }else{
            $current_balance = 0;
            $month_quota = 0;
            $questions_count = 0;
            $addToCartProducts = 0;
        }

        return view('user.sites', [
            'type' => 0,
            'menus' => Menu::orderBy('sort', 'ASC')->get(),
            'title' => 'title_'.Config::get('app.locale'),
            'text' => 'text_'.Config::get('app.locale'),
            'current_balance' => $current_balance,
            'month_quota'     => $month_quota,
            'questions_count'   => $questions_count,
            'addToCartProducts'   => $addToCartProducts,
        ]);
    }

    public function faq(){
        if(Auth::check()){
            $user_id = Auth::user()->id;
            $tools = new Tools();
            $current_balance = Auth::user()->balance;
            $month_quota     = $tools->totalUsdMonthQuota();
            $questions_count = Question::where('user_id', $user_id)->whereNull('status')->count();
            $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        }else{
            $current_balance = 0;
            $month_quota = 0;
            $questions_count = 0;
            $addToCartProducts = 0;
        }

        return view('user.faq', [
            'type' => 0,
            'menus' => Menu::orderBy('sort', 'ASC')->get(),
            'title' => 'title_'.Config::get('app.locale'),
            'text' => 'text_'.Config::get('app.locale'),
            'faqs' => Faq::all(),
            'current_balance' => $current_balance,
            'month_quota'     => $month_quota,
            'questions_count'   => $questions_count,
            'addToCartProducts'   => $addToCartProducts,
        ]);
    }

    public function agreement(){
        if(Auth::check()){
            $user_id = Auth::user()->id;
            $tools = new Tools();
            $current_balance = Auth::user()->balance;
            $month_quota     = $tools->totalUsdMonthQuota();
            $questions_count = Question::where('user_id', $user_id)->whereNull('status')->count();
            $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        }else{
            $current_balance = 0;
            $month_quota = 0;
            $questions_count = 0;
            $addToCartProducts = 0;
        }

        return view('user.agreement', [
            'type'              => 0,
            'menus'             => Menu::orderBy('sort', 'ASC')->get(),
            'title'             => 'title_'.Config::get('app.locale'),
            'text'              => 'text_'.Config::get('app.locale'),
            'current_balance'   => $current_balance,
            'month_quota'       => $month_quota,
            'questions_count'   => $questions_count,
            'addToCartProducts' => $addToCartProducts,
            'agreement'         => DB::table('agreement')->find(1),
        ]);
    }

    public function paymes(){
        if(Auth::check()){
            $user_id = Auth::user()->id;
            $tools = new Tools();
            $current_balance = Auth::user()->balance;
            $month_quota     = $tools->totalUsdMonthQuota();
            $questions_count = Question::where('user_id', $user_id)->whereNull('status')->count();
            $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        }else{
            $current_balance = 0;
            $month_quota = 0;
            $questions_count = 0;
            $addToCartProducts = 0;
        }


        return view('user.paymes', [
            'type'              => 0,
            'menus'             => Menu::orderBy('sort', 'ASC')->get(),
            'title'             => 'title_'.Config::get('app.locale'),
            'text'              => 'text_'.Config::get('app.locale'),
            'current_balance'   => $current_balance,
            'month_quota'       => $month_quota,
            'questions_count'   => $questions_count,
            'addToCartProducts' => $addToCartProducts,
        ]);
    }
}
