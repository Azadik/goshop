<?php

namespace App\Http\Controllers;

use App\Models\CargoPrices;
use App\Models\Product;
use App\Models\TmpProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Tools;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin\Menu;
use Illuminate\Support\Facades\Config;
use App\Models\Question;
use Intervention\Image\Facades\Image;
use App\User;



class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function tools(){
        return new Tools();
    }

    public function index()
    {
        $user_id = Auth::user()->id;
        $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();

        $product_c = DB::table('products')
            ->select('baglama_id', DB::raw('count(*) as total'))
//            ->orderBy(DB::raw('count(baglama_id)'), 'DESC')
            ->where('user_id', $user_id)
            ->where('status', 3)
            ->whereNotNull('baglama_id')
            ->groupBy('baglama_id')
            ->get();

        $product_in_baku = DB::table('products')->select('baglama_id', DB::raw('count(*) as total'))->where('user_id', $user_id)->where('status', 6)->whereNotNull('baglama_id')->groupBy('baglama_id')->get();
        $product_tehvil = DB::table('products')->select('baglama_id', DB::raw('count(*) as total'))->where('user_id', $user_id)->where('status', 7)->whereNotNull('baglama_id')->groupBy('baglama_id')->get();

        $count_sifarish = count($product_c);
        $count_in_baku = count($product_in_baku);
        $count_tehvil = count($product_tehvil);

//        sifarsih verilenler
        $product = [];
        $counter = [];
        $baglamalar = [];
        $boutique_name = [];
        foreach ($product_c  as $key=>$p_c){
            if(!$p_c->baglama_id) { continue; }
            $product[$key] = DB::table('products')->where('baglama_id', $p_c->baglama_id)->where('status', 3)->where('user_id', $user_id)->orderBy('id', 'DESC')->get();
            $counter[$key] = $p_c->total;
            $baglama = DB::table('products')
                ->where('baglama_id', $p_c->baglama_id)->where('status', 3)->where('user_id', $user_id)
                ->first();
            $baglamalar[$key]= DB::table('baglama')
            ->where('id', $baglama->baglama_id)
            ->first();

            $boutique_name[$key] = DB::table('boutique')->where('id', $baglamalar[$key]->boutique_id)->first();
        }
//        end


//        products in baku
        $product_b = [];
        $counter_b = [];
        $baglamalar_b = [];
        $boutique_name_b = [];
        foreach ($product_in_baku  as $key=>$p_c){
            $product_b[$key] = DB::table('products')->where('baglama_id', $p_c->baglama_id)->where('status', 6)->where('user_id', $user_id)->orderBy('id', 'DESC')->get();
            $counter_b[$key] = $p_c->total;
            $baglama_b = DB::table('products')
                ->where('baglama_id', $p_c->baglama_id)->where('status', 6)->where('user_id', $user_id)
                ->first();
            $baglamalar_b[$key]= DB::table('baglama')->where('id', $baglama_b->baglama_id)->first();
            $boutique_name_b[$key] = DB::table('boutique')->where('id', $baglamalar_b[$key]->boutique_id)->first();

        }
//        end


//        products tehvil
        $product_t = [];
        $counter_t = [];
        $baglamalar_t = [];
        $boutique_name_t = [];
        foreach ($product_tehvil  as $key=>$p_c){
            $product_t[$key] = DB::table('products')->where('baglama_id', $p_c->baglama_id)->where('status', 7)->where('user_id', $user_id)->orderBy('id', 'DESC')->get();
            $counter_t[$key] = $p_c->total;
            $baglama_t = DB::table('products')
                ->where('baglama_id', $p_c->baglama_id)->where('status', 7)->where('user_id', $user_id)
                ->first();
            $baglamalar_t[$key]= DB::table('baglama')->where('id', $baglama_t->baglama_id)->first();
            $boutique_name_t[$key] = DB::table('boutique')->where('id', $baglamalar_t[$key]->boutique_id)->first();
        }
//        end


        $tools = new Tools();

        $total_basket_price = DB::select('SELECT SUM(price_az * count) as price_az, SUM(price * count) as price_tl from tm_products  WHERE   status IN  (0, 1) AND user_id = '.$user_id );

        return view('user.order',[
            'menus' => Menu::orderBy('sort', 'ASC')->get(),
            'title' => 'title_'.Config::get('app.locale'),
            'type' => 2,
            'current_balance'   => Auth::user()->balance,
//            'orders'            => DB::table('tm_products')->where('user_id', $user_id)->whereIn('status', [0, 1])->orderBy('id', 'DESC')->get(),
            'tm_products'       => DB::table('products')->where('user_id', $user_id)->where('status', 3)->whereNull('baglama_id')->orderBy('id', 'DESC')->get(),
            'questions_count'   => Question::where('user_id', $user_id)->whereNull('status')->count(),
//            'total_basket_price'=>$total_basket_price[0],

            'products'          => $product,
            'counter'           => $counter,
            'baglamalar'        => $baglamalar,
            'boutique_name'     => $boutique_name,

            'products_b'        => $product_b,
            'counter_b'         => $counter_b,
            'baglamalar_b'      => $baglamalar_b,
            'boutique_name_b'   => $boutique_name_b,

            'products_t'        => $product_t,
            'counter_t'         => $counter_t,
            'baglamalar_t'      => $baglamalar_t,
            'boutique_name_t'   => $boutique_name_t,

            'count_sifarish'    => $count_sifarish,
            'count_in_baku'     => $count_in_baku,
            'count_tehvil'     => $count_tehvil,
            'month_quota'      => $tools->totalUsdMonthQuota(),
            'addToCartProducts' => $addToCartProducts,
        ]);
    }

    public function addOrder(){
        $tools = new Tools();
        $user_id = Auth::user()->id;
        $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        $course_valute = DB::table('course_valute')->find(1);


        $cargo_prices_table = CargoPrices::all();

        $cargo_data = [];
        foreach ($cargo_prices_table as $cargo_price){
            $cargo_data["$cargo_price->site"] = array('price' => $cargo_price->price, 'limit' => $cargo_price->limit);
        }

        return view('user.addorder',[
            'menus' => Menu::orderBy('sort', 'ASC')->get(),
            'title' => 'title_'.Config::get('app.locale'),
            'type' => 1,
            'course_valute'    => $course_valute,
            'questions_count'   => Question::where('user_id', $user_id)->whereNull('status')->count(),
            'current_balance'   => Auth::user()->balance,
            'month_quota'       => $tools->totalUsdMonthQuota(),
            'addToCartProducts' => $addToCartProducts,
            'cargo_prices_table' => json_encode($cargo_data),
        ]);
    }


    public function addToCart(Request $request){
        $tools = new Tools();
        $user_id = Auth::user()->id;
        $country = $request->country;
        $course_valute = DB::table('course_valute')->find(1);
        $rush_order = $course_valute->usa * 1;

        $urls = $request->url;
        $prices = $request->price;
        $notes = $request->note;

        $hours = $request->hour;
        $minutes = $request->minute;
        $days = $request->day;
        $counts = $request->count;
        $own_price = $request->own_price;


//        tecili sifarish
        if($request->rush){
            $status = 1;

//            $u_balance = DB::table('users')->find($user_id); // burda iki defe yazmagin sebebi uzun regemi okrugleniya etmek olasidir. meselen 2.999949494
//            $u_balance = $u_balance->balance - $rush_order;
//            $u_balance = round($u_balance, 2);
//
//            DB::table('users')->where('id', $user_id)->update([
//                'balance' => $u_balance  // prichina okrugleniya chisla, mojno bilo pisat tak 'balance' => DB::raw('balance - '.$rush_order)
//            ]);

//            $user_balance = DB::table('users')->find($user_id);
//            $user_balance = $user_balance->balance;
//
//            DB::table('balance')->insert([
//                'date'              => date('d-m-Y'),
//                'time'              => date('H:i:s'),
//                'user_id'           => $user_id,
//                'current_balance'   => $user_balance,
//                'balance'           => '-'.$rush_order
//            ]);
        }else{
            $status = 0;
        }
//        end

        foreach ($urls as $key => $url){

            if($hours[$key] == 0 and $minutes[$key] == 0 and $days[$key] == 0){
                $time = null;
            }elseif ($hours[$key] != 0 and $minutes[$key] == 0 and $days[$key] == 0){
                $time = strtotime(date('d-m-Y H:i:s')) + $tools->convertHourToSeconds($hours[$key]);
            }elseif ($hours[$key] == 0 and $minutes[$key] != 0 and $days[$key] == 0){
                $time = strtotime(date('d-m-Y H:i:s')) + $tools->convertMinutesToSeconds($minutes[$key]);
            }elseif ($hours[$key] == 0 and $minutes[$key] == 0 and $days[$key] != 0){
                $time = strtotime(date('d-m-Y H:i:s')) + $tools->convertDayToSeconds($days[$key]);
            }elseif ($hours[$key] != 0 and $minutes[$key] != 0 and $days[$key] != 0){
                $time = strtotime(date('d-m-Y H:i:s')) + $tools->convertDayToSeconds($days[$key]) + $tools->convertHourToSeconds($hours[$key]) + $tools->convertMinutesToSeconds($minutes[$key]);
            }elseif ($hours[$key] != 0 and $minutes[$key] != 0 and $days[$key] == 0){
                $time = strtotime(date('d-m-Y H:i:s')) + $tools->convertHourToSeconds($hours[$key]) + $tools->convertMinutesToSeconds($minutes[$key]);
            }elseif ($hours[$key] != 0 and $minutes[$key] == 0 and $days[$key] != 0){
                $time = strtotime(date('d-m-Y H:i:s')) + $tools->convertDayToSeconds($days[$key]) + $tools->convertHourToSeconds($hours[$key]);
            }elseif ($hours[$key] == 0 and $minutes[$key] != 0 and $days[$key] != 0){
                $time = strtotime(date('d-m-Y H:i:s')) + $tools->convertDayToSeconds($days[$key]) + $tools->convertMinutesToSeconds($minutes[$key]);
            }


            if($country == 1){
                $price_tl = $prices[$key];
                $price_az = round($course_valute->tl * $price_tl, 2);

                $price_with_count = ($course_valute->tl * $price_tl) * $counts[$key]; // sena s kolichstvom produkta
                $all_usd = round($price_with_count / $course_valute->usa, 2);

                $price_usd = null;
            } else {
                $price_usd = $prices[$key];
                $price_az = round($course_valute->usa * $price_usd, 2);

                $price_with_count = $price_usd * $counts[$key]; // sena s kolichstvom produkta
                $all_usd = $price_with_count;

                $price_tl = null;
            }

            $title = null;
            $filename = null;

            DB::table('tm_products')->insert([
                'title'         => $title,
                'img'           => $filename,
                'url'           => $urls[$key],
                'price'         => $price_tl,
                'price_az'      => $price_az,
                'price_usa'     => $price_usd,
                'note'          => $notes[$key],
                'status'        => $status,
                'order_date'    => date('Y-m-d'),
                'order_time'    => date("H:i:s"),
                'boutique_time' => $time,
                'country'       => $country,
                'user_id'       => $user_id,
                'count'         => $counts[$key],
                'all_usd'       => $all_usd,
                'own_price'     => $own_price[$key],
            ]);
        }

        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Linklər uğurla göndərildi !</div>');
    }


   public function editOrder(Request $request){
        $order_id = $request->order_id;
        $order = TmpProduct::find($order_id);
        if($order->country == 1){
            $valyuta = "TL";
            $price = $order->own_price;
            $total_price = $order->count * $price;
        }else{
            $valyuta = "USD";
            $price = $order->own_price;
            $total_price = $order->count * $price;
        }


//         Lang::;
        $language = new Lang();

       echo '
       <div class="row basket-modal">
          <form action="'.route('updateOrder').'" method="post">
          <div class="col-md-4 col-sm-4">';

       if($order->img){
           echo '<img src="'.asset('products/').'/'.$order->img.'" class="img-thumbnail" alt="Cinque Terre">';
       }else{
           echo '<img src="'.asset('products/no_img.png').'" class="img-thumbnail" alt="Cinque Terre">';
       }

          echo '</div>

          <div class="col-md-8 col-sm-8">
              <div class="blue-header"><a href="'.$order->url.'" target="_blank"> '.$order->title.'</a></div>
                <br>
                
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                          <label for="price">'.$language::get('interface.price').' '.$valyuta.':</label>
                          <input name="price" type="text" class="form-control" id="price" value="'.$price.'" required onkeyup=\'validateNum(event)\'>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                          <label for="total_price">'.$language::get('interface.total_price').' '.$valyuta.':</label>
                          <input  type="text" class="form-control" id="total_price" value="'.$total_price.'" required readonly>
                        </div>
                    </div>
                </div>
                
              <div class="form-group">
                  <label for="count">'.$language::get('interface.count').':</label>
                  <input name="count" type="number" class="form-control" id="count" value="'.$order->count.'" required>
              </div>

              <div class="form-group">
                  <label for="url">'.$language::get('interface.url').':</label>
                  <input name="url" type="text" class="form-control" id="url" value="'.$order->url.'" required>
              </div>
          </div>
          <div class="col-md-12">
              <div class="form-group">
                  <label for="comment">'.$language::get('interface.comment').':</label>
                  <textarea name="note" class="form-control" rows="5" id="comment" required>'.$order->note.'</textarea>
              </div>
          '.csrf_field().'                                
            <input type="hidden" name="id" value="'.$order->id.'">
              <div class="form-group text-center">
                  <button class="btn btn-success">'.$language::get('interface.edit').'</button>
              </div>
          </div>
          </form>
       </div>';
   }



   public function deleteOrder(Request $request){
       if (Auth::check()) {
           $product_id = $request->order_id;
           $user_id = Auth::user()->id;
           $tmp_product = DB::table('tm_products')->where('user_id', $user_id)->where('id', $product_id)->first();
           if($tmp_product){
               if($tmp_product->img){
                   unlink(public_path().'/products/'.$tmp_product->img);
                   DB::table('tm_products')->where('user_id', $user_id)->where('id', $product_id)->delete();
               }else{
                   DB::table('tm_products')->where('user_id', $user_id)->where('id', $product_id)->delete();
               }

//             $tools = new Tools();
//             $tools->recycleBinBasket($product_id);

               return array("status" => 1);
           }else{
               return json_encode(array("status" => 2));
           }
       }else{
           $tools = new Tools();
           $tools->blacklist();
       }
   }



   public function updateOrder(Request $request){
       $user_id = Auth::user()->id;

       $course_valute = DB::table('course_valute')->find(1);
       $tl = $course_valute->tl;
       $usd = $course_valute->usa;

       $price = floatval($request->price);

       $order = DB::table('tm_products')->where('user_id', $user_id)->where('id', $request->id)->first();

       if($order){

           if($order->country == 1){
               $price_tl            = $price * 0.05 + $price;
               $price_with_count    = $price_tl * $request->count;
               $price_azn           = $price_tl * $tl;
               $all_usd             = ($price_with_count * $tl) / $usd;
               $price_usd           = null;
           } else {
               $price_usd           = $price * 0.05 + $price;
               $price_with_count    = $price_usd * $request->count;
               $price_azn           = $price_usd * $usd;
               $all_usd             = $price_with_count;
               $price_tl            = null;
           }


           //            title and img parser
           $data = $this->tools()->siteParser($request->url);
           $img_url = $data['img'];
           $title = $data['title'];

           if($img_url){
               $file = file_get_contents($img_url);
               $filename = $user_id.'_'.time().'.png';
               file_put_contents(public_path().'/products/'.$filename, $file);
               //        Image::make(public_path().'/images/'.$filename)->insert(public_path().'/img_site/watermark.png', 'top-right', 5, 5)->resize(300, 169)->save(public_path().'/images/'.$filename);
               //        Image::make(public_path().'/products/'.$filename)->resize(300, 169)->save(public_path().'/products/'.$filename, 80);
               $thumbnailpath = public_path().'/products/'.$filename;
               $img = Image::make($thumbnailpath)->resize(350, 200, function($constraint) {
                   $constraint->aspectRatio();
               });
               $img->save($thumbnailpath);

//               esli fayl est to udalit stariy fayl
               if($order->img){
                   if (file_exists(public_path().'/products/'.$order->img)) {
                       unlink(public_path().'/products/'.$order->img);
                   }
               }
           }else{
               $filename = null;
           }
//            end parser

           DB::table('tm_products')->where('id', $request->id)->update([
               'title'      => $title,
               'img'        => $filename,
               'price'      => $price_tl,
               'own_price'  => $price,
               'price_az'   => $price_azn,
               'all_usd'    => $all_usd,
               'price_usa'  => $price_usd,

               'count'      => $request->count,
               'url'        => $request->url,
               'note'       => $request->note,
           ]);
           return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Success updated!</div>');
       }else{
           $tools = new Tools();
           $tools->blacklist();

           return Redirect::back()->with('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.Lang::get('interface.access_denied').'</div>');
       }
   }

   public function pay($id){
       $course_valute = DB::table('course_valute')->find(1);
       $tl = $course_valute->tl;
       $usd = $course_valute->usa;

       $user_id = Auth::user()->id;
       $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
       $tools = new Tools();
       $order = TmpProduct::where('user_id', $user_id)->where('id', $id)->first();
       $total_prices = DB::select('SELECT  SUM(price * count ) as total_tl, SUM(price_az * count ) as total_az
                                                FROM tm_products WHERE id = ('.$id.') AND user_id = '.$user_id.'
                                                ');

//       cargo prices
       $haverushproduct = 0;
       $sites = [];
       if($order->status == 1){
           $haverushproduct = 1;
       }

       $price = DB::select('SELECT SUM(own_price * `count`) as total_price, country from tm_products WHERE url LIKE "%'.$tools->getSiteName($order->url).'%" AND id = '.$id);
       $sites[$tools->getSiteName($order->url)] = array('country'=> $price[0]->country, 'price' => $price[0]->total_price);

       $cargo_info = [];
       $cargo_prices = 0;
       foreach ($sites as $key=>$value){
           if($value['country'] == 1){
               $a = CargoPrices::where('site', $key)->first();
               if($a){
                   if($value['price'] >= $a->limit ){
                       if($a->limit == 0){ //giymetinde asli olamayarag kargo pulu olan saytlar uchun
                           $cargo_info[$key] = array( 'price' => $a->price, 't_c_price' => $value['price']);
                           $cargo_prices = $cargo_prices + $a->price;
                       }else{
                           $cargo_info[$key] = array( 'price' => 0, 't_c_price' => $value['price']);
                           $cargo_prices = $cargo_prices + 0;
                       }
                   }else{
                       $cargo_info[$key] = array( 'price' => $a->price, 't_c_price' => $value['price']);
                       $cargo_prices = $cargo_prices + $a->price;
                   }
               }else{
                   $cargo_info[$key] = array(  'price' => 7.99, 't_c_price' => $value['price']);
                   $cargo_prices = $cargo_prices + 7.99;
               }
           }else{
               $cargo_prices = $cargo_prices + 0;
           }
       }
       $cargo_prices = $cargo_prices + $cargo_prices * 0.05;
//       $cargo_prices = $tl * $cargo_prices; //cargonun umumi meblegin gunun valyutasina gore mezennesi

       if($haverushproduct){
           $convert_usd_to_tl =  $usd / $tl;
           $cargo_prices = $cargo_prices + $convert_usd_to_tl;
       }
//       end



       return view('user.pay',[
           'menus'              => Menu::orderBy('sort', 'ASC')->get(),
           'title'              => 'title_'.Config::get('app.locale'),
           'type'               => 3,
           'current_balance'    => Auth::user()->balance,
           'month_quota'        => $tools->totalUsdMonthQuota(),
           'questions_count'    => Question::where('user_id', $user_id)->whereNull('status')->count(),
           'addToCartProducts'  => $addToCartProducts,

           'order'              => $order,
           'total_prices'       => $total_prices[0],

           'cargo_info'         => $cargo_info,
           'cargo_prices'       => round($cargo_prices, 2),
           'haverushproduct'    => $haverushproduct,
       ]);
   }


       public function allPay(Request $request){

        if(!$request->all()){
            return Redirect::back()->with('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Ödəniş etmək istədiyiniz məhsulu seçin</div>');
        }

       $course_valute = DB::table('course_valute')->find(1);
       $tl = $course_valute->tl;
       $usd = $course_valute->usa;

       $user_id = Auth::user()->id;
       $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();

       $tools = new Tools();
       $orders = DB::table('tm_products')->whereIn('id', $request->orders)->where('user_id', $user_id)->get();

       $total_prices = DB::select('SELECT SUM(price * count ) as total_tl, SUM(price_az * count ) as total_az
                                                FROM tm_products WHERE id IN ('.implode(',', $request->orders).')
                                                ');

//       cargo prices
       $haverushproduct = 0;
       $sites = [];
       foreach ($orders as $order){
           if($order->status == 1){
               $haverushproduct = 1;
           }
           if(!in_array($tools->getSiteName($order->url), $sites)){
               $price = DB::select('SELECT SUM(own_price * `count`) as total_price, country from tm_products WHERE url LIKE "%'.$tools->getSiteName($order->url).'%" AND id IN ('.implode(',', $request->orders).') group by country');
               $sites[$tools->getSiteName($order->url)] = array('country'=> $price[0]->country, 'price' => $price[0]->total_price);
           }
       }

       $cargo_info = [];
       $cargo_prices = 0;
       foreach ($sites as $key=>$value){
           if($value['country'] == 1){
               $a = CargoPrices::where('site', $key)->first();
               if($a){
                   if($value['price'] >= $a->limit ){
                       if($a->limit == 0){ //giymetinde asli olamayarag kargo pulu olan saytlar uchun
                           $cargo_info[$key] = array( 'price' => $a->price, 't_c_price' => $value['price']);
                           $cargo_prices = $cargo_prices + $a->price;
                       }else{
                           $cargo_info[$key] = array( 'price' => 0, 't_c_price' => $value['price']);
                           $cargo_prices = $cargo_prices + 0;
                       }
                   }else{
                       $cargo_info[$key] = array( 'price' => $a->price, 't_c_price' => $value['price']);
                       $cargo_prices = $cargo_prices + $a->price;
                   }
               }else{
                   $cargo_info[$key] = array(  'price' => 7.99, 't_c_price' => $value['price']);
                   $cargo_prices = $cargo_prices + 7.99;
               }
           }else{
               $cargo_prices = $cargo_prices + 0;
           }
       }
           $cargo_prices = $cargo_prices + $cargo_prices * 0.05;
//           $cargo_prices = $tl * $cargo_prices; //cargonun umumi meblegin gunun valyutasina gore mezennesi



           if($haverushproduct){
               $convert_usd_to_tl =  $usd / $tl;
               $cargo_prices = $cargo_prices + $convert_usd_to_tl;
           }
//       end

//           dd($total_prices[0]->total_az);

       return view('user.allpay',[
           'menus'              => Menu::orderBy('sort', 'ASC')->get(),
           'title'              => 'title_'.Config::get('app.locale'),
           'type'               => 3,
           'current_balance'    => Auth::user()->balance,
           'month_quota'        => $tools->totalUsdMonthQuota(),
           'questions_count'    => Question::where('user_id', $user_id)->whereNull('status')->count(),
           'addToCartProducts'  => $addToCartProducts,
           'orders'             => $orders,
           'total_prices'       => $total_prices[0],

           'cargo_info'         => $cargo_info,
           'cargo_prices'       => round($cargo_prices, 2),
           'haverushproduct'    => $haverushproduct,
       ]);
   }

//show Traking
    public function showTracking(Request $request){
        $baglama = DB::table('baglama')->find($request->order_id);
        $language = new Lang();


        echo '
        <div class="row bs-wizard" style="border-bottom:0;">
                              <div class="col-xs-2 col-xs-offset-1 bs-wizard-step complete">
                                  <div class="text-center bs-wizard-stepnum">'.$language::get('tracking.was_ordered').'</div>
                                  <div class="progress"><div class="progress-bar"></div></div>
                                  <a href="#" class="bs-wizard-dot"></a>
                                  <div class="bs-wizard-info text-center">'.$baglama->date.'</div>
                              </div>

                              <div class="col-xs-2 bs-wizard-step '; if($baglama->outside_date){ echo 'complete'; }else{ echo 'disabled'; }  echo '"><!-- complete -->
                                  <div class="text-center bs-wizard-stepnum">'.$language::get('tracking.overseas_terminal').'</div>
                                  <div class="progress"><div class="progress-bar"></div></div>
                                  <a href="#" class="bs-wizard-dot"></a>
                                  <div class="bs-wizard-info text-center">'.$baglama->outside_date.'</div>
                              </div>

                              <div class="col-xs-2 bs-wizard-step '; if($baglama->send_baku_date){ echo 'complete'; }else{ echo 'disabled'; }  echo '"><!-- complete -->
                                  <div class="text-center bs-wizard-stepnum">'.$language::get('tracking.sent_to_baku').'</div>
                                  <div class="progress"><div class="progress-bar"></div></div>
                                  <a href="#" class="bs-wizard-dot"></a>
                                  <div class="bs-wizard-info text-center">'.$baglama->send_baku_date.'</div>
                              </div>

                              <div class="col-xs-2 bs-wizard-step '; if($baglama->in_baku_date){ echo 'complete'; }else{ echo 'disabled'; }  echo '"><!-- active -->
                                  <div class="text-center bs-wizard-stepnum">'.$language::get('tracking.in_baku_office').'</div>
                                  <div class="progress"><div class="progress-bar"></div></div>
                                  <a href="#" class="bs-wizard-dot"></a>
                                  <div class="bs-wizard-info text-center">'.$baglama->in_baku_date.'</div>
                              </div>

                              <div class="col-xs-2 bs-wizard-step '; if($baglama->handed_over_date){ echo 'complete'; }else{ echo 'disabled'; }  echo '"><!-- active -->
                                  <div class="text-center bs-wizard-stepnum">'.$language::get('tracking.was_handed_over').'</div>
                                  <div class="progress"><div class="progress-bar"></div></div>
                                  <a href="#" class="bs-wizard-dot"></a>
                                  <div class="bs-wizard-info text-center">'.$baglama->handed_over_date.'</div>
                              </div>
                          </div>
        ';
    }

    public function abroadAddress(){
        $user_id = Auth::user()->id;
        $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        $tools = new Tools();


        return view('user.abroadaddress',[
            'menus' => Menu::orderBy('sort', 'ASC')->get(),
            'title' => 'title_'.Config::get('app.locale'),
            'type' => 3,
            'questions_count'   => Question::where('user_id', $user_id)->whereNull('status')->count(),
            'month_quota'       => $tools->totalUsdMonthQuota(),
            'addToCartProducts' => $addToCartProducts,
            'current_balance'   => Auth::user()->balance,
        ]);
    }


    public function profile(){
        $tools = new Tools();
        $user_id = Auth::user()->id;
        $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();

        return view('user.profile',[
            'menus' => Menu::orderBy('sort', 'ASC')->get(),
            'title' => 'title_'.Config::get('app.locale'),
            'type' => 4,
            'current_balance'   => Auth::user()->balance,
            'questions_count'   => Question::where('user_id', $user_id)->whereNull('status')->count(),
            'month_quota'      => $tools->totalUsdMonthQuota(),
            'addToCartProducts' => $addToCartProducts,
        ]);
    }


    public function updateProfile(Request $request){
            Validator::make($request->all(), [
                'name'      => 'required|min:2|max:255',
                'last_name' => 'required|min:2|max:255',
                'contact'   => 'required|min:9|max:255',
                'passport'  => 'required|min:8|max:255',
                'fin_code'  => 'required|min:7|max:255',
                'address'   => 'required|min:8|max:255',
            ])->validate();


            DB::table('users')->where('id', Auth::user()->id)->update([
                'name'      => $request->name,
                'last_name' => $request->last_name,
                'contact'   => $request->contact,
                'passport'  => $request->passport,
                'fin_code'  => $request->fin_code,
                'address'   => $request->address,
            ]);

            return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Uğurla yeniləndi</div>');

    }

    public function changePassword(Request $request){
        $now_password = $request->old_pass;
        if(Hash::check($now_password, Auth::user()->password)){
            Validator::make($request->all(), [
                'password'      => 'required|min:8|confirmed',
            ])->validate();

            DB::table('users')->where('id', Auth::user()->id)->update([
                'password'  => bcrypt($request->password),
            ]);

            return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Uğurla yeniləndi</div>');

        }else{
            return Redirect::back()->with('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Köhnə şifrə düzgün deyil</div>');
        }
    }

//    profile photo upload
    public function profileImgUpload(Request $request){
        $user = Auth::user();

        Validator::make($request->all(), [
            'img' => 'required|mimes:jpeg,jpg,png|max:1000',
        ])->validate();

        if($request->img) {
            $destinationPath = public_path() . '/profile/';

            if(is_file($destinationPath.$user->img)){
                unlink($destinationPath.$user->img);
            }

            $file = $request->file('img');

            //get filename with extension
            $filenamewithextension = $request->file('img')->getClientOriginalName();

            $extension = $request->file('img')->getClientOriginalExtension();

            $filenametostore = $user->id.'_'.time().'.'.$extension;

            $file->move($destinationPath, $filenametostore);


            //Resize image here
            $thumbnailpath = $destinationPath.$filenametostore;
            $img = Image::make($thumbnailpath)->resize(200, 150, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);

//            Image::make($thumbnailpath)->resizeCanvas(400, 400)->save($thumbnailpath);

            DB::table('users')->where('id', $user->id)->update([
                'img' => $filenametostore
            ]);

        }
        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Şəkil uğurla əlavə edildi!</div>');
    }


    public function rules(){
        if(Auth::check()){
            $user_id = Auth::user()->id;
            $tools = new Tools();
            $current_balance = Auth::user()->balance;
            $month_quota     = $tools->totalUsdMonthQuota();
            $questions_count = Question::where('user_id', $user_id)->whereNull('status')->count();
            $addToCartProducts = DB::table('tm_products')->where('user_id', $user_id)->count();
        }else{
            $current_balance = 0;
            $month_quota = 0;
            $questions_count = 0;
            $addToCartProducts = 0;
        }

        return view('user.rules', [
            'type'              => 0,
            'menus'             => Menu::orderBy('sort', 'ASC')->get(),
            'title'             => 'title_'.Config::get('app.locale'),
            'text'              => 'text_'.Config::get('app.locale'),
            'current_balance'   => $current_balance,
            'month_quota'       => $month_quota,
            'questions_count'   => $questions_count,
            'addToCartProducts' => $addToCartProducts,
            'rules'             => DB::table('rules')->find(1),
        ]);
    }


    public function getDataFromUrl(Request $request){
        $tools = new Tools();
        $data = $tools->getPriceSize($request->site);
//        $data = $tools->getPriceSize('https://www.trendyol.com/koton-kids/pembe-kiz-cocuk-atlet-p-3977975?boutiqueId=311556&merchantId=637');
        return $data;
    }


    //korzina
    public function cart()
    {
        $user = Auth::user();
        $total_basket_price = DB::select('SELECT SUM(price_az * count) as price_az, SUM(price * count) as price_tl from tm_products  WHERE   status IN  (0, 1) AND user_id = '.$user->id );
        $addToCartProducts = DB::table('tm_products')->where('user_id', $user->id)->count();


        //       cargo prices
        $course_valute = DB::table('course_valute')->find(1);
        $tl = $course_valute->tl;
        $usd = $course_valute->usa;

        $tools = new Tools();
        $orders = DB::table('tm_products')->where('user_id', $user->id)->whereIn('status', [0, 1])->orderBy('id', 'DESC')->get();

        $haverushproduct = 0;
        $sites = [];
        foreach ($orders as $order){
            if($order->status == 1){
                $haverushproduct = 1;
            }
            if(!in_array($tools->getSiteName($order->url), $sites)){
                $price = DB::select('SELECT SUM(own_price * `count`) as total_price, country from tm_products WHERE url LIKE "%'.$tools->getSiteName($order->url).'%" AND user_id IN  ('.$user->id.') AND status IN (0, 1) group by country');
                $sites[$tools->getSiteName($order->url)] = array('country'=> $price[0]->country, 'price' => $price[0]->total_price);
            }
        }

        $cargo_info = [];
        $cargo_prices = 0;
        foreach ($sites as $key=>$value){
            if($value['country'] == 1){
                $a = CargoPrices::where('site', $key)->first();
                if($a){
                    if($value['price'] >= $a->limit ){
                        if($a->limit == 0){ //giymetinde asli olamayarag kargo pulu olan saytlar uchun
                            $cargo_info[$key] = array( 'price' => $a->price, 't_c_price' => $value['price']);
                            $cargo_prices = $cargo_prices + $a->price;
                        }else{
                            $cargo_info[$key] = array( 'price' => 0, 't_c_price' => $value['price']);
                            $cargo_prices = $cargo_prices + 0;
                        }
                    }else{
                        $cargo_info[$key] = array( 'price' => $a->price, 't_c_price' => $value['price']);
                        $cargo_prices = $cargo_prices + $a->price;
                    }
                }else{
                    $cargo_info[$key] = array(  'price' => 7.99, 't_c_price' => $value['price']);
                    $cargo_prices = $cargo_prices + 7.99;
                }
            }else{
                $cargo_prices = $cargo_prices + 0;
            }
        }
        $cargo_prices = $cargo_prices + $cargo_prices * 0.05;
//           $cargo_prices = $tl * $cargo_prices; //cargonun umumi meblegin gunun valyutasina gore mezennesi


        if($haverushproduct){
            $convert_usd_to_tl =  $usd / $tl;
            $cargo_prices = $cargo_prices + $convert_usd_to_tl;
        }
//       end


        return view('user.cart',[
            'menus'             => Menu::orderBy('sort', 'ASC')->get(),
            'title'             => 'title_'.Config::get('app.locale'),
            'type'              => 9,
            'current_balance'   => $user->balance,
            'orders'            => $orders,
            'total_basket_price'=>$total_basket_price[0],
            'addToCartProducts' => $addToCartProducts,
            'questions_count'   => Question::where('user_id', $user->id)->whereNull('status')->count(),

            'cargo_info'         => $cargo_info,
            'cargo_prices'       => round($cargo_prices, 2),
            'haverushproduct'    => $haverushproduct,
        ]);
    }
}
