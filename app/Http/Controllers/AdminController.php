<?php

namespace App\Http\Controllers;


use App\Models\Admin\Menu;
use App\Models\Faq;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\User;
use Tools;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function tool(){
        $tools = new Tools();
        return $tools;
    }


    public function index()
    {
        return view('admin.home',[
            'menus' => DB::table('menu')->orderBy('sort', 'ASC')->get()
        ]);
    }

    public function menuSort(Request $request){
        foreach ($request->sort as $key=>$value){
            DB::table('menu')->where('id', $key)->update([
                'sort'=>$value
            ]);
        }
        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully sorted!</div>');
    }

    public function addMenu(Request $request){
        $menu = new Menu();
        $menu->title_az = $request->title_az;
        $menu->title_en = $request->title_en;
        $menu->title_ru = $request->title_ru;
        $menu->url = $request->url;
        $menu->sort = $request->sort;
        $menu->save();
        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully added!</div>');

    }

    public function deleteMenu($id){
        Menu::where('id', $id)->delete();
        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully deleted!</div>');
    }


    public function editMenu($id){
        $menu = DB::table('menu')->find($id);
        return view('admin.edit_menu',[
            'menu' => $menu
        ]);
    }

    public function updateMenu(Request $request){
        DB::table('menu')->where('id', $request->id)->update([
            'title_az'=> $request->title_az,
            'title_en'=> $request->title_en,
            'title_ru'=> $request->title_ru,
            'sort'=> $request->sort,
            'url'=> $request->url,
        ]);

        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully updated!</div>');
    }

//    page
    public function page(){
        return view('admin.page');
    }

    public function addPage(Request $request){
        $url = $this->tool()->translit($request->title_en);
        $url = $this->tool()->qorun($url);

        $page = new Page();
        $page->title_az = $request->title_az;
        $page->title_en = $request->title_en;
        $page->title_ru = $request->title_ru;
        $page->text_az  = $request->text_az;
        $page->text_en  = $request->text_en;
        $page->text_ru  = $request->text_ru;
        $page->url = $url;
        $page->save();
        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully added!</div>');
    }

    public function pageList(){
        return view('admin.page_list', [
            'pages' => Page::all(),
        ]);
    }

    public function editPage($id){
        return view('admin.edit_page', [
            'page' => Page::find($id),
        ]);
    }

    public function deletePage($id){
        Page::where('id', $id)->delete();
        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully deleted!</div>');
    }

    public function updatePage(Request $request){
        $url = $this->tool()->translit($request->title_en);
        $url = $this->tool()->qorun($url);

        DB::table('pages')->where('id', $request->id)->update([
            'title_az'=>$request->title_az,
            'title_en'=>$request->title_en,
            'title_ru'=>$request->title_ru,
            'text_az'=>$request->text_az,
            'text_en'=>$request->text_en,
            'text_ru'=>$request->text_ru,
            'url'=>$url,
        ]);

        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully updated!</div>');
    }


    //    faq
    public function faq(){
        return view('admin.faq');
    }

    public function addFaq(Request $request){
        $url = $this->tool()->translit($request->title_en);
        $url = $this->tool()->qorun($url);

        $page = new Faq();
        $page->title_az = $request->title_az;
        $page->title_en = $request->title_en;
        $page->title_ru = $request->title_ru;
        $page->text_az  = $request->text_az;
        $page->text_en  = $request->text_en;
        $page->text_ru  = $request->text_ru;
        $page->url = $url;
        $page->save();
        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully added!</div>');
    }

    public function faqList(){
        return view('admin.faq_list', [
            'pages' => Faq::all(),
        ]);
    }

    public function editFaq($id){
        return view('admin.edit_faq', [
            'page' => Faq::find($id),
        ]);
    }

    public function deleteFaq($id){
        Faq::where('id', $id)->delete();
        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully deleted!</div>');
    }

    public function updateFaq(Request $request){
        $url = $this->tool()->translit($request->title_en);
        $url = $this->tool()->qorun($url);

        DB::table('faq')->where('id', $request->id)->update([
            'title_az'=>$request->title_az,
            'title_en'=>$request->title_en,
            'title_ru'=>$request->title_ru,
            'text_az'=>$request->text_az,
            'text_en'=>$request->text_en,
            'text_ru'=>$request->text_ru,
            'url'=>$url,
        ]);

        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully updated!</div>');
    }

    public function agreement(){
        return view('admin.agreement', [
            'agreement' => DB::table('agreement')->find(1),
        ]);
    }

    public function UpdateAgreement(Request $request){
        DB::table('agreement')->where('id', 1)->update([
            'title_az' => $request->title_az,
            'title_en' => $request->title_en,
            'title_ru' => $request->title_ru,
            'text_az' => $request->text_az,
            'text_en' => $request->text_en,
            'text_ru' => $request->text_ru,
        ]);
        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully updated!</div>');
    }

    public function rules(){
        return view('admin.rules', [
            'rules' => DB::table('rules')->find(1),
        ]);
    }

    public function UpdateRules(Request $request){
        DB::table('rules')->where('id', 1)->update([
            'title_az' => $request->title_az,
            'title_en' => $request->title_en,
            'title_ru' => $request->title_ru,
            'text_az' => $request->text_az,
            'text_en' => $request->text_en,
            'text_ru' => $request->text_ru,
        ]);
        return Redirect::back()->with('msg', '<div class="alert alert-success alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully updated!</div>');
    }
}
