<?php

namespace App\Http\Controllers;

use App\Mail\OrderMail;
use App\Models\InsuranceProduct;
use App\Models\Order;
use App\Models\PaymEsTransaction;
use App\Models\PostTerminal;
use App\Models\TransactionData;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Models\Balance;
use mysql_xdevapi\Session;

class OrdersController extends Controller
{
// "trans_id" => "OKYOj7VFAuHoL55PoBmpZzJQu2s="
// "Ucaf_Cardholder_Confirm" => "0"

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function approved(Request $request){
        $ca = app_path('certificate/pasha/PSroot.pem');
        $key = app_path('certificate/privkey.pem');
        $cert = app_path('certificate/pasha/certificate.0010877.pem');
        $password = "goshop1";

        $merchant_handler = "https://ecomm.pashabank.az:18443/ecomm2/MerchantHandler";
        $client_handler = "https://ecomm.pashabank.az:8463/ecomm2/ClientHandler";
        $success_page = "success.html";
        $card_expired_page = "card_expired.html";
        $insufficient_funds_page = "insufficient_funds.html";
        $system_malfunction_page = "system_malfunction.html";
        $trans_id = filter_input(INPUT_POST, 'trans_id');


//        if (strlen($trans_id) != 20 ||
//            base64_encode(base64_decode($trans_id)) != $trans_id) {
//            session()->flash('msg', '<div class="alert alert-danger alert-dismissible fade in text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Yanlış məlumat daxil edilib!</div>');
//            return redirect()->route('payment.errors');
//        }

        if (strlen($trans_id) != 20 ||
            base64_encode(base64_decode($trans_id)) != $trans_id) {
        }

        $params['command'] = "C";
        $params['trans_id'] = $trans_id;

        // IP адрес Клиента
        if (filter_input(INPUT_SERVER, 'REMOTE_ADDR') != null) {
            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
        } elseif (filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR') != null) {
            $params['client_ip_addr'] =
                filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR');
        } elseif (filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP') != null) {
            $params['client_ip_addr'] = filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP');
        } else {
            // should never happen
            $params['client_ip_addr'] = "10.10.10.10";
        }
        $qstring = http_build_query($params);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $merchant_handler);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $qstring);
        curl_setopt($ch, CURLOPT_SSLCERT, $cert);
        curl_setopt($ch, CURLOPT_SSLKEY, $key);
        curl_setopt($ch, CURLOPT_SSLKEYTYPE, "PEM");
        curl_setopt($ch, CURLOPT_SSLKEYPASSWD, $password);
        curl_setopt($ch, CURLOPT_CAPATH, $ca);
        curl_setopt($ch, CURLOPT_SSLCERTTYPE, "PEM");
        curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        $result = curl_exec($ch);

        if (curl_error($ch)) {
            header("Location: " . $system_malfunction_page);
        }

        curl_close($ch);
        $r_hm = array();
        $r_arr = array();
        $r_arr = explode("\n", $result);
        for ($i = 0; $i < count($r_arr); $i++) {
            $param = explode(":", $r_arr[$i])[0];
            $value = substr(explode(":", $r_arr[$i])[1], 1);
            $r_hm[$param] = $value;
        }

//        $user_id = Auth::user()->id;

        if ($r_hm["RESULT"] == "OK") {
            if ($r_hm["RESULT_CODE"] == "000") {

                $insproduct = InsuranceProduct::where('trans_id', $trans_id)->first();
                $post_terminal = PostTerminal::where('trans_id', $trans_id)->first(); //check transfer from post terminal

                if($post_terminal){
                    $u_id = Auth::user()->id;
                    $t_balance = round($post_terminal['amount'], 2);
                    DB::select('update users SET balance = IFNULL(balance, 0) + '.$t_balance.' WHERE id = '.$u_id);
                    DB::select('update post_terminal SET `buy` = 1 WHERE trans_id = "'.$trans_id.'"');

                    $user = User::find($u_id);
                    $balance_operation = new Balance();
                    $balance_operation->balance = $t_balance;
                    $balance_operation->current_balance = $user->balance;
                    $balance_operation->date = date('Y-m-d');
                    $balance_operation->time = date('h:i:s');
                    $balance_operation->user_id = $u_id;
                    $balance_operation->save();
                }elseif($insproduct) {
                    DB::table('insurance_transaction')->where('trans_id', $trans_id)->update([
                        'status' => 1
                    ]);
                    DB::table('insurance_transaction')->whereNull('status')->delete();

                    DB::table('insurance_products')->where('trans_id', $trans_id)->update([
                        'status' => 1
                    ]);
                    DB::table('insurance_products')->whereNull('status')->delete();

                    $insuranced_products = DB::table('insurance_products')->where('trans_id', $trans_id)->where('status', 1)->get();

                    foreach($insuranced_products as $i_pr){
                        DB::table('products')->where('id', $i_pr->product_id)->update([
                            'insurance' => 1
                        ]);
                    }
                } else {
                    $order = Order::where('trans_id', $trans_id)->first();

                    DB::table('orders')->where('trans_id', $trans_id)->update([
                        'buy' => 1
                    ]);

                    DB::table('tm_products')->where('temp_order_id', $order['id'])->update([
                        'order_id' => $order['id'],
                        'status' => 3,
                    ]);

                    DB::select('INSERT INTO products ( 
                                                                user_id,
                                                                 url,
                                                                  note,
                                                                   order_date,
                                                                    `count`,
                                                                     country,
                                                                      price,
                                                                       weight,
                                                                        shipping_price,
                                                                         img,
                                                                          status,
                                                                           boutique_id,
                                                                            order_time,
                                                                             boutique_time,
                                                                              price_az,
                                                                               price_usa,
                                                                                title,
                                                                                 order_id,
                                                                                  baglama_id,
                                                                                   all_usd,
                                                                                    own_price, 
                                                                                     img_status,
                                                                                      order_number,
                                                                                      cargo,
                                                                                      cargo_az
                                                                                       )
                                                              
                                                          SELECT 
                                                           user_id,
                                                                 url,
                                                                  note,
                                                                   order_date,
                                                                    `count`,
                                                                     country,
                                                                      price,
                                                                       weight,
                                                                        shipping_price,
                                                                         img,
                                                                          status,
                                                                           boutique_id,
                                                                            order_time,
                                                                             boutique_time,
                                                                              price_az,
                                                                               price_usa,
                                                                                title,
                                                                                 order_id,
                                                                                  baglama_id,
                                                                                   all_usd,
                                                                                    own_price, 
                                                                                     img_status,
                                                                                      order_number,
                                                                                      cargo,
                                                                                      cargo_az
                                                           FROM tm_products  WHERE order_id = ' . $order['id']);
                    //                DB::table('products')->where('order_id', $order['id'])->update([
                    //                    'status' => 3,
                    //                ]);

                    DB::table('tm_products')->where('order_id', $order['id'])->delete();
                    DB::table('orders')->whereNull('buy')->delete();

                    //                          send mail about orders
                    $name = Auth::user()->name;
                    $last_name = Auth::user()->last_name;
                    Mail::to('goshop.az1@gmail.com')->send(new OrderMail($name, $last_name));
                    //                          end
                }

                return redirect()->route('success.payment');
            } else {
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = TransactionData::where('trans_id', $trans_id)->first();
                return redirect()->route('system.malfunction.page', $transaction_data->id);
            }
        } elseif ($r_hm["RESULT"] == "FAILED") {

            Order::where('trans_id', $trans_id)->delete();
            $transaction_data = TransactionData::where('trans_id', $trans_id)->first();

            if ($r_hm["RESULT_CODE"] == "116") {
                return redirect()->route('insufficient.funds.page', $transaction_data->id);
            } elseif ($r_hm["RESULT_CODE"] == "129") {
                return redirect()->route('card.expired.page', $transaction_data->id);
            } elseif ($r_hm["RESULT_CODE"] == "909") {
                return redirect()->route('system.malfunction.page', $transaction_data->id);
            } else {
                return redirect()->route('system.malfunction.page', $transaction_data->id);
            }
        } elseif ($r_hm["RESULT"] == "TIMEOUT") {
            Order::where('trans_id', $trans_id)->delete();
            $transaction_data = TransactionData::where('trans_id', $trans_id)->first();
            return redirect()->route('system.malfunction.page', $transaction_data->id);
        } else {
            Order::where('trans_id', $trans_id)->delete();
            $transaction_data = TransactionData::where('trans_id', $trans_id)->first();
            return redirect()->route('system.malfunction.page', $transaction_data->id);
        }
    }



//    Paym.es
    public function paymEsResult(Request $request){
        $paymes = $request->all();

        $trans_id = $request->payuPaymentReference;


            if ($paymes["message"] == "AUTHORIZED") {
                    $order = Order::where('trans_id', $trans_id)->first();

                    DB::table('orders')->where('trans_id', $trans_id)->update([
                        'buy' => 1
                    ]);

                    DB::table('tm_products')->where('temp_order_id', $order['id'])->update([
                        'order_id' => $order['id'],
                        'status' => 3,
                    ]);


                DB::select('INSERT INTO products ( 
                                                                user_id,
                                                                 url,
                                                                  note,
                                                                   order_date,
                                                                    `count`,
                                                                     country,
                                                                      price,
                                                                       weight,
                                                                        shipping_price,
                                                                         img,
                                                                          status,
                                                                           boutique_id,
                                                                            order_time,
                                                                             boutique_time,
                                                                              price_az,
                                                                               price_usa,
                                                                                title,
                                                                                 order_id,
                                                                                  baglama_id,
                                                                                   all_usd,
                                                                                    own_price, 
                                                                                     img_status,
                                                                                      order_number,
                                                                                      cargo,
                                                                                      cargo_az
                                                                                       )
                                                              
                                                          SELECT 
                                                           user_id,
                                                                 url,
                                                                  note,
                                                                   order_date,
                                                                    `count`,
                                                                     country,
                                                                      price,
                                                                       weight,
                                                                        shipping_price,
                                                                         img,
                                                                          status,
                                                                           boutique_id,
                                                                            order_time,
                                                                             boutique_time,
                                                                              price_az,
                                                                               price_usa,
                                                                                title,
                                                                                 order_id,
                                                                                  baglama_id,
                                                                                   all_usd,
                                                                                    own_price, 
                                                                                     img_status,
                                                                                      order_number,
                                                                                      cargo,
                                                                                      cargo_az
                                                           FROM tm_products  WHERE order_id = ' . $order['id']);
                    //                DB::table('products')->where('order_id', $order['id'])->update([
                    //                    'status' => 3,
                    //                ]);

                    DB::table('tm_products')->where('order_id', $order['id'])->delete();
                    DB::table('orders')->whereNull('buy')->delete();
                    $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();


                //send mail about orders
                    $name = Auth::user()->name;
                    $last_name = Auth::user()->last_name;
                    Mail::to('goshop.az1@gmail.com')->send(new OrderMail($name, $last_name, $transaction_data->amount));
                    //end

                return redirect()->route('success.payment');
            } elseif ($paymes["message"] == "GWERROR_14"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'İşleme kapalı veya geçersiz kart. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }

            elseif ($paymes["message"] == "GWERROR_57"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Kart sahibine açık olmayan işlem, kart hamili bankası ile görüşerek kart yetkilerini kontrol ettirmelidir. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_-2"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Banka tarafında yaşanılan anlık sorun veya anlık yoğunluktan dolayı işlem hata almıştır, bir süre sonra tekrar deneyiniz. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_-3"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Banka tarafında yaşanılan anlık sorun veya anlık yoğunluktan dolayı işlem hata almıştır, bir süre sonra tekrar deneyiniz. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_05"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Genel Red, kart hamili bankası ile iletişime geçerek red sebebini öğrenebilir ve çözüm sağlayabilir. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_-8"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Kart numarası hatalı girilmiş veya geçersiz kart. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_-9"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Kartın son kullanma tarihi hatalı girilmiş. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_13"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Bankanın işlem yapma hacminden yüksek işlem gönderimi veya geçersiz taksit tutarı gönderimi. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_14"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'İşleme kapalı veya geçersiz kart. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_15"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'İşleme kapalı veya geçersiz kart. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_-18"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Hatalı CVV. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_19"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Banka tarafında yaşanılan anlık sorun veya anlık yoğunluktan dolayı işlem hata almıştır, bir süre sonra tekrar deneyiniz. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_34"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Fraud şüphesi , işlem kart bankası tarafından fraud olarak değerlendirilmiştir.Detaylı bilgi için kart hamili bankası ile iletişime geçmelidir. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_41"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Kayıp kart, işlem kart bankası tarafından çalıntı olarak değerlendirilmiştir.Detaylı bilgi için kart hamili bankası ile iletişime geçmelidir. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_43"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Çalıntı kart, işlem kart bankası tarafından çalıntı olarak değerlendirilmiştir.Detaylı bilgi için kart hamili bankası ile iletişime geçmelidir. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_51"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Limit yetersiz. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_54"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'İşleme kapalı, geçersiz kart. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_58"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Üye işyerinin bu işlemi yapmaya yetkisi yok. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_61"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'İşlem yapma limitini aşıyor. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_62"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Kart yetkileri bu işlemi yapmaya uygun değil, kart hamili bankası ile görüşerek kart yetkilerini kontrol ettirmelidir. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_65"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Kartın günlük işlem limiti aşılmış. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_75"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Kartın günlük işlem limiti aşılmış veya kart bloke edilmiş. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_82"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'İşlem banka tarafında zaman aşımına uğramış, banka tarafında yoğunluk veya anlık teknik sorun yaşanıyor olabilir, bir süre sonra tekrar deneyiniz. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_84"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Hatalı CVV. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_91"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Banka tarafında yaşanılan anlık sorun veya anlık yoğunluktan dolayı işlem hata almıştır, bir süre sonra tekrar deneyiniz. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_93"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'BDDK veya banka tarafınca yasal olarak işlem yapma yetkisi olmayan kartlarda alınan hatadır.Kart hamilleri bankaları ile görüşerek hata çözümü sağlayabilir. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_96"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Banka tarafında yaşanılan anlık sorun veya anlık yoğunluktan dolayı işlem hata almıştır, bir süre sonra tekrar deneyiniz. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_101"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Sanal posun işlemde gönderilen taksit sayısını işlemek için yetkisi yok, pos bankanız ile iletişime geçerek taksit yetkilerinizi kontrol ediniz. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_105"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', '3D doğrulaması yapılamadı, 3D şifresinin doğru girildiğinden, kartın 3D işlem yapma yetkisi olduğundan emin olunuz, kart hamili bankası ile iletişime geçerek detaylı bilgi alabilir ve çözüm sağlayabilir. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_0876"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Kart bilgileri hatalı tuşlanmış veya böyle bir kart yok veya kartın günlük işlem limiti dolmuş. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_2204"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Kartın veya üye işyerinin taksitli işlem yapma yetkisi yok. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_2304"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Aynı hash veya sipariş numarası ile devam eden işlem var, mükerrer işlem. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "GWERROR_0876"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Kart bilgileri hatalı tuşlanmış veya böyle bir kart yok veya kartın günlük işlem limiti dolmuş. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "HASH_MISTMATCH"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Hash uyuşmazlığı. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            elseif ($paymes["message"] == "INVALID SIGNATURE"){
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Hatalı hash hesaplaması, işlem gönderdiğiniz yapının encoding ayarlarının UTF-8 without bom olarak ayarlı olduğundan ve hash değerlerini doğru hesapladığınızdan emin olunuz. '.'KOD: '.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }
            else {
                Order::where('trans_id', $trans_id)->delete();
                $transaction_data = PaymEsTransaction::where('trans_id', $trans_id)->first();

                session()->flash('msg', 'Ödəniş keçmədi, bu barədə support@goshop.az yada Sorğular bölməsində bizə yazın yada çat vasitəsi ilə bildirin. Kod:'.$transaction_data->id);
                return redirect()->route('paymes.payment.result');
            }

    }



    public function declined($id){
        dd($id);
    }

    public function sendMail(){
//        return view('mails.geted_order');
        $name = 'test';
        Mail::to('goshop.aze@gmail.com')->send(new OrderMail($name));
    }
}
