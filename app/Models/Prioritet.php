<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prioritet extends Model
{
    protected $table = 'prioritet';
    public $timestamps = false;
}
