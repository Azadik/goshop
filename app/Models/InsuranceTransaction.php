<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InsuranceTransaction extends Model
{
    protected $table = 'insurance_transaction';
    protected $fillable = ['user_id', 'time', 'status', 'trans_id', 'amount'];
}
