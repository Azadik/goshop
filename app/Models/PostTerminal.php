<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostTerminal extends Model
{
    protected $table = 'post_terminal';
    protected $fillable = ['user_id', 'time', 'status', 'trans_id', 'amount'];
}
