<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CargoPrices extends Model
{
    protected $table = 'cargoprices';
    protected $fillable = ['site', 'price', 'limit'];
}
