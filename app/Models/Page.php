<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';
    protected $fillable = ['title_az', 'title_en', 'title_ru', 'text_az', 'text_en', 'text_ru'];
    public $timestamps = false;
}
