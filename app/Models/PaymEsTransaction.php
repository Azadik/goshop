<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymEsTransaction extends Model
{
    protected $table = 'transaction_paymes';
    protected $fillable = ['user_id', 'trans_id', 'amount'];
}
