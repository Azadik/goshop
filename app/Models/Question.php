<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';
    protected $fillable = ['title', 'text', 'img', 'user_id', 'prioritet_id', 'movzu_id', 'link'];
    public $timestamps = false;
}
