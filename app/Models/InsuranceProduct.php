<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InsuranceProduct extends Model
{
    protected $table        = 'insurance_products';
    protected $fillable     = ['user_id', 'product_id', 'time', 'trans_id', 'product_price', 'insurance_price'];
    public $timestamps      = false;
}
