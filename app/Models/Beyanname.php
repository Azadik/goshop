<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Beyanname extends Model
{
    protected $table = 'beyanname';
    protected $fillable = [
        'package_count',
        'magazin_name',
        'sifarish_number',
        'user_id',
        'price',
        'date',
        'f_a_date',
        'mehsul_type',
        'tracking_number',
        'invois',
        'description',
        'country',
        'shipping_price_usd',
        'shipping_price_azn',
        'shipping_price_tl',
        'weight',
        'status',
        'send_baku_date',
        'in_baku_date',
        'handed_over_date',
        'price_type',
    ];
}
