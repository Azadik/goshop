<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movzu extends Model
{
    protected $table = 'movzu';
    public $timestamps = false;
}
