<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';
    protected $fillable = ['title_az', 'title_en', 'title_ru', 'sort', 'url'];
    public $timestamps = false;
}
