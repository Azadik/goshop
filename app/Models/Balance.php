<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    protected $table = 'balance';
    protected $fillable = ['date', 'user_id', 'balance', 'current_balance', 'time'];
    public $timestamps = false;
}
