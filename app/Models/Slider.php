<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'slider';
    protected $fillable = ['img', 'title', 'url', 'status'];
    public $timestamps = false;
}
