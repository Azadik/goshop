<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['user_id', 'url', 'note', 'order_date', 'count', 'country', 'price', 'weight', 'shipping_price', 'img', 'status', 'boutique_id'];
    public $timestamps = false;
}
