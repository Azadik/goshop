<?php


//Route::get('/search', 'SearchController@autocomplete')->name('autocomplete');


Route::get('/users/logout', 'Auth\LoginController@userLogout')->name('user.logout');

//cronscripts
Route::get('/getcourse',        'Cronscript\CourseValuteController@getCourseValute');
Route::get('/copy_data',        'Cronscript\CopyDataController@copyProductFromOneToOther');
Route::get('/parseimgtitle',    'Cronscript\CopyDataController@parseImageAndTitle');

Route::get('trendyol',    'Cronscript\CopyDataController@getSliderFromTrendyol');
Route::get('setnull',    'Cronscript\CopyDataController@setNullImgStatus');


Route::group(
    ['prefix' => 'orders'],
    function(){
        Route::post('approved', ['as'=>'order.approved',  'uses'=>'OrdersController@approved']);
        Route::post('paymes',   ['as'=>'order.paymes',    'uses'=>'OrdersController@paymEsResult']);

        Route::get('test_mail', 'OrdersController@sendMail');
//        Route::get('declined/{id}', ['as'=>'order.declined',  'uses'=> 'OrdersController@declined']);
//        Route::post('canceled', ['as'=>'order.canceled',  'uses'=> 'OrdersController@canceled']);
    }
);



Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ],
    function()
    {
        Auth::routes();
        /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
        Route::get('/', 'Guest\HomeController@index')->name('home');
        Route::get('page/{id}/{string}',    'Guest\HomeController@getPage')->name('guest.getpage');
        Route::get('price',                 'Guest\HomeController@getPrice')->name('guest.price');
        Route::get('sites-for-buy',         'Guest\HomeController@getSites')->name('guest.sites');
        Route::get('faq',                   'Guest\HomeController@faq')->name('guest.faq');
        Route::get('agreement',             'Guest\HomeController@agreement')->name('guest.agreement');
        Route::get('paymes',                'Guest\HomeController@paymes')->name('guest.paymes');

        Route::prefix('home')->group(function (){
            Route::get('/',                 'HomeController@index')->name('myOrders');

//            Route::get('/url',              'HomeController@getDataFromUrl')->name('get.datafromurl');
            Route::post('get_data_from_url',   'HomeController@getDataFromUrl')->name('get.datafromurl');

            Route::get('/addorder',         'HomeController@addOrder')->name('addOrder');
            Route::post('/addtocart',       'HomeController@addToCart')->name('addToCart');
            Route::post('editorder',        'HomeController@editOrder')->name('editOrder');
            Route::post('delorder',         'HomeController@deleteOrder')->name('deleteOrder');
            Route::post('updateorder',      'HomeController@updateOrder')->name('updateOrder');
            Route::get('/pay/{id}',         'HomeController@pay')->name('pay');
            Route::get('allpay',            'HomeController@allPay')->name('allPay');
            Route::post('showtracking',     'HomeController@showTracking')->name('showtracking');

            Route::get('address',           'HomeController@abroadAddress')->name('abroadAddress');
            Route::get('profile',           'HomeController@profile')->name('profile');
            Route::post('update_profile',   'HomeController@updateProfile')->name('update.profile');
            Route::post('change_password',   'HomeController@changePassword')->name('profile.change.password');
            Route::post('profile_img_upload', 'HomeController@profileImgUpload')->name('profile.img.upload');
            Route::get('rules',             'HomeController@rules')->name('user.rules');
            Route::get('cart',              'HomeController@cart')->name('user.cart');

            Route::get('balance',           'BalanceController@balance')->name('balance');
            Route::get('questions',         'QuestionsController@questions')->name('questions');
            Route::get('question/{id}',     'QuestionsController@getQuestion')->name('get.question');
            Route::post('create_question',  'QuestionsController@createQuestion')->name('create.question');
            Route::post('create_comment',   'QuestionsController@createComment')->name('create.comment');
            Route::post('questions_finish', 'QuestionsController@questionsFinish')->name('questions.finish');

            Route::post('b',                'BeyannameController@beyanname')->name('user.beyyanname');
            Route::get('delivery/{status}', 'BeyannameController@delivery')->name('user.delivery');
            Route::post('b_tracking',       'BeyannameController@showTracking')->name('beyanname.showtracking');
            Route::post('edit_beyanname',   'BeyannameController@editBeyanname')->name('beyanname.edit');
            Route::post('b_update',         'BeyannameController@updateBeyanname')->name('update.beyyanname');

//          payment Pashabank
            Route::post('payment',        'PaymentController@payment')->name('payment');
            Route::get('success',         'PaymentController@successPayment')->name('success.payment');
            Route::get('smp/{trans_id}',  'PaymentController@systemMalfunctionPage')->name('system.malfunction.page');
            Route::get('ifp/{trans_id}',  'PaymentController@insufficientFundsPage')->name('insufficient.funds.page');
            Route::get('cep/{trans_id}',  'PaymentController@cardExpiredPage')->name('card.expired.page');
            Route::get('pe',              'PaymentController@paymentErrors')->name('payment.errors');

            Route::post('post_terminal',   'PaymentController@postTerminal')->name('post.terminal');
            Route::post('set_pay',        'PaymentController@setPay')->name('set.pay');

//            paym.es
            Route::post('paymes_form',     'PaymentController@paymEsForm')->name('paymes.form');
            Route::post('pay_mes',         'PaymentController@paymEs')->name('request.paymes');
            Route::get('paymes_msg',      'PaymentController@paymEsGetMessage')->name('paymes.payment.result');

//            insurance
            Route::post('showinsurance', 'InsuranceController@showInsurance')->name('show.insurance');
            Route::post('insurancepay',   'PaymentController@insurancePayment')->name('pay.insurance');
        });


        Route::prefix('admin')->group(function() {
            Route::get('/i',                'AdminController@index')->name('admin.dashboard');
            Route::get('/login',            'Auth\AdminLoginController@showLoginForm')->name('admin.login');
            Route::post('/login',           'Auth\AdminLoginController@login')->name('admin.login.submit');
            Route::get('/logout',           'Auth\AdminLoginController@logout')->name('admin.logout');

            Route::get('menu',              'AdminController@menu')->name('menu');
            Route::get('edit_menu/{id}',    'AdminController@editMenu')->name('edit.menu');
            Route::post('update_menu',      'AdminController@updateMenu')->name('update.menu');
            Route::get('delete_menu/{id}',  'AdminController@deleteMenu')->name('delete.menu');
            Route::post('add_menu',         'AdminController@addMenu')->name('add.menu');
            Route::post('menusort',         'AdminController@menuSort')->name('menusort');

            Route::get('page',              'AdminController@page')->name('page');
            Route::get('pages',             'AdminController@pageList')->name('page.list');
            Route::post('add_page',         'AdminController@addPage')->name('add.page');
            Route::get('edit_page/{id}',    'AdminController@editPage')->name('edit.page');
            Route::get('delete_page/{id}',  'AdminController@deletePage')->name('delete.page');
            Route::post('update_page',      'AdminController@updatePage')->name('update.page');

            Route::get('faq',               'AdminController@faq')->name('faq');
            Route::get('faq_list',          'AdminController@faqList')->name('faq.list');
            Route::post('add_faq',          'AdminController@addFaq')->name('add.faq');
            Route::get('edit_faq/{id}',     'AdminController@editFaq')->name('edit.faq');
            Route::get('delete_faq/{id}',   'AdminController@deleteFaq')->name('delete.faq');
            Route::post('update_faq',       'AdminController@updateFaq')->name('update.faq');

            Route::get('agreement',         'AdminController@agreement')->name('agreement');
            Route::post('u_agreement',      'AdminController@UpdateAgreement')->name('update.agreement');
            Route::get('rules',         'AdminController@rules')->name('rules');
            Route::post('u_rules',      'AdminController@UpdateRules')->name('update.rules');
        });
    });

