$(function () {

let new_link = '<div class="own-order-box">' +
    '<div style="clear: both; width: 95%; margin: 0 auto"><hr></div><div class="col-md-10 price-parent">\n' +
    '                                        <input name="url[]" type="text" class="own-input-style own-url-style js_get_price_and_size" required  placeholder="Bura linki yazın">\n' +
    '                                        <input name="own_price[]" type="text" class="change-price text-center own-prices js_set_price" placeholder="Qiymət" required>\n' +
    '                                        <input class="text-center own-prices" value="+ 5%" disabled style="background-color: #1796fb; color: #ffffff; border-color: #057cda">' +
    '<select class="js_set_sizes form-control size_form_control">\n' +
    '                                        </select>' +
    '                                        <input name="price[]" type="text" class="update-price text-center own-prices" value="Toplam" readonly>' +
    ' &nbsp;<span class="btn btn-warning counter" data-c="plus"><span class="glyphicon glyphicon-plus"></span></span>\n' +
    '                                        <input name="count[]" style="width: 30px" class="text-center own-prices count" value="1" readonly>\n' +
    '                                        <span class="btn btn-warning counter" data-c="minus"><span class="glyphicon glyphicon-minus"></span></span>\n' +
    '                                    </div>\n' +
    '\n' +
    '                                    <div class="col-md-2 text-right">\n' +
    '                                        <span class="btn btn-danger delete-order-box">Sil</span>\n' +
    '                                    </div>\n' +
    '\n' +
    '\n' +
    '                                    <div class="col-md-12">\n' +
    '                                        <div class="own-date" style="display: none">\n' +
    '                                            <spam class="end-time">Dükanın və ya aksiaynın bitmə vaxtı</spam>\n' +
    '                                            <input type="number" name="hour[]" class="text-center" placeholder="saat" min="1" max="23"> <input type="number" min="1" max="59" name="minute[]" class="text-center" placeholder="degige" >&nbsp;\n' +
    '                                            <input type="number" min="1" max="7" name="day[]" class="text-center" placeholder="gun" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Limit 7 gündən artıx ola bilməz">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '\n' +
    '                                    <div class="col-md-12 col-sm-12">\n' +
    '                                        <div class="form-group">\n' +
    '                                            <textarea name="note[]" class="form-control" rows="2" required placeholder="Qeydi bura yazın"></textarea>\n' +
    '                                        </div>\n' +
    '                                    </div></div>';

let new_link_with_cheked = '<div class="own-order-box"><div style="clear: both; width: 95%; margin: 0 auto"><hr></div><div class="col-md-9 price-parent">\n' +
    '                                        <input name="url[]" type="text" class="own-input-style own-url-style js_get_price_and_size" required  placeholder="Bura linki yazın">\n' +
    '                                        <input type="text" class="change-price text-center own-prices js_set_price" placeholder="Qiymət" required>\n' +
    '                                        <input class="text-center own-prices" value="+ 5%" disabled style="background-color: #1796fb; color: #ffffff; border-color: #057cda"><select class="js_set_sizes form-control size_form_control">\n' +
    '                                        </select>' +
    '                                        <input name="price[]" type="text" class="update-price text-center own-prices" value="Toplam" readonly>' +
    ' <span class="btn btn-warning counter" data-c="plus"><span class="glyphicon glyphicon-plus"></span></span>\n' +
    '                                        <input name="count[]" style="width: 30px" class="text-center own-prices count" value="1" readonly>\n' +
    '                                        <span class="btn btn-warning counter" data-c="minus"><span class="glyphicon glyphicon-minus"></span></span>\n' +
    '                                    </div>\n' +
    '\n' +
    '                                    <div class="col-md-3 text-right">\n' +
    '                                        <span class="btn btn-danger delete-order-box">Sil</span>\n' +
    '                                    </div>\n' +
    '\n' +
    '\n' +
    '                                    <div class="col-md-12">\n' +
    '                                        <div class="own-date" style="display: block">\n' +
    '                                            <spam class="end-time">Dükanın və ya aksiaynın bitmə vaxtı</spam>\n' +
    '                                            <input type="number" name="hour[]" class="text-center" placeholder="saat" min="1" max="23"> <input type="number" min="1" max="59" name="minute[]" class="text-center" placeholder="degige" >&nbsp;\n' +
    '                                            <input type="number" min="1" max="7" name="day[]" class="text-center" placeholder="gun" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Limit 7 gündən artıx ola bilməz">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '\n' +
    '                                    <div class="col-md-12 col-sm-12">\n' +
    '                                        <div class="form-group">\n' +
    '                                            <textarea name="note[]" class="form-control" rows="2" required  placeholder="Qeydi bura yazın"></textarea>\n' +
    '                                        </div>\n' +
    '                                    </div></div>';


function checkCountry(element){
    return parseInt($(element).closest(".form").find(".country").val());
}

$(".rush-order").click(function (event) {
    if (this.checked) {
        $(event.target).closest('.form').find(".own-date").show();
    } else {
        $(event.target).closest('.form').find(".own-date").hide();
    }
})

// add new link
$('.new-link').click(function (event) {
// pri dobavlenii novogo linka proverit chekbox
    if($(event.target).closest('.form').find(".rush-order").is(":checked")) {
        $(event.target).closest('.form').find('.order-box').append(new_link_with_cheked);
    }else{
        $(event.target).closest('.form').find('.order-box').append(new_link);
    }
});


$(document).on("click", ".delete-order-box", function (event) {
    let ordercount = parseInt($(event.target).closest('.own-order-box').find('.count').val());
    let getDeletedPrice = parseFloat($(event.target).closest(".own-order-box").find(".update-price").val()) ;


    if(!isNaN(getDeletedPrice)){
        if(checkCountry(event.target) == 1){
            let getTotalTlPrice = parseFloat ($('#total_tl').text());
            let updateTotalTl = getTotalTlPrice - getDeletedPrice * ordercount;
            let updateTotalAz = updateTotalTl * tl;
            $('#total_tl').text(updateTotalTl.toFixed(2));
            $('#total_az').text(updateTotalAz.toFixed(2));
        } else{
            let getTotalUSDPrice = parseFloat ($('#total_usd').text());
            let updateTotalUSD = getTotalUSDPrice - getDeletedPrice * ordercount;
            let updateTotalAZ = updateTotalUSD * usd;
            $('#total_usd').text(updateTotalUSD.toFixed(2));
            $('#total_usd_az').text(updateTotalAZ.toFixed(2));
        }
    }

    $(event.target).closest(".own-order-box").remove();
});

$(document).on("keyup", ".change-price", function (event) {
    event.target.value = event.target.value.replace(/[^\d,.]*/g, '')
        .replace(/([,.])[,.]+/g, '$1')
        .replace(/^[^\d]*(\d+([.]\d{0,5})?).*$/g, '$1');

    let getPrice = parseFloat(event.target.value);
    let total = getPrice + getPrice * 0.05;

    if(isNaN(total)){
        $(event.target).closest('.price-parent').find('.update-price').val('0.00');
    }else{
        $(event.target).closest('.price-parent').find('.update-price').val(total.toFixed(2));
    }

    let all_prices = 0;
    $(event.target).closest('.form').find(".update-price").each(function() {
        if(!isNaN(parseFloat($(this).val()))){
            let ordercount = parseInt($(this).closest('.price-parent').find('.count').val());
            all_prices = all_prices + parseFloat($(this).val() * ordercount);
        }
    });

    if( checkCountry(event.target) == 1){
        console.log('a');
        let total_az = all_prices * tl;
        $('#total_tl').text(all_prices.toFixed(2));
        $('#total_az').text(total_az.toFixed(2));
    }else{
        let total_az = all_prices * usd;
        $('#total_usd').text(all_prices.toFixed(2));
        $('#total_usd_az').text(total_az.toFixed(2));
    }
});

$(document).on('click', '.counter', function (event) {
    let counter = $(event.currentTarget).attr('data-c');
    let count = parseInt( $(event.target).closest('.price-parent').find('.count').val());
    let price = parseFloat($(event.target).closest('.price-parent').find('.update-price').val());

    if(counter == 'plus'){
        count = count + 1;
        $(event.target).closest('.price-parent').find('.count').val(count);
        if( checkCountry(event.target) == 1){
            let total_tl =  parseFloat($('#total_tl').text()) + price;
            let total_az = total_tl * tl;
            $('#total_tl').text(total_tl.toFixed(2));
            $('#total_az').text(total_az.toFixed(2));
        }else{
            let total_usd =  parseFloat($('#total_usd').text()) + price;
            let total_az = total_usd * usd;
            $('#total_usd').text(total_usd.toFixed(2));
            $('#total_usd_az').text(total_az.toFixed(2));
        }
    }else{
        if(count != 1 ){
            count = count - 1;
            $(event.target).closest('.price-parent').find('.count').val(count);
            if( checkCountry(event.target) == 1){
                let total_tl =  parseFloat($('#total_tl').text()) - price;
                let total_az = total_tl * tl;
                $('#total_tl').text(total_tl.toFixed(2));
                $('#total_az').text(total_az.toFixed(2));
            }else{
                let total_usd =  parseFloat($('#total_usd').text()) - price;
                let total_az = total_usd * usd;
                $('#total_usd').text(total_usd.toFixed(2));
                $('#total_usd_az').text(total_az.toFixed(2));
            }
        }
    }
});
});