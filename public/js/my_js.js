// Calculator
function validate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault)
            theEvent.preventDefault();
    }
}

// goshop icon
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
});

// registrasiyada yerleshir
$("select[name=social]").change(function(){
    if($(this).val()==7)
        $("div.diger").show();
    else
        $("div.diger").hide();
});


// dropdown hover menu
$('ul.nav li.dropdown').hover(function() {
    $(this).find('.dropdown-menu').stop(true, true).delay().show();
}, function() {
    $(this).find('.dropdown-menu').stop(true, true).delay().hide();
});

// id = country package wght
function getEtalonResult(val) {
    if (val == "cm") {
        return 1;
    } else {
        return 2.54;
    }
}

function getVolumetricWeight(width, length, height, weight) {
    var r = (width * length * height) / 6000;
    if (r >= weight) {
        return r;
    } else {
        return weight;
    }
}

function getResult(weight, country) {
    if ((country == 1)) {
        if (weight >= 0 && weight <= 0.25) {
            return 2.50;
        } else if (weight > 0.25 && weight <= 0.5){
            return 3.70;
        } else {
            return weight * 5.90;
        }
    } else if (country == 2) {
        if (weight >= 0 && weight <= 0.5) {
            return 5.10;
        } else {
            return weight * 8.8;
        }
    }
}

var decimal_places = 2;
var decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places);

function showResult(a){
    $('#t_price').animateNumber({
        number : a * decimal_factor,
        numberStep : function(now, tween) {
            var floored_number = (now) / decimal_factor,
                target = $(tween.elem);

            if (decimal_places > 0) {
                // force decimal places even if they are 0
                floored_number = floored_number.toFixed(decimal_places);

                // replace '.' separator with ','
                floored_number = floored_number.toString().replace('.', ',');
            }

            target.text('$' + floored_number);

        }
    }, 500);
}

$("#calculate").click(function () {
    let country         = $('#country').val();
    let package         = $('#package').val();

    let weight          = $('#weight').val();
    let weight_etalon   = $('#weight_etalon').val();

    let length          = $('#length').val();
    let length_etalon   = $('#length_etalon').val();

    let width           = $('#width').val();
    let width_etalon    = $('#width_etalon').val();

    let height          = $('#height').val();
    let height_etalon   = $('#height_etalon').val();


    console.log(weight_etalon);


    var r_weight = (width >= 100 || length>=100 || height >= 100)? getVolumetricWeight(width * getEtalonResult(width_etalon), length * getEtalonResult(length_etalon), height * getEtalonResult(height_etalon), weight * getEtalonResult(weight_etalon)): weight;


    if(r_weight > 0.5 && r_weight <= 1) {
        r_weight = 1;
    }

    let result = package * getResult(r_weight, country);

    if ((width != 0 && length != 0 && height != 0) || (weight != 0 && package != 0)) {
        showResult(result);
    } else {
        showResult(0);
    }
});


function showResultInLink(a){
    $('#link_price_result').animateNumber({
        number : a * decimal_factor,
        numberStep : function(now, tween) {
            var floored_number = (now) / decimal_factor,
                target = $(tween.elem);

            if (decimal_places > 0) {
                // force decimal places even if they are 0
                floored_number = floored_number.toFixed(decimal_places);

                // replace '.' separator with ','
                floored_number = floored_number.toString();
            }

            target.text(floored_number + ' AZN');

        }
    }, 200);
}


$('#link_price').keyup(function (event) {
    event.target.value = event.target.value.replace(/[^\d,.]*/g, '')
        .replace(/([,.])[,.]+/g, '$1')
        .replace(/^[^\d]*(\d+([.]\d{0,5})?).*$/g, '$1');

    let price = event.target.value.replace(/[^\d,.]*/g, '')
        .replace(/([,.])[,.]+/g, '$1')
        .replace(/^[^\d]*(\d+([.]\d{0,5})?).*$/g, '$1');
    let totalLinkPrice = tl * price;

    // $('#link_price_result').text(totalLinkPrice);
    showResultInLink(totalLinkPrice);
});



function showResultInLinkUSD(a){
    $('#link_price_result_usd').animateNumber({
        number : a * decimal_factor,
        numberStep : function(now, tween) {
            var floored_number = (now) / decimal_factor,
                target = $(tween.elem);

            if (decimal_places > 0) {
                // force decimal places even if they are 0
                floored_number = floored_number.toFixed(decimal_places);

                // replace '.' separator with ','
                floored_number = floored_number.toString();
            }

            target.text(floored_number + ' AZN');

        }
    }, 200);
}

$('#link_price_usd').keyup(function (event) {
    event.target.value = event.target.value.replace(/[^\d,.]*/g, '')
        .replace(/([,.])[,.]+/g, '$1')
        .replace(/^[^\d]*(\d+([.]\d{0,5})?).*$/g, '$1');

    let price = event.target.value.replace(/[^\d,.]*/g, '')
        .replace(/([,.])[,.]+/g, '$1')
        .replace(/^[^\d]*(\d+([.]\d{0,5})?).*$/g, '$1');
    let totalLinkPrice = usd * price;

    // $('#link_price_result').text(totalLinkPrice);
    showResultInLinkUSD(totalLinkPrice);
});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});